# -*- coding: utf-8 -*-
"""
Created on Thu Feb 19 11:34:45 2015

    Comparing set of values for SNR and Doppler Noise based on multi-antenna
    sessions.
@author: molera
"""
import numpy as np
import matplotlib.pyplot as plt
import readTable as rt

mex_file  = '/Users/molera/Downloads/ScintObsSummaryTabMEX.Block3.f3.4.197i.txt'

print 'Reading table of measurements for Table 1 MEX'
sc3 = rt.readTable(mex_file)
sc3.read_table_v35()

dnoise = sc3.Dnoise

'Data is based on the experiment observed on m0122'
ant1 = dnoise[772:781]
ant2 = dnoise[781:790]
ant3 = dnoise[790:799]
ant4 = dnoise[799:808]
ant5 = dnoise[808:816]
  
sigma1 = np.std(ant1)
sigma2 = np.std(ant2)
sigma3 = np.std(ant3)
sigma4 = np.std(ant4)
sigma5 = np.std(ant5)

w1 = 1/np.power(sigma1,2)
w2 = 1/np.power(sigma2,2)
w3 = 1/np.power(sigma3,2)
w4 = 1/np.power(sigma4,2)
w5 = 1/np.power(sigma5,2)

anta = np.sum([np.multiply(ant1,w1), np.multiply(ant2,w2), np.multiply(ant3,w3), np.multiply(ant4,w4)], axis=0) / (w1 + w2 + w3 + w4)

meana  = np.mean(anta)
sigmaa = np.std(anta)

comp_sigma = [sigma1/sigmaa, sigma2/sigmaa, sigma3/sigmaa, sigma4/sigmaa]

print 'Worst pair  %0.2f' %(np.max(comp_sigma))
print 'Mean  pair  %0.2f' %(np.mean(comp_sigma))
print 'Best  pair  %0.2f' %(np.min(comp_sigma))

plt.figure()
plt.hold(True)
plt.grid(which='minor', axis='both', color='green')    
plt.plot(sigma1, marker='o', mec='red', mfc='None', linestyle='None')
plt.plot(sigma2, marker='o', mec='blue', mfc='None', linestyle='None')
plt.plot(sigma3, marker='o', mec='green', mfc='None', linestyle='None')
plt.plot(sigma4, marker='o', mec='black', mfc='None', linestyle='None')
plt.plot(sigmaa, marker='+', mec='blue')
plt.xlabel('m0122')
plt.ylabel('Doppler noise (mHz in 10s)')
plt.legend(['Ww','Sh','Ho','Bd','Aveg'])
plt.show()
