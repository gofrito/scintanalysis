#!/usr/bin/env python
"""
Created on Fri Oct 10 12:36:31 2014
    class readTable
    functions:  read_table()
                read_table_old()
                read_table_v33()
                read_table_v34()
                read_table_v35()
    @author: molera
    """

import sys

class readTable(object):
    def __init__(self,fileName):
            self.fileName = fileName
            self.code = []
            self.nr = []
            self.AUdist = []
            self.Broadening = []
            self.ErrorSlope = []
            self.HH = []
            self.LatEcl = []
            self.LonEcl = []
            self.MM = []
            self.Peak3mHz = []
            self.PhN = []
            self.PhSc = []
            self.SOT = []
            self.STO = []
            self.SS = []
            self.ScintSlope = []
            self.Dnoise = []
            self.DopSNR = []
            self.SolarAct = []
            self.SysNoise = []
            self.TEC = []
            self.Tx = []
            self.az = []
            self.dd = []
            self.deh = []
            self.dem = []
            self.des = []
            self.dur = []
            self.el = []
            self.mm = []
            self.ns = []
            self.nsc = []
            self.rah = []
            self.ram = []
            self.ras = []
            self.yy = []
            self.Ceb_Ion_TEC = []
            self.VEX_Ion_TEC = []

    def read_table_v33(self):
        if sys.version_info[0] >= 3:
            with open(self.fileName,'r', encoding='utf-8') as f:
                f_lines = f.readlines()
        else:
            with open(self.fileName,'r') as f:
                f_lines = f.readlines()
        for line in f_lines:
            if len(line)>2 and line[0]!='/' and line[1]!='_':
                vals = list(map(float, line[1:117].split() + line[119:175].split() + line[179:-3].split()))
                self.nr.append(vals[0])
                self.ns.append(vals[1])
                self.nsc.append(vals[2])
                self.code.append(vals[3])
                self.yy.append(vals[4])
                self.mm.append(vals[5])
                self.dd.append(vals[6])
                self.HH.append(vals[7])
                self.MM.append(vals[8])
                self.SS.append(vals[9])
                self.dur.append(vals[10])
                self.rah.append(vals[11])
                self.ram.append(vals[12])
                self.ras.append(vals[13])
                self.deh.append(vals[14])
                self.dem.append(vals[15])
                self.des.append(vals[16])
                self.az.append(vals[17])
                self.el.append(vals[18])
                self.LatEcl.append(vals[19])
                self.LonEcl.append(vals[20])
                self.AUdist.append(vals[21])
                self.SOT.append(vals[22])
                self.STO.append(vals[23])
                self.PhSc.append(vals[24])
                self.PhN.append(vals[25])
                self.ScintSlope.append(vals[26])
                self.ErrorSlope.append(vals[27])
                self.Peak3mHz.append(vals[28])
                self.SysNoise.append(vals[29])
                self.Dnoise.append(vals[30])
                self.DopSNR.append(vals[31])
                self.SolarAct.append(vals[32])
                self.Tx.append(vals[33])
                self.VEX_Ion_TEC.append(vals[34])
                self.Ceb_Ion_TEC.append(vals[35])
                self.TEC.append(vals[36])
            
    def read_table_v34(self):
        if sys.version_info[0] >= 3:
            with open(self.fileName,'r', encoding='utf-8') as f:
                f_lines = f.readlines()
        else:
            with open(self.fileName,'r') as f:
                f_lines = f.readlines()
        for line in f_lines:
            if len(line)>2 and line[0]!='/' and line[1]!='_':
                vals = list(map(float, line[1:117].split() + line[120:176].split() + line[181:-3].split()))
                self.nr.append(vals[0])
                self.ns.append(vals[1])
                self.nsc.append(vals[2])
                self.code.append(vals[3])
                self.yy.append(vals[4])
                self.mm.append(vals[5])
                self.dd.append(vals[6])
                self.HH.append(vals[7])
                self.MM.append(vals[8])
                self.SS.append(vals[9])
                self.dur.append(vals[10])
                self.rah.append(vals[11])
                self.ram.append(vals[12])
                self.ras.append(vals[13])
                self.deh.append(vals[14])
                self.dem.append(vals[15])
                self.des.append(vals[16])
                self.az.append(vals[17])
                self.el.append(vals[18])
                self.LatEcl.append(vals[19])
                self.LonEcl.append(vals[20])
                self.AUdist.append(vals[21])
                self.SOT.append(vals[22])
                self.STO.append(vals[23])
                self.PhSc.append(vals[24])
                self.PhN.append(vals[25])
                self.ScintSlope.append(vals[26])
                self.ErrorSlope.append(vals[27])
                self.Peak3mHz.append(vals[28])
                self.SysNoise.append(vals[29])
                self.Dnoise.append(vals[30])
                self.DopSNR.append(vals[31])
                self.SolarAct.append(vals[32])
                self.Tx.append(vals[33])
                self.VEX_Ion_TEC.append(vals[34])
                self.Ceb_Ion_TEC.append(vals[35])
                self.TEC.append(vals[36])

    def read_table_v35(self):
        if sys.version_info[0] >= 3:
            with open(self.fileName,'r', encoding='utf-8') as f:
                f_lines = f.readlines()
        else:
            with open(self.fileName,'r') as f:
                f_lines = f.readlines()
        for line in f_lines:
            if len(line)>2 and line[0]!='/' and line[1]!='_':
                vals = list(map(float, line[1:118].split() + line[120:182].split() + line[184:-3].split()))
                self.nr.append(vals[0])
                self.ns.append(vals[1])
                self.nsc.append(vals[2])
                self.code.append(vals[3])
                self.yy.append(vals[4])
                self.mm.append(vals[5])
                self.dd.append(vals[6])
                self.HH.append(vals[7])
                self.MM.append(vals[8])
                self.SS.append(vals[9])
                self.dur.append(vals[10])
                self.rah.append(vals[11])
                self.ram.append(vals[12])
                self.ras.append(vals[13])
                self.deh.append(vals[14])
                self.dem.append(vals[15])
                self.des.append(vals[16])
                self.az.append(vals[17])
                self.el.append(vals[18])
                self.LatEcl.append(vals[19])
                self.LonEcl.append(vals[20])
                self.AUdist.append(vals[21])
                self.SOT.append(vals[22])
                self.STO.append(vals[23])
                self.PhSc.append(vals[24])
                self.PhN.append(vals[25])
                self.ScintSlope.append(vals[26])
                self.ErrorSlope.append(vals[27])
                self.Peak3mHz.append(vals[28])
                self.SysNoise.append(vals[29])
                self.Dnoise.append(vals[30])
                self.DopSNR.append(vals[31])
                self.SolarAct.append(vals[32])
                self.Tx.append(vals[33])
                self.VEX_Ion_TEC.append(vals[34])
                self.Ceb_Ion_TEC.append(vals[35])
                self.TEC.append(vals[36])
                
    def read_table_old(self):
        if sys.version_info[0] >= 3:
            with open(self.fileName,'r', encoding='utf-8') as f:
                f_lines = f.readlines()
        else:
            with open(self.fileName,'r') as f:
                f_lines = f.readlines()
        for line in f_lines:
            if len(line)>2 and line[0]!='/' and line[1]!='_':
                vals = list(map(float, line[1:112].split() + line[114:-3].split()))
                self.nr.append(vals[0])
                self.ns.append(vals[1])
                self.nsc.append(vals[2])
                self.code.append(vals[3])
                self.yy.append(vals[4])
                self.mm.append(vals[5])
                self.dd.append(vals[6])
                self.HH.append(vals[7])
                self.MM.append(vals[8])
                self.SS.append(vals[9])
                self.dur.append(vals[10])
                self.rah.append(vals[11])
                self.ram.append(vals[12])
                self.ras.append(vals[13])
                self.deh.append(vals[14])
                self.dem.append(vals[15])
                self.des.append(vals[16])
                self.az.append(vals[17])
                self.el.append(vals[18])
                self.SOT.append(vals[19])
                self.AUdist.append(vals[20])
                self.LatEcl.append(vals[21])
                self.LonEcl.append(vals[22])
                self.PhN.append(vals[23])
                self.PhSc.append(vals[24])
                self.ScintSlope.append(vals[26])
                self.ErrorSlope.append(vals[27])
                self.Peak3mHz.append(vals[28])
                self.SysNoise.append(vals[29])
                self.SolarAct.append(vals[30])
                self.VEX_Ion_TEC.append(vals[32])
                self.Ceb_Ion_TEC.append(vals[33])
                self.TEC.append(vals[34])