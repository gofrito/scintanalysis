#!$(which python)
"""
    Created on Fri Aug 29 17:18:51 2014
    Scintillation analysis version 1.2
    Caracterising the major paratmeters influencing the S/C 
    traking phase and Doppler noise
    @author: molera
    """
import numpy as np
import matplotlib.pyplot as plt
import readTable as rt
import ScintFunction as sf

fileName = '/Users/molera/Downloads/ScintObsSummaryTabVEX.Block3.f3.3.085g.txt'

def parameters_antenna(antenna_code):
    antenna = ['Mh','Mc','Ma','Nt','Wz','Ys','Pu','On','Hh','Sh','Km','Ur','Ht','Sv','Zc','Bd','T6','Ww']
    code = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
    altitude = [75.63,67.18,543.37,143.23,669.12,998.93,180,1415,29,1973.37,2033.2,1374.2,86.1,1175.1,821.6,49.3,127.9]
    pressure = [0.99107,0.99207,0.9376,0.98316,0.92372,0.88829,0.97888,0.99881,0.84553,0.99657,0.79133,0.78576,0.84963,0.98985,0.86993,0.90717,0.99417,0.98495]
    codeInd = code[antenna.index(antenna_code)]
    altInd = altitude[antenna.index(antenna_code)]
    presInd = pressure[antenna.index(antenna_code)]
    return codeInd,altInd,presInd

    '''
        To be corrected for nominal surface pressure, which is altitude dependent
        '''
def airmass(el):
    airm = 1./(np.sin(self.el) + 0.50572*np.power(6.07995 + self.el,-1.6364))
    
    return airm
        
def plotting(self):
    self.phi = sf.sot2gop(self.AUdist,self.SOT)
    #plt.plot(self.STO,np.log10(np.multiply(self.PhSc,2000)), color='red', marker='x')
    #plt.plot(self.STO,np.log10(np.multiply(self.PhN,2000)), color='orange', marker='.')
    plt.plot(self.phi,np.log10(np.multiply(np.sqrt(np.power(self.PhSc,2) - np.power(self.PhN,2)),2000)), color='black', marker='.')
    plt.plot(self.phi,np.log10(self.VEX_Ion_TEC), color='blue', marker='.')
    plt.plot(self.phi,np.log10(self.Ceb_Ion_TEC),color='cyan', marker='.')
    plt.plot(self.phi,np.log10(self.TEC),color='red', marker='o')
    plt.ylabel('Phase scintillation index')
    plt.xlabel('Sun-Target-Observer angle (deg)')
    plt.grid('on')
    plt.show()
    
    plt.subplot(131)
    plt.plot(self.Dnoise,self.PhSc,'.')
    plt.title('Doppler Noise vs. Phase scintillation index')
    plt.xlabel('Doppler Noise (rad^2/Hz)')
    plt.ylabel('Phase Scint index')
    plt.subplot(132)
    plt.plot(self.DopSNR,self.PhSc,'.')
    plt.title('Detection SNR vs. Phase scintillation index')
    plt.xlabel('Detection SNR')
    plt.ylabel('Phase Scint index')
    plt.subplot(133)
    plt.plot(self.Dnoise,self.DopSNR,'.')
    plt.show()


''' 
    Getting the airmass at both ends (Cebreros and the station)
    We use the Kasten and Young model. Kasten F. and A.T. Young 1989. Revised optical air mass tables and approximation
    formula. Applied optics 28:4735-4738.
    '''
    # For Cebreros
    # airmtx = airm(self.elcb)*pceb
    # For stations
    #    airmrx = airmass(self.el)*pst[self.code-1]

print 'Reading table of measurements'
sc = rt.readTable(fileName)
sc.read_table()
print 'Plotting the Scintillation index versus SOT'
plotting(sc)

