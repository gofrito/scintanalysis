#!/usr/bin/env python
"""
    Created on Wed Oct 22 16:05:07 2014
    ScintFunctions.py
    It contains several functions used in the scintillation analysis
    @author: molera
    """
import numpy as np

#==============================================================================
# Converting SOT and Distance into Orbital phase
# The values (Rv and dturn) are set for the case of Venus
# In case of Mars use Rm and dturn 
#==============================================================================
def sot2pho(dist,sot,s):
    ns    = np.size(dist)
    if s == 'v':
        R = 0.723
        dturn = 0.719
    else:
        R = 1.524
        dturn = 0
    
    beta = np.arccos((np.power(R,2) + 1 - np.power(dist,2))/(2*R))*180/np.pi
    gama = np.arcsin((1/R*np.sin(np.multiply(sot,np.pi)/180)))*180/np.pi

    pho = 180 - gama - sot
    phos = np.zeros(ns)

    for ip in np.arange(ns):
        if (dist[ip] > dturn):
            phos[ip] = pho[ip]
        else:
            phos[ip] = beta[ip]
    
    pho = 180 - phos
    return pho

def sot2gop(dist,sot):
    ns    = np.size(dist)
    dturn = 1.228
        
    phi = np.zeros(ns)
    gam = np.zeros(ns)
        
    for ip in np.arange(ns):
        phi[ip] = np.arcsin(np.multiply(dist[ip],np.sin(sot[ip]*np.pi/180))/0.723)
        gam[ip] = np.pi - np.arcsin(dist[ip]*np.sin(sot[ip]*np.pi/180)/0.723)
        
    for ip in np.arange(ns):
        if dist[ip] < dturn:
            phi[ip] = gam[ip]
        
    return phi*180/np.pi
    
'''
    Transform Doppler residual to mm/s
    '''
def hz2mm(f_nom,doppler):
    stdnoise = np.divide(np.multiply(doppler,3e8),f_nom)
    return stdnoise

'''
    Read values from the Scintillation Summary Table and extract a copy to 
    a numpy array with specific labels for each column. It works in version 3.
    '''
def scint2array(filename):
    scintTable = []
    with open(filename) as f:
        f_lines = f.readlines()

    for line in f_lines:
        if len(line)>20 and line[0]!='/':
            line_parsed = line.replace('|',' ')
            line_parsed = line_parsed.replace("'",' ')
            scintTable.append(line_parsed)

    sc = np.loadtxt(scintTable, dtype=[('TotalScanNr','u2'),('SessionNr','u2'),('ScanNr','u2'),
        ('StationCode','u2'),('YY','i2'),('MM','i2'),('DD','i2'),('hh','i2'),('mm','i2'),
        ('ss','i2'),('dur','i2'),('rah','f2'),('ram','f2'),('ras','f2'),('deh','f2'),
        ('dem','f2'),('des','f2'),('az','f2'),('el','f2'),('LatEcl','f2'),('LonEcl','f2'),
        ('AUdist','f2'),('SOT','f2'),('STO','f2'),('PhSc','f2'),('PhN','f2'),('ScintSlope','f2'),
        ('ErrorSlope','f2'),('Peak3mHz','f2'),('SysNoise','f1'),('DopSNR','f2'),('SNR','f4'),
        ('SolarAct','f2'),('Tx','u1'),('VEX_Ion_TEC','f2'),('Ceb_Ion_TEC','f2'),('TEC','f2')])

    return sc