#!/usr/bin/env python
"""
Created on Fri Oct 10 12:29:10 2014
    
    VEX and MEX data Handling
    Scripts reads Scintillation summary tables, plots the results 
    wrt to the solar elongation and show some stadistics.

    @author: molera
    """
    
import numpy as np
import ScintFunctions as sf
import matplotlib.pyplot as plt
import readTable as rt

#==============================================================================
# 0 TEC models for Venus and Mars 
# 1 PCal, MEX and VEX scintillation values from summary tables
# 2 Histogram of the VEX and MEX observations wrt telescopes
# 3 Plot MEX and VEX scintillation index for all data observed
# 4
#==============================================================================
plot = 3
path_files = '/Users/gmolera/Sync/Projects/IPS/Result/'

pcal_file  = path_files + 'ScintObsSummaryTabPCAL.020.txt'
tecV_file  = path_files + 'TecVenus.NFSn720.txt'
tecM_file  = path_files + 'TecMars.NFSn720.txt'
vex1_file  = path_files + 'ScintObsSummaryTabVEX.Block1.101.txt'
vex2_file  = path_files + 'ScintObsSummaryTabVEX.Block2.049.txt'
vex3_file  = path_files + 'ScintObsSummaryTabVEX.Block3.167i.txt'
mex_file   = path_files + 'ScintObsSumMEX.Block1.txt'
mex2_file  = path_files + 'ScintObsSumMEX.Block2.txt'
air_file   = path_files + 'AirMass.vex.txt'
SummaryMex = path_files + 'PlotVectorsMex.txt'
SummaryVex = path_files + 'PlotVectorsVex.txt'

#==============================================================================
# Reading the interplanetary TEC model for Venus and Mars Express. The table is
# distributed in 720 points over 180 degrees. The sampling is 0.25 deg.
# Column 0: phon - nominal orbital phase     
# Column 1: sotn - nominal sun-observer-target angle
# Column 2: tecn - nominal one-way TEC       
# Column 3: tecf - fast wind one-way TEC
# Column 4: tecs - slow wind one-way TEC 
#==============================================================================

tecV = np.loadtxt(tecV_file,skiprows=0)
tecM = np.loadtxt(tecM_file,skiprows=0) 

phonv = tecV[:,0]; sotnv = tecV[:,1]; tecnv = tecV[:,2]; tecfv = tecV[:,3]; tecsv = tecV[:,4]
phonm = tecM[:,0]; sotnm = tecM[:,1]; tecnm = tecM[:,2]; tecfm = tecM[:,3]; tecsm = tecM[:,4]

# Coefficients for up-down TEC combining accounting for up/down frequency ratio
C1 = (1 + 880/749)
C2 = np.square(1 + np.power(880/749,2))

tecnv = C2*tecnv; tecfv = C2*tecfv; tecsv = C2*tecsv
tecnm = C2*tecnm; tecfm = C2*tecfm; tecsm = C2*tecsm

# Mean of fast and slow winds
tecmv = 0.5*(tecfv + tecsv)
tecmm = 0.5*(tecfm + tecsm)

# re - normalizing fast,slow and mean tecs for best fit with nominal
# just to simplify the further analysis
# print 'Double check the normalization coefficient is the same as in the notes - 13.10.2014'
Cnf = tecnv/tecfv; tecfv = tecfv*Cnf
Cns = tecnv/tecsv; tecsv = tecsv*Cns
Cnm = tecnv/tecmv; tecmv = tecmv*Cnm

if plot == 0:
    plt.title('Total Electron Content models for Venus and Mars')
    plt.semilogy(phonv, tecnv, color='blue', label='tecnv')
    plt.semilogy(sotnv, tecnv, color='blue', linestyle='dashed', label='tecnv')
    plt.semilogy(phonm, tecnm, color='red', label='tecnm')
    plt.semilogy(sotnm, tecnm, color='red',  linestyle='dashed', label='tecnm')
    plt.xlabel('phonv,sotnv,phonm,sotnm')
    plt.ylabel('Total Electron Content')
    plt.legend()
#    plt.annotate(s='Nominal TEC (in tec units) for Venus (blue) and Mars (red). \n    Solid lines - against orbital phase, dashed lines - against solar elongation)',(80,1000))
    plt.show()

##==============================================================================
## Read and unpack Pcal data table
##==============================================================================
Dppc = np.loadtxt(pcal_file)
Nppc = np.size(Dppc)

scpc = Dppc[:,8]
scpn = Dppc[:,9]
stp  = Dppc[:,3]
slop = Dppc[:,10]
dnpc = Dppc[:,14]

scpc = np.real(np.sqrt(np.power(scpc,2) + np.power(scpn,2)))

if plot == 1:
    plt.plot(scpc, 'bx')
    plt.title('Fluctuation of the PCal signal')
    plt.xlabel('samples')
    plt.ylabel('scpc')
    plt.title('Example of PCal scintillation index')
    plt.show()

#==============================================================================
# Read and unpack AirMass data table
#==============================================================================

airmass = np.loadtxt(air_file)

tpho  = airmass[:,0]
tairm = airmass[:,1]

#==============================================================================
# Summary with IP TEC data + ionosphere + troposhphere
# all together
#==============================================================================

vdata = np.loadtxt(SummaryVex)
mdata = np.loadtxt(SummaryMex)
clock = np.zeros(np.size(vdata[:,0]))
clock[:] = 0.112

if plot == 1:
    plt.figure()
    plt.hold(True)
    plt.title('Two-way phase scintillation extracted from VEX observations, 2009-2014')
    plt.grid(which='major', axis='both', color='green',linestyle='-', linewidth=0.5)
    plt.semilogy(vdata[:,0], vdata[:,1], color='blue', marker='+', linestyle='None', label='Phase [rad]')
    plt.semilogy(vdata[:,0], vdata[:,2], marker='o', mec='red', mfc='None', linestyle='None', label='IP tec')
    plt.semilogy(vdata[:,0], vdata[:,3], color='chocolate', marker='x', linestyle='None', label='IO tec')
    plt.semilogy(vdata[:,0], vdata[:,4], color='brown', marker='+', linestyle='None', label='Airmass')
    plt.semilogy(vdata[:,0], clock, color='black', marker='.', linestyle='None', label='H-maser clock')					
    plt.semilogy([0,180],[0.040,0.040], color='red', label='Instr. error')				
    plt.xlabel('Orbital phase (deg)')
    plt.ylabel('Scintillation index (rad)')
    plt.xlim([0,180])
    plt.legend(fontsize='medium')
    plt.show()

if plot == 1:
    plt.figure()
    plt.hold(True)
    plt.title('Two-way phase scintillation extracted from Mars observations, 2014-2016')
    plt.grid(which='major', axis='both', color='green',linestyle='-', linewidth=0.5)
    plt.semilogy(mdata[:,0], mdata[:,1], color='blue', marker='+', linestyle='None', label='Phase [rad]')
    plt.semilogy(mdata[:,0], mdata[:,2], marker='o', mec='red', mfc='None', linestyle='None', label='IP tec - scaled')
    plt.semilogy(mdata[:,0], mdata[:,3], color='chocolate', marker='x', linestyle='None', label='IO tec - scaled')
    plt.xlabel('Orbital phase (deg)')
    plt.ylabel('Scintillation index (rad)')
    # 'Phase (in rad) - IP and IO TEC and air mass - scaled for best fit'
    plt.xlim([0,180])
    plt.legend(fontsize='medium')
    plt.show()

#==============================================================================
# Reading and uncpacking the measurement table for VEX and MEX
# VEX contains 3 different tables. 
# Table 1 is format 1.0
# Table 2 is format 3.3
# Table 3 is format 3.4
#==============================================================================

print('Reading table of measurements for Table 1 VEX')
sc0 = rt.readTable(vex1_file)
sc0.read_table_old()
sc0.pho = sf.sot2pho(sc0.AUdist,sc0.SOT,'v')

print('Reading table of measurements for Table 2 VEX')
sc1 = rt.readTable(vex2_file)
sc1.read_table_v33()
sc1.pho = sf.sot2pho(sc1.AUdist,sc1.SOT,'v') 

print('Reading table of measurements for Table 3 VEX')
sc2 = rt.readTable(vex3_file)
sc2.read_table_v35()
sc2.pho = sf.sot2pho(sc2.AUdist,sc2.SOT,'v')

print('Reading table of measurements for Table 1 MEX')
sc3 = rt.readTable(mex_file)
sc3.read_table_v35()
sc3.pho = sf.sot2pho(sc3.AUdist,sc3.SOT,'m')

print('Reading table of measurements for Table 2 MEX')
sc4 = rt.readTable(mex_file)
sc4.read_table_v35()
sc4.pho = sf.sot2pho(sc4.AUdist,sc4.SOT,'m')
#==============================================================================
# Combined data for VEX
#==============================================================================
vpho    = sc0.pho
vstat   = sc0.code + sc1.code + sc2.code
vpho    = np.append(sc0.pho, sc1.pho,axis=0)
vpho    = np.append(vpho, sc2.pho, axis=0)
vsot    = sc0.SOT + sc1.SOT + sc2.SOT
vdist   = sc0.AUdist + sc1.AUdist + sc2.AUdist
vscint  = sc0.PhSc + sc1.PhSc + sc2.PhSc
vsn     = sc0.SysNoise + sc1.SysNoise + sc2.SysNoise
sc0.TEC = np.multiply(1.45,sc0.TEC)
sc1.TEC = np.multiply(1.0,sc1.TEC)
sc2.TEC = np.multiply(1.0,sc2.TEC)
vtecn   = np.concatenate((sc0.TEC,sc1.TEC,sc2.TEC),axis=0)

viotec = sc0.VEX_Ion_TEC + sc1.VEX_Ion_TEC + sc2.VEX_Ion_TEC
vcetec = sc0.Ceb_Ion_TEC + sc1.Ceb_Ion_TEC + sc2.Ceb_Ion_TEC
velt   = sc0.el + sc1.el + sc2.el
vslope = sc0.ScintSlope + sc1.ScintSlope + sc2.ScintSlope

mstat  = sc3.code + sc4.code
mpho   = np.append(sc3.pho, sc4.pho)
mscint = np.append(sc3.PhSc, sc4.PhSc)
mtec   = np.append(sc3.TEC, sc4.TEC)
mDnoise= np.append(sc3.Dnoise, sc4.Dnoise) 
miotec = np.append(sc3.VEX_Ion_TEC, sc4.VEX_Ion_TEC)
mcetec = np.append(sc3.Ceb_Ion_TEC, sc4.VEX_Ion_TEC)
mclock = np.zeros(np.size(mpho))

mclock[:] = 70

#==============================================================================
# Plot 2 Histogram of the VEX and MEX observations with respect
# to the radio telescopes
#==============================================================================
nstat = 26
labels = ['Mh','Mc','Ma','Nt','Wz','Ys','Pu','On','Hh','Sh','Km','Ur','Ht','Sv','Zc','Bd','T6','Ww','Ho','Hb','Yg','Ke','Wd','Wn']
if plot == 2:
    plt.figure()
    plt.subplot(2,1,1)
    plt.hist(vstat,np.arange(18)+1)
    plt.xlim([1,nstat])
    plt.grid(True)
    plt.xticks(np.arange(nstat)+1,labels)

    plt.subplot(2,1,2)
    plt.hist(mstat,np.arange(nstat)+1, color='red')
    plt.grid(True)
    plt.xticks(np.arange(nstat)+1,labels)    
    plt.xlim([1,nstat])
    plt.show()

#==============================================================================
# Summary plot of scintillation measurement
# a) Two-way interplanetary TEC at measurement times - red open circles
# b) Measured scintillation index (scaled for best fit IP+IO TEC) - blue's +
# c) Two-way ionospheric TEC (for Cebrerors and receiving station) - brown's x
#==============================================================================
if plot == 3:
    plt.figure()
    plt.title('Summary plot of VEX scintillation measurements, 2009-14')
    plt.grid(which='major', axis='both', color='green',linestyle='-', linewidth=0.5)
    plt.semilogy(vpho, np.multiply(2400,vscint), color='blue', marker='+', linestyle='None', label='Phase [rad] - scaled')
    plt.semilogy(vpho, vtecn, marker='o', mec='red', mfc='None', linestyle='None', label='IP tec')
    plt.semilogy(vpho, viotec, color='chocolate', marker='x', linestyle='None', label='IO tec')
    plt.semilogy(tpho, tairm, color='brown', marker='+', linestyle='None', label='Airmass')
    plt.xlim([0,180])
    plt.ylabel('Phase scintillation')
    plt.xlabel('Orbital phase (deg)')    
    plt.legend(fontsize='small')
    plt.show()

    plt.figure()
    plt.title('Summary plot of MEX scintillation measurements, 2014-16')
    plt.grid(which='major', axis='both', color='green',linestyle='-', linewidth=0.5)
    plt.semilogy(mpho, np.multiply(1700,mscint), color='blue', marker='+', linestyle='None', label='Phase [rad] - scaled')
    plt.semilogy(mpho, mtec, marker='o', mec='red', mfc='None', linestyle='None', label='IP tec')
    plt.semilogy(mpho, miotec, color='chocolate', marker='x', linestyle='None', label='IO tec')
    plt.semilogy(mpho, mclock, color='black', marker='.', linestyle='None', label='H-maser clock')
    plt.semilogy([0,180],[20,20], color='red', label='Instr. error')				
    plt.xlim([0,180])
    plt.ylim([5,1e5])
    plt.xlabel('Orbital phase (deg)')
    plt.ylabel('Scintillation index (rad)')
    plt.legend(fontsize='medium')
    plt.show()
    
hmaser = np.zeros((len(vpho)))
hmaser[1:len(vpho)] = 0.112
if plot == 4:
    plt.figure()
    plt.hold(True)
    plt.title('Two-way IP TEC at the observed time - open circles\nMeasured Scint index, (scaled for a best fit) - blue +')
    plt.grid(which='major', axis='both', color='green',linestyle='-', linewidth=0.5)
    plt.semilogy(vpho, vscint, color='blue', marker='+', linestyle='None')
    plt.semilogy(vpho, np.multiply(vtecn,3.67e-4), marker='o', mec='red', mfc='None', linestyle='None')
    plt.semilogy(vpho, np.multiply(np.sum([viotec,vcetec], axis=0),1.5e-3), color='chocolate', marker='x', linestyle='None')
    plt.semilogy(tpho, np.multiply(tairm,0.02), color='brown', marker='x', linestyle='None')
 #   plt.semilogy(phonv, np.multiply(tecnv,3.67e-2), mec='red', marker='o', mfc='None', linestyle='None')
    plt.xlabel('Orbital phase (deg)')
    plt.ylabel('Scint index')
    plt.show()

    plt.subplot(2,1,2)
    plt.hist(sc3.code,np.arange(nstat)+1, color='red')
    plt.grid(True)
    plt.xticks(np.arange(nstat)+1,labels)    
    plt.xlim([1,nstat])
    plt.show()

# Summary plot of scintillation measurement
# a) Two-way interplanetary TEC at measurement times - red open circles
# b) Measured scintillation index (scaled for best fit IP+IO TEC) - blue's +
if plot == 1:
    plt.figure()
    plt.title('Summary plot of VEX scintillation measurements')
    plt.grid(which='major', axis='both', color='green',linestyle='-', linewidth=0.5)
    plt.semilogy(vpho, np.multiply(2400,vscint), color='blue', marker='+', linestyle='None', label='Phase [rad] - scaled')
    plt.semilogy(vpho, vtecn, marker='o', mec='red', mfc='None', linestyle='None', label='IP tec')
    plt.semilogy(vpho, viotec, color='chocolate', marker='x', linestyle='None', label='IO tec')
    plt.semilogy(tpho, tairm, color='brown', marker='+', linestyle='None', label='Airmass')
    plt.xlim([0,180])
    plt.ylabel('Phase scintillation')
    plt.xlabel('Orbital phase (deg)')    
    plt.legend(fontsize='small')
    plt.show()

    plt.figure()
    plt.title('Summary plot of MEX scintillation measurements')
    plt.hold(True)
    plt.title('Two-way phase scintillation for Mars orbit')
    plt.grid(which='major', axis='both', color='green',linestyle='-', linewidth=0.5)
    plt.semilogy(mpho, np.multiply(1700,mscint), color='blue', marker='+', linestyle='None', label='Phase [rad] - scaled')
    plt.semilogy(mpho, mtec, marker='o', mec='red', mfc='None', linestyle='None', label='IP tec')
    plt.semilogy(mpho, miotec, color='chocolate', marker='x', linestyle='None', label='IO tec')
    plt.xlim([0,180])
    plt.xlabel('Orbital phase (deg)')
    plt.ylabel('Scintillation index (rad)')
    plt.legend(fontsize='medium')
    plt.show()
    
# VEX scintillation data summary plot    
# a) Two-way interplanetary TEC at measurement times - red open circles
# b) Measured scintillation index (scaled for best fit IP+IO TEC) - blue's +
# c) Two-way ionospheric TEC (for Cebrerors and receiving station) - brown's x
hmaser = np.zeros((len(vpho)))
hmaser[1:len(vpho)] = 0.112
if plot == 0:
    plt.figure()
    plt.hold(True)
    plt.title('Two-way IP TEC at the observed time - open circles\nMeasured Scint index, (scaled for a best fit) - blue +')
    plt.grid(which='major', axis='both', color='green',linestyle='-', linewidth=0.5)
    plt.semilogy(vpho, vscint, color='blue', marker='+', linestyle='None')
    plt.semilogy(vpho, np.multiply(vtecn,3.67e-4), marker='o', mec='red', mfc='None', linestyle='None')
    plt.semilogy(vpho, np.multiply(np.sum([viotec,vcetec], axis=0),1.5e-3), color='chocolate', marker='x', linestyle='None')
    plt.semilogy(tpho, np.multiply(tairm,0.02), color='brown', marker='x', linestyle='None')
 #   plt.semilogy(phonv, np.multiply(tecnv,3.67e-2), mec='red', marker='o', mfc='None', linestyle='None')
    plt.xlabel('Orbital phase (deg)')
    plt.ylabel('Scint index')
    plt.xlim([0,180])
    plt.show()
				
#==============================================================================
# Print out a summary of the data
#
#==============================================================================

print('\033[92m' + 'Two-way interplanetary TEC: min ' + np.str(np.min(vtecn)) + ' - ' + np.str(np.max(vtecn)) + ' rad\n' + '\033[0m')
print('Other contributions\n')
print('\033[92m' + 'Two-way Allan variance of H-maser : 0.112 rad / 2.14 ps / 7.1e-15' + '\033[0m')
print('\033[92m' + 'Two-way ionospheric TEC           : ' + np.str(np.mean(np.sum([viotec,vcetec], axis=0))/1000.) + ' rad' + ' - ' + np.str(np.mean(np.sum([viotec,vcetec], axis=0))/(1000*2*np.pi*8.4e9)) + ' ps \033[0m')

if plot == 0:
    plt.figure()
    plt.hold(True)
    plt.title('Two-way IP TEC at the observed time - open circles\nMeasured Scint index, (scaled for a best fit) - blue +')
    plt.grid(which='major', axis='both', color='green',linestyle='-', linewidth=0.5)
    plt.semilogy(mpho, mDnoise, color='blue', marker='+', linestyle='None')
    plt.semilogy(mpho, np.multiply(mtec,3.67e-2), marker='o', mec='red', mfc='None', linestyle='None')
    plt.semilogy(mpho, np.multiply(np.sum([miotec,mcetec], axis=0),1.5e-3), color='chocolate', marker='x', linestyle='None')
    plt.xlabel('Orbital phase (deg)')
    plt.ylabel('Scint index')
    plt.xlim([0,180])
    plt.show()
#==============================================================================
# Print out a summary of the data
#
#==============================================================================

print('\033[92m' + 'Two-way interplanetary TEC: min ' + np.str(np.min(vtecn)) + ' - ' + np.str(np.max(vtecn)) + ' rad\n' + '\033[0m')
print('Other contributions\n')
print('\033[92m' + 'Two-way Allan variance of H-maser : 0.112 rad / 2.14 ps / 7.1e-15' + '\033[0m')
print('\033[92m' + 'Two-way ionospheric TEC           : ' + np.str(np.mean(np.sum([viotec,vcetec], axis=0))/1000.) + ' rad' + ' - ' + np.str(np.mean(np.sum([viotec,vcetec], axis=0))/(1000*2*np.pi*8.4e9)) + ' ps \033[0m')

#==============================================================================
# Determine a linear fit for a relation between measured Doppler noise 
# and scintillation index 
#
#==============================================================================
# Venus data
vscint   = np.append(sc1.PhSc, sc2.PhSc)
vdn      = np.append(sc1.Dnoise, sc2.Dnoise)
vSNR     = np.append(sc1.DopSNR, sc2.DopSNR)

Lsdv     = np.polyfit(vscint,vdn,1)
vdncf    = Lsdv[1] + np.multiply(Lsdv[0],vscint)

# Goodness of the fit
vdnbcfit = np.std(vdn-vdncf)/np.sqrt(np.size(vscint))

print('\033[94m bias  : ' + str(Lsdv[1]) + ' mHz \n slope : '  + str(Lsdv[0]) + ' mHz per randian \033[0m \n gfit  : ' + str(vdnbcfit))

# Mars data
mscint   = np.append(sc3.PhSc, sc4.PhSc)
mdn      = np.append(sc3.Dnoise, sc4.Dnoise)
mSNR     = np.append(sc3.DopSNR, sc4.DopSNR)

Lsdm     = np.polyfit(mscint,mdn,1)
mdncf    = Lsdm[1] + np.multiply(Lsdm[0],mscint)

mdnfit = np.std(mdn-mdncf)/np.sqrt(np.size(mscint))

print('\033[94m bias  : ' + str(Lsdm[1]) + ' mHz \n slope : '  + str(Lsdm[0]) + ' mHz per randian \n gfit  : ' + str(mdnfit))

#==============================================================================
# Relation between the measured Doppler/RadialVelocity noise and scintillation index measurements. 
# VEX - blue, MEX - red, PCal - black
#==============================================================================
Cdv = (1e3*3e8/(8.42e9)*1/np.sqrt(6)*0.5)
if plot == 5:
    plt.figure()
    plt.subplot(1,2,1)
    plt.hold(True)
    plt.grid(which='major', axis='both', color='green')
    plt.plot(vscint,vdn, color='blue', marker='+', linestyle='None')
    plt.plot(mscint,mdn, color='red', marker='+', linestyle='None')
    plt.plot(vscint,vdncf, color='blue')
    plt.plot(mscint,mdncf, color='red')
    plt.plot(scpc,dnpc, color='black', marker='+')
    plt.xlabel('Scintillation index [rad]')
    plt.ylabel('Doppler noise [mHz in 10s]')
    plt.subplot(1,2,2)
    plt.hist(vdn*Cdv,np.arange(20))
    plt.show()

#==============================================================================
# Doppler noise (mHz in 10 seconds) and Scintillation index (radians in 300 seconds) vs. Carrier detection SNR in 0.4 Hz
# VEX - blue, MEX - red
# These plots show that measured Scintillation index and Doppler noise practically do not depend on SNR and antenna size    
#==============================================================================
if plot == 6:
    plt.figure()
    plt.subplot(2,1,1)
    plt.title('Doppler noise (mHz in 10s) and Scintillation index (rad in 300s) \n vs. Carrier detection SNR in 0.4 Hz VEX - blue, MEX - red')
    plt.hold(True)
    plt.grid(which='minor', axis='both', color='green')    
    plt.loglog(vSNR,vscint, color='blue', marker='+', linestyle='None')
    plt.loglog(mSNR,mscint, color='red', marker='+', linestyle='None')
    plt.xlabel('Carrier detection SNR')
    plt.ylabel('Scintillation index')
    plt.ylim([0.01,10])
    plt.subplot(2,1,2)
    plt.hold(True)
    plt.grid(which='minor', axis='both', color='green')        
    plt.loglog(vSNR,vdn, color='blue', marker='x', linestyle='None')
    plt.loglog(mSNR,mdn, color='red', marker='x', linestyle='None')   
    plt.xlabel('Carrier detection SNR')
    plt.ylabel('Doppler Noise')
    plt.ylim([0.1,100])

    plt.figure()
    plt.subplot(2,1,1)
    plt.title('Doppler noise (mHz in 10 seconds) and Carrier detection SNR \n vs. Station number (VEX - blue, MEX - red)')
    plt.hold(True)
    plt.grid(which='minor', axis='both', color='green')    
    plt.semilogy(sc1.code+sc2.code,vscint, color='blue', marker='+', linestyle='None')
    plt.semilogy(sc3.code,mscint, color='red', marker='+', linestyle='None')
    plt.xlabel('Carrier detection SNR')
    plt.ylabel('Scintillation index')
    plt.xticks(np.arange(18)+1,labels)
    plt.ylim([0.01,10])
    plt.xlim([0,19])    
    plt.subplot(2,1,2)
    plt.hold(True)
    plt.grid(which='minor', axis='both', color='green')    
    plt.semilogy(sc1.code+sc2.code,vdn, color='blue', marker='x', linestyle='None')
    plt.semilogy(sc3.code,mdn, color='red', marker='x', linestyle='None')
    plt.xlabel('Station')
    plt.ylabel('Doppler Noise')
    plt.xticks(np.arange(18)+1,labels)
    plt.ylim([0.1,100])
    plt.xlim([0,19])
    
#==============================================================================
# Plotting scintillation values in the region closer to the Sun
#==============================================================================
if  plot==7:
	plt.figure()
	Rs = 0.0046491
	AU = 1
	r  = np.arange(-30.,30.,0.22)
	so = np.tan(r*Rs)*180/np.pi
	Ne = 100*np.divide(0.10e4,np.power(r,4)) + np.divide(0.22e3,np.power(r,2))
	plt.semilogy(so, Ne, color='black', marker='.')
	plt.xlabel('Solar radii')
	plt.ylabel('Electron density (cm^{-3})')
	plt.xlim([-8,8])
	plt.show()
	
	plt.figure()
	tmp = np.multiply(sc2.SOT[847:1093],-1)
	tmp = np.concatenate((tmp,sc2.SOT[1093:]),axis=0)
	tmp2 = np.multiply(sc0.SOT[74:98],-1)
	tmp2 = np.concatenate((tmp2,sc0.SOT[98:147]),axis=0)
	plt.title('VEX rms 2-way range rate residual')
	#plt.plot(tmp,sf.hz2mm(8.41599e9,sc2.Dnoise[847:]), color='red', marker='x', linestyle='None')
	plt.plot(tmp,sc2.PhSc[847:], color='red', marker='x', linestyle='None', label='2014')
	plt.plot(tmp2,sc0.PhSc[74:147], color='blue', marker='x', linestyle='None', label='2010')
	plt.hold
	plt.plot(so, 10*np.log10(Ne), color='cyan')
	plt.xlabel('Angle Sun-Earth-VEX - degrees')
	plt.ylabel('RMS range rate resdiuals')
	plt.text(0.1,-8,'    INGRESS               -------                 EGRESS', horizontalalignment='center', verticalalignment='center')
	plt.xlim([-8,8])
	plt.legend()
	plt.grid('on')
	plt.show()
				
	plt.figure()
	plt.title('MEX rms 2-way range rate residual')
	plt.xlabel('Angle Sun-Earth-MEX in degrees')
	plt.ylabel('RMS range rate residuals')
	plt.legend()
	plt.grid('on')
	plt.show()