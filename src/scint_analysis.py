#!$(which python)
"""
    Created on Fri Jul  4 11:44:23 2014
    
    Analysys function to estimate the scintillation coefficients and values
    @author: molera
    """
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sps

expr = 'v0715'
station = 'Ht'

'''
    read the file with the phases
    1st column time stamps
    other columns phase in radians
    '''
def read_phases(phase_file):
    phase = np.loadtxt(phase_file,skiprows=0)
    return phase

'''
    Return the staion code 
    '''
def station_code(antenna_code):
    antenna = ['Mh','Mc','Ma','Nt','Wz','Ys','Pu','On','Hh','Sh','Km','Ur','Ht','Sv','Zc','Bd','T6','Ww']
    code = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
    codeInd = code[antenna.index(antenna_code)]

    return codeInd

class scintAnal(object):
    def __init__(self, dataDir, phaseFn):
        self.phase_file = dataDir + phaseFn
        self.code = station_code(phaseFn[-6:-4])
        self.dd = phaseFn[18:20]
        self.mm = phaseFn[15:17]
        self.yy = phaseFn[10:14]
        self.summ_file =  dataDir + phaseFn[0:7] + 'sum.' + phaseFn[7:20] + '.' + phaseFn[-6:-4] + '.txt'

        self.fss = 0.003
        self.fse = 3.003
        self.fns = 4.
        self.fne = 7.
        self.flo = 0.003
        self.fhi = 0.25

        self.verbose = 0
        self.base = 100
        self.expt = 50

    def process_scintillation(self):
        phase = read_phases(self.phase_file)
        phshape = np.shape(phase)
        nph = phshape[1] - 1
        lph = phshape[0]
        
        ph = np.zeros((lph,nph))
        tt = np.zeros((lph,1))

        tt = phase[:,0]
        ph = phase[:,1:]

        # Calculate standard stadistics for each phase scan
        dtp = (tt[1]-tt[0])
        Ts  = dtp*lph
        df  = 1/Ts
        BW  = lph * df
        ff  = np.arange(0,BW,df)
        
        print 'Max phase: ' + str(np.max(ph))  + ' Min phase: ' + str(np.min(ph))
        print 'Avg phase: ' + str(np.mean(ph)) + ' Std phase: ' + str(np.std(ph))        

        '''
            # Calculate the power spectra in 2 different ways:
            # 1 Std power spectra and normalize by the bandwidth
            # 2 Calculate the spectra using a cosine window
            '''
        sp  = np.fft.fft(ph,axis=0)
        psp = np.power(np.abs(sp),2)
        psp = 2*psp/BW

        wcos = sps.cosine(lph)
        tmp = np.zeros((lph,nph))
        for ip in np.arange(nph):
            tmp[:,ip] = np.multiply(ph[:,ip],wcos)

        spw = np.fft.fft(tmp,axis=0)
        pspw = np.power(np.abs(spw),2)
        pspw = 2*pspw/BW

        # Plot the power spectra in units of rad2 per Hz
        pspa  = np.multiply(1./nph,np.sum(psp, axis=1))
        pspaw = np.multiply(1./nph,np.sum(pspw, axis=1))

        if self.verbose == 1:
            plt.loglog(ff,pspa,'r')
            plt.loglog(ff,pspaw,'b')
            plt.xlim(0.001,10)
            plt.show()

        '''
            # Compute the system noise level
            # System noise mean (snm) estimated as the mean value in noise band
            # Peaks of the scintillation is at 3 mHz (PeakScint) 
            '''
        bss  = np.floor(self.fss/df + 1.5)
        bse  = np.floor(self.fse/df + 1.5)        
        bns  = np.floor(self.fns/df + 1.5)
        bne  = np.floor(self.fne/df + 1.5)
        bbeg = np.floor(self.flo/df + 0.5)
        bend = np.floor(self.fhi/df + 0.5)

        snm  = np.mean(pspaw[bns:bne])
        PeakScint = pspaw[bbeg]*1e-4

        print 'System noise : ' + str(snm)
        print 'Peak Scint   : ' + str(PeakScint)
        
        ffs  = ff[bbeg:bend]
        pss  = pspaw[bbeg:bend]
        Lffs = np.log10(ffs)
        Lpss = np.log10(pss)

        Lf     = np.polyfit(Lffs,Lpss,1)
        Lfit   = Lf[1] + Lf[0]*Lffs
        dLfit  = Lpss - Lfit
        rdLfit = np.std(dLfit)
                            
        Slope  = Lf[0]
        errSlope = np.divide(rdLfit,(np.max(Lffs)-np.min(Lffs)))
                            
        print 'Slope        : ' + str(Slope)
        print 'Error Slope  : ' + str(errSlope) + '\n'

        '''
            # Estimate the phase scint in the scintillation band
            # and from the noise band
            '''                    
        FiltScint = np.zeros(lph)
        FiltNoise = np.zeros(lph)        
        
        FiltScint[bss:bse] = 2
        FiltNoise[bns:bne] = 2
        spsc = np.zeros((lph,nph),dtype=np.complex64)
        spno = np.zeros((lph,nph),dtype=np.complex64)
                        
        for ip in np.arange(nph):
            spsc[:,ip] = np.multiply(sp[:,ip],FiltScint)
            spno[:,ip] = np.multiply(sp[:,ip],FiltNoise)
                            
        phs = np.real(np.fft.ifft(spsc,axis=0))
        phn = np.real(np.fft.ifft(spno,axis=0))
                            
        rmsphs = np.std(phs,axis=0)
        rmsphn = np.std(phn,axis=0)
                            
        rmsphsc = np.sqrt(np.power(rmsphs,2) - np.power(rmsphn,2))
        print 'Phase scint  : ' + np.str(np.mean(rmsphsc))
        print 'Phase stand  : ' + np.str(np.std(rmsphsc))
        print rmsphs
        data_out = np.zeros((nph,14))
            
        for ip in np.arange(nph):
            data_out[ip] = [self.base + int(ip+1), self.expt, int(ip+1), self.code, self.yy ,self.mm, self.dd, 1140, rmsphs[ip], rmsphn[ip], Slope, errSlope, PeakScint, snm]
                
        np.savetxt(self.summ_file, data_out, fmt='%d %d %d %d %d %d %d %d %.3f %.3f %.3f %.3f %.3f %.3f', delimiter=' ', newline='\n', header='Ns Se Sc An YY MM DD  Ts   PSc   NSc   Slo   Err   Peak  Noise')

dataDir = '/Users/molera/Treball/SCtracking/data/' + expr[0] + '14' + expr[1:5] + '/'
phaseFn = 'Phases.vex2014.' + expr[1:3] + '.' + expr[3:5] + '.' + station + '.txt'

scint = scintAnal(dataDir,phaseFn)
scint.process_scintillation()