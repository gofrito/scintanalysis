i#!$(which python)
from numpy import *
import matplotlib.pyplot as plt


# Definition of the initial variables
AU = 149597870.700		# km
TECu = 10^16			# m^-2
Rsun = 696342			# km

Roe = AU				# Earth
Roy = 0.387098*AU		# Mercury
Rov = 0.723327*AU		# Venus
Rom = 1.523679*AU		# Mars
Roj = 5.204267*AU		# Jupiter
Roh = 100*AU			# Heliopause

# Building orbital positions in a 2D map
Np = 180
jp = range(1,Np)
dp = pi/180
p  = (jp+0.5)*dp
po = jp + 0.5
xp = cos(p)
yp = sin(p)
Vee = Roe*(-1,0)

# How to calculate the SOT
def Sot(ve,vp):
    Sot = acos((-ve/mdof(ve))*((vp-ve)/modf(vp-ve)))

# Formulas for electron density for nominal, fast and slow wind models.
def nenom(dist):
  nnom  = 5^-6*(dist/AU)^2;
  return nnom

def nefast(dist):
  nfast = (1.155*(dist/Rsun)^-2 + 32.2*(dist/Rsun)^-4.39 + 3254*(dist/Rsun)^-16.25)*10^11
  return nfast

def neslow(dist):
  nslow = (4.1*(dist/Rsun)^-2 + 23.53*(dist/Rsun)^-2.7)*10^11 + (1.5*(dist/Rsun)^-6 + 2.99*(dist/Rsun)^-16)*10^14
  return nslow

# Computing the TEC
def Tdl(Npi,Ropp):
  for k in range(Np):
    Vp = (x*Ropp,yp*Ropp)
    oph = po
    sot = Sot(Vee,Vp)*180/pi
    dep = modf(Vee - Vp)
    uvp = (Vp - Vee)/dep
    ddp = dep/Npi
    tecin = 0
    tecif = 0
    tecis = 0
    for ji in range(Npi):
      vpi = Vp - uvp*ddp(ji+0.5)
      ds = modf(vpi)
      nn = nenom(ds)
      nf = nefast(ds)
      ns = neslow(ds)
      dtec = nn * ddp
      tecin = tecin + ddp*nn
      tecif = tecif + ddp*nf
      tecis = tecis + ddp*ns
    tecn = tecin/TECu
    tecf = tecif/TECu
    tecs = tecis/TECu
    distep = dep/AU
  return(oph,sot,distep,tecn,tecf,tecs)

# Analytical check of Earth planet TEC at the closest proximity  
def tprox(Roe,rop):
  tprox = (AU^2/Roe - AU^2/rop)*nenom(Roe)/TECu
  
  Ni = 30000
  ty = Tdl(Ni,Roy)
  tv = Tdl(Nv,Rov)
  tm = Tdl(Ni,Rom)
  tj = Tdl(Ni,Roj)
  th = Tdl(Ni,Roh)
  
  tyn  = ty(3)
  soty = ty(1)
  tvn  = tv(3)
  sotv = tv(1)
  tvf  = tv(4)
  tvs  = tv(5)
  tmn  = tm(3)
  sotm = tm(1)
  tjn  = tj(3)
  sotj = tj(1)
  thn  = th(3)
  soth = th(1)
  
  typ = -tprox(Roy)
  tvp = -tprox(Rov)
  tmp = tprox(Rom)
  tjp = tprox(Roj)
  thp = tprox(Roh)
  
  graph = 1
  
  #def Plotting Total Electron Content vs phase
  if graph == '1':
     plt.plot(po,log(tvm),'c')
     plt.plot(po,log(tmn),'b')
     plt.plot(po,log(tjn),'r')
     plt.plot(po,log(tyn),'g')
     plt.plot(po,log(thn),'k')
     plt.ylabel('TEC (rad^2/Hz)')
     plt.xlabel('Orbital phase (degrees)')
     plt.title('Total Electron Content vs. Orbital Phase')
     plt.show()
  if graph == '2':
     plt.plot(sotv,tvn,'c')
     plt.plot(sotm,tvm,'b')
     plt.plot(sotj,tvj,'r')
     plt.plot(sotn,tvn,'g')
     plt.plot(soth,tvh,'k')
     plt.ylabel('TEC (rad^2/Hz)')
     plt.xlabel('Solar elongation (degrees)')
     plt.title('Total Electron Content vs. Solar Elongation')
     plt.show()
  if graph == '3':
     plt.plot(sotv,log(tvn),'r')
     plt.plot(sotv,log(tvs),'g')
     plt.plot(sotv,log(tvf),'b')
     plt.ylabel('TEC (rad^2/Hz)')
     plt.xlabel('Solar elongation (degrees)')
     plt.title('Total Electron Content for different kind of solar wind')
     plt.show()