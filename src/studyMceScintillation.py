#!$(which python)
"""
    Created on Sun Mar 15 09:45:33 2015

    @author: molera
    """

import numpy as np
import matplotlib.pyplot as plt

vex1_file = '/Users/molera/Downloads/ScintObsSummaryTabVEX.Block1.103.txt'

with open(vex1_file) as f:
    f_lines = f.readlines()

scintTable = []
for line in f_lines:
   if len(line)>20 and line[0]!='/':
       line_parsed = map(float, line.replace('|',' ').split())
       scintTable.append(line_parsed)

scintTable = np.array(scintTable) 

#==============================================================================
#  CME epoch    -  Scintillation epoch
# 3 March 2010  - 1,2 and 4 March 2010
# 18 March 2010 - 21 March
#==============================================================================

scint2010 = scintTable[scintTable[:,4]==2010]
tempsc = scint2010[scint2010[:,5]==3]
tempsc2 = scintTable[scintTable[:,19] > 9]
print 'Data processed for the 3rd of March 2010'
if 0:
    plt.figure()
    plt.hold()
    plt.semilogy(tempsc2[tempsc2[:,19] < 15,19],tempsc2[tempsc2[:,19] < 15,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==1),19], tempsc[tempsc[:,6]==1,24], marker='x', color='blue', linestyle='None', label='1st March')
    plt.semilogy(tempsc[(tempsc[:,6]==2),19], tempsc[tempsc[:,6]==2,24], marker='x', color='black', linestyle='None', label='2nd March')
    plt.semilogy(tempsc[(tempsc[:,6]==4),19], tempsc[tempsc[:,6]==4,24], marker='x', color='purple', linestyle='None', label='4th March')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 3 March 2010')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')
    
tempsc2 = scintTable[scintTable[:,19] > 16]
print 'Data processed for the 18th of March 2010'
if 0:
    plt.figure()
    plt.hold()
    plt.semilogy(tempsc2[tempsc2[:,19] < 23,19],tempsc2[tempsc2[:,19] < 23,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==21),19], tempsc[tempsc[:,6]==21,24], marker='x', color='blue', linestyle='None', label='21st March')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 18 March 2010')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')

#==============================================================================
#  CME epoch    -  Scintillation epoch
# 21 April 2011
# 27 May 2011
# 16 October 2011
# 18,19 November 2011
# 2 March 2011
#==============================================================================
scint2011 = scintTable[scintTable[:,4]==2011]
tempsc = scint2011[scint2011[:,5]==4]
tempsc2 = scintTable[scintTable[:,19] > 27]
print 'Data processed for the 21st of April 2011'
if 0:
    plt.figure()
    plt.hold()
    plt.semilogy(tempsc2[tempsc2[:,19] < 34,19],tempsc2[tempsc2[:,19] < 34,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==18),19], tempsc[tempsc[:,6]==18,24], marker='x', color='blue', linestyle='None', label='18th April')
    plt.semilogy(tempsc[(tempsc[:,6]==21),19], tempsc[tempsc[:,6]==21,24], marker='x', color='black', linestyle='None', label='21st April')
    plt.semilogy(tempsc[(tempsc[:,6]==23),19], tempsc[tempsc[:,6]==23,24], marker='x', color='purple', linestyle='None', label='23rd April')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 21st April 2011')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')

tempsc = scint2011[scint2011[:,5]==5]
tempsc2 = scintTable[scintTable[:,19] > 19]
print 'Data processed for the 27th of May 2011'
if 0:
    plt.figure()
    plt.hold()
    plt.semilogy(tempsc2[tempsc2[:,19] < 28,19],tempsc2[tempsc2[:,19] < 28,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==24),19], tempsc[tempsc[:,6]==24,24], marker='x', color='blue', linestyle='None', label='24th May')
    plt.semilogy(tempsc[(tempsc[:,6]==26),19], tempsc[tempsc[:,6]==26,24], marker='x', color='black', linestyle='None', label='26th May')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 27th May 2011')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')

tempsc = scint2011[scint2011[:,5]==10]
tempsc2 = scintTable[scintTable[:,19] > 12]
print 'Data processed for the 16th of October 2011'
if 0:
    plt.figure()
    plt.hold()
    plt.semilogy(tempsc2[tempsc2[:,19] < 20,19],tempsc2[tempsc2[:,19] < 20,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==17),19], tempsc[tempsc[:,6]==17,24], marker='x', color='blue', linestyle='None', label='17th October')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 16th October 2011')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')
    
tempsc = scint2011[scint2011[:,5]==11]
tempsc2 = scintTable[scintTable[:,19] > 18]
print 'Data processed for the 18-19th November 2011'
if 0:
    plt.figure()
    plt.hold()
    plt.semilogy(tempsc2[tempsc2[:,19] < 28,19],tempsc2[tempsc2[:,19] < 28,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==14),19], tempsc[tempsc[:,6]==14,24], marker='x', color='blue', linestyle='None', label='14th November')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 18-19th November 2011')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')

#==============================================================================
#  CME epoch    -  Scintillation epoch
# 2 March 2012
#==============================================================================
scint2012 = scintTable[scintTable[:,4]==2012]
tempsc = scint2012[scint2012[:,5]==3]
tempsc2 = scintTable[scintTable[:,19] > 40]
print 'Data processed for the 2nd March 2012'
if 0:
    plt.figure()
    plt.hold()
    plt.semilogy(tempsc2[tempsc2[:,19] < 47,19],tempsc2[tempsc2[:,19] < 47,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==4),19], tempsc[tempsc[:,6]==4,24], marker='x', color='blue', linestyle='None', label='4 March')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 2nd of March 2012')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')
    
#==============================================================================
#  CME epoch    -  Scintillation epoch
# 7,8 March 2013
#==============================================================================
scint2013 = scintTable[scintTable[:,4]==2013]
tempsc = scint2013[scint2013[:,5]==3]
tempsc2 = scintTable[scintTable[:,19] > 1]
print 'Data processed for the 7th of March 2013'
if 0:
    plt.figure()
    plt.hold()
    plt.semilogy(tempsc2[tempsc2[:,19] < 9,19],tempsc2[tempsc2[:,19] < 9,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==7),19], tempsc[tempsc[:,6]==7,24], marker='x', color='blue', linestyle='None', label='7 March')
    plt.semilogy(tempsc[(tempsc[:,6]==8),19], tempsc[tempsc[:,6]==8,24], marker='x', color='black', linestyle='None', label='8 March')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 7th of March 2013')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')
    
#==============================================================================
#  CME epoch    -  Scintillation epoch
# 20 February 2014
# 9,10 May 2014
# 24,26 September 2014
#==============================================================================
vex2_file = '/Users/molera/Downloads/ScintObsSummaryTabVEX.Block2.047.txt'
with open(vex2_file) as f:
    f_lines = f.readlines()

scintTable = []
for line in f_lines:
   if len(line)>20 and line[0]!='/':
       line_parsed = map(float, line.replace('|',' ').split())
       scintTable.append(line_parsed)

scintTable = np.array(scintTable)
scint2014 = scintTable[scintTable[:,4]==2014]
tempsc = scint2014[scint2014[:,5]==2]
tempsc2 = scintTable[scintTable[:,22] > 36]
print 'Data processed for the 20th February 2014'
if 0:
    plt.figure()
    plt.hold()
    plt.semilogy(tempsc2[tempsc2[:,22] < 45,22],tempsc2[tempsc2[:,22] < 45,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==14),22], tempsc[tempsc[:,6]==14,24], marker='x', color='blue', linestyle='None', label='14 February')
    plt.semilogy(tempsc[(tempsc[:,6]==17),22], tempsc[tempsc[:,6]==17,24], marker='x', color='black', linestyle='None', label='17 February')
    plt.semilogy(tempsc[(tempsc[:,6]==21),22], tempsc[tempsc[:,6]==21,24], marker='x', color='purple', linestyle='None', label='21 February')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 20th February 2014')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')


vex3_file = '/Users/molera/Downloads/ScintObsSummaryTabVEX.Block3.f3.4.167i.txt'
with open(vex3_file) as f:
    f_lines = f.readlines()

scintTable = []
for line in f_lines:
   if len(line)>20 and line[0]!='/':
       line_parsed = map(float, line.replace('|',' ').split())
       scintTable.append(line_parsed)

scintTable = np.array(scintTable)
scint2014 = scintTable[scintTable[:,4]==2014]
tempsc = scint2014[scint2014[:,5]==5]
tempsc2 = scintTable[scintTable[:,22] > 39]
print 'Data processed for the 9 and 10th of May 2014'
if 0:
    plt.figure()
    plt.semilogy(tempsc2[tempsc2[:,22] < 45,22],tempsc2[tempsc2[:,22] < 45,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==04),22], tempsc[tempsc[:,6]==04,24], marker='x', color='blue', linestyle='None', label='4 May')
    plt.semilogy(tempsc[(tempsc[:,6]==05),22], tempsc[tempsc[:,6]==05,24], marker='x', color='black', linestyle='None', label='5 May')
    plt.semilogy(tempsc[(tempsc[:,6]==11),22], tempsc[tempsc[:,6]==11,24], marker='x', color='purple', linestyle='None', label='11 May')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 9-10 May 2014')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')
    
tempsc = scint2014[scint2014[:,5]==9]
tempsc2 = scintTable[scintTable[:,22] > 5]
print 'Data processed for the 24-26 of September 2014'
if 1:
    plt.figure()
    plt.semilogy(tempsc2[tempsc2[:,22] < 13,22],tempsc2[tempsc2[:,22] < 13,24], marker='x', color='red', linestyle='None')
    plt.semilogy(tempsc[(tempsc[:,6]==21),22], tempsc[tempsc[:,6]==21,24], marker='x', color='blue', linestyle='None', label='21 September')
    plt.semilogy(tempsc[(tempsc[:,6]==22),22], tempsc[tempsc[:,6]==22,24], marker='x', color='black', linestyle='None', label='22 September')
    plt.semilogy(tempsc[(tempsc[:,6]==23),22], tempsc[tempsc[:,6]==23,24], marker='x', color='purple', linestyle='None', label='23 September')
    plt.semilogy(tempsc[(tempsc[:,6]==25),22], tempsc[tempsc[:,6]==25,24], marker='x', color='green', linestyle='None', label='25 September')
    plt.semilogy(tempsc[(tempsc[:,6]==26),22], tempsc[tempsc[:,6]==26,24], marker='x', color='cyan', linestyle='None', label='26 September')
    plt.legend(fontsize='medium')
    plt.grid(which='minor', axis='both', color='green',linestyle='-', linewidth=0.1)
    plt.title('CME epoch: 24-26 September 2014')
    plt.ylabel('Scint [rad]')
    plt.xlabel('SOT (deg)')