#!$(which python)
"""
    Created on Fri Jul  4 11:44:23 2014

    Plot results from the ef026 experiment
    @author: molera
    """
# Import required tools
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sps

def read_fdets(fdets_file):
    fdets = np.loadtxt(fdets_file,skiprows=4)
    return fdets
 
def read_phases(phase_file):
    phase = np.loadtxt(phase_file,skiprows=0) 
    return phase
    
def station_code(antenna_code):
    antenna = ['Mh','Mc','Ma','Nt','Wz','Ys','Pu','On','Hh','Sh','Km','Ur','Ht','Sv','Zc','Bd','T6','Ww']
    code = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
    codeInd = code[antenna.index(antenna_code)]

    return codeInd
    
class cr(object):
    def __init__(self, dataDir,fdetsFn, phaseFn,files):
        self.fdets_file = dataDir + fdetsFn
        self.phase_file = dataDir + phaseFn
        self.summ_file =  dataDir + phaseFn[0:7] + 'sum.' + phaseFn[7:-8] + 'txt'
        self.fss = 0.003
        self.fse = 3.003        
        self.fns = 4.
        self.fne = 7.
        self.flo = 0.003
        self.fhi = 0.2
        self.tt = np.array([])
        self.ph = np.array([])
        self.verbose = 1
        self.nphases = len(files)*3.
        self.Ns = 0
        self.base = 100
        self.expt = 50
        
    def plot_fdets(self):
        self.fdets = read_fdets(self.fdets_file)
        tt = self.fdets[:,1]
        fdet = self.fdets[:,4]
        rfit = self.fdets[:,5]
        ax1 = plt.subplot2grid((3,1),(0,0))
        ax1.plot(tt,fdet,'r.')
        plt.xlim([np.min(tt),np.max(tt)])
        ax2 = plt.subplot2grid((3,1),(1,0))
        ax2.plot(tt,rfit,'b.')
        plt.tick_params(bottom='off')
        plt.xlim([np.min(tt),np.max(tt)])
        ax3 = plt.subplot2grid((3,1),(2,0))
        ax3.plot(tt,self.fdets[:,2],'r')
        plt.xlim([np.min(tt),np.max(tt)])
        plt.xlabel('Time [s]')
        plt.tight_layout()
        plt.draw()
        plt.show()
        
        print 'Std deviation of the fit :' + str(np.std(self.fdets[:,5]))
        print 'Total Doppler frequency  :' + str(np.abs(self.fdets[-1,4] - self.fdets[0,4]))
        print 'Average Signal2Noise rate:' + str(np.mean(self.fdets[:,2]))
        
    def plot_phases(self):
        self.phase = read_phases(self.phase_file)
        tt = self.phase[:,0]
        ph = self.phase[:,1]
        plt.plot(tt,ph,'b')
        plt.xlabel('Time [s]')
        plt.ylabel('Phase [rad]')
        plt.show()

    def split_phases(self,jp):  # For this experiment the sc was recorded for 1 hour and not 19 min
        if (jp <10):      
            self.phase = read_phases(self.phase_file[0:-5] + str(jp) + '.txt')
        else:
            self.phase  = read_phases(self.phase_file[0:-6] + str(jp) + '.txt')
        
        # We split the phase array in 3 parts.
        # ttsize = len(self.phase)
        # ttsplit = int(np.floor(ttsize/3))
        ttsplit = 46467
        
        ph = np.zeros((ttsplit,3))
        tt = np.zeros((ttsplit,3))

        if jp != 13:
            for ip in range(3):
                tt[:,ip] = self.phase[0+ip*ttsplit:ttsplit+ip*ttsplit,0]
                ph[:,ip] = self.phase[0+ip*ttsplit:ttsplit+ip*ttsplit,1]
        else:
            for ip in range(2):
                tt[:,ip] = self.phase[0+ip*ttsplit:ttsplit+ip*ttsplit,0]
                ph[:,ip] = self.phase[0+ip*ttsplit:ttsplit+ip*ttsplit,1]
            
        if self.verbose == 1:
            plt.plot(ph[:,0],'b')
            plt.plot(ph[:,1]+1,'r')
            plt.plot(ph[:,2]-1,'g')
            plt.xlabel
            plt.xlabel('Time [s]')
            plt.ylabel('Phase [rad]')
            plt.show()
        
        print np.shape(ph)
        if self.Ns == 0:
            self.ph = ph
            self.tt = tt[:,0]
        else:
            self.ph = np.append(self.ph,ph, axis=1)
            
        self.Ns = self.Ns+3
        
    def process_scintillation(self):
        # Calculate standard stadistics for each phase scan
        lph = len(self.ph)
        dtp = (self.tt[1]-self.tt[0])
        Ts  = dtp*lph
        df  = 1/Ts
        BW  = lph * df
        ff  = np.arange(0,BW,df)
        Phx = np.max(self.ph)
        Phn = np.min(self.ph)
        Phr = np.std(self.ph)
        Phm = np.mean(self.ph)
        
        # Make first the power spectra
        # and normalize by the bandwidth
        sp  = np.fft.fft(self.ph,axis=0)
        psp = np.power(np.abs(sp),2)    
        psp = 2*psp/BW
        
        # Estimate the window spectra using a cosine window.
        # I used scipy.cosine although i could use also 
        #np.cos(np.pi/12*(np.arange(12)-0.5*np.arange(12))
        tph  = sps.cosine(lph)
        tmp = np.zeros((lph,self.nphases))

        for ip in np.arange(self.nphases):
            tmp[:,ip] = np.multiply(self.ph[:,ip],tph)

        spw = np.fft.fft(tmp,axis=0)
        pspw = np.power(np.abs(spw),2)
        pspw = 2*psp/BW
        
        # Plot the power spectra in units of rad2 per Hz
        pspa  = np.multiply(1/self.nphases,np.sum(psp, axis=1))
        pspaw = np.multiply(1/self.nphases,np.sum(pspw, axis=1))
        
        plt.loglog(ff,pspa,'r')
        plt.loglog(ff,pspaw,'b')
        plt.xlim(0.0001,10)
        plt.show()
        
        # Compute the system noise level 
        bns = self.fns/df
        bne = self.fne/df
        
        # System noise mean (snm) estimated as the mean value in noise band
        snm = np.mean(pspaw[bns:bne])
        
        bbeg = np.floor(self.flo/df + 0.5)
        bend = np.floor(self.fhi/df + 0.5)
        
        # Calculate the Peak of the scintillations (PeakScint)
        # as the power spectra windowed
        PeakScint = pspaw[bbeg]
        
        ffs = ff[bbeg:bend]
        pss = pspaw[bbeg:bend]
        Lffs = np.log10(ffs)
        Lpss = np.log10(pss)
        
        Lf = np.polyfit(Lffs,Lpss,1)
        Lfit = Lf[1] + Lf[0]*Lffs
        
        plt.plot(Lffs,Lpss,'r')
        plt.plot(Lffs,Lfit,'b')
        plt.show()
        
        dLfit = Lpss - Lfit
        rdLfit = np.std(dLfit)
        
        Slope = Lf[0]
        errSlope = np.divide(rdLfit,(np.max(Lffs)-np.min(Lffs)))
        
        print 'Slope       : ' + str(Slope)
        print 'Error Slope : ' + str(errSlope)
        
        FiltScint = np.zeros(lph)
        FiltNoise = np.zeros(lph)
        bss = np.floor(self.fss/df + 0.5)
        bse = np.floor(self.fse/df + 0.5)
        bns = np.floor(self.fns/df + 0.5)
        bne = np.floor(self.fne/df + 0.5)
        
        FiltScint[bss:bse] = 2
        FiltNoise[bns:bne] = 2
        spsc = np.zeros((lph,self.nphases),dtype=np.complex64)
        spno = np.zeros((lph,self.nphases),dtype=np.complex64)
        phs = np.zeros((lph,self.nphases))
        
        for ip in np.arange(self.nphases):    
            spsc[:,ip] = np.multiply(sp[:,ip],FiltScint)
            spno[:,ip] = np.multiply(sp[:,ip],FiltNoise)

        phs = np.real(np.fft.ifft(spsc,axis=0))
        phn = np.real(np.fft.ifft(spno,axis=0))

        rmsphs = np.std(phs,axis=0)
        rmsphn = np.std(phn,axis=0)
                
        rmsphsc = np.sqrt(np.power(rmsphs,2) - np.power(rmsphn,2))
        code = station_code(self.phase_file[-11:-9])
        dd = self.phase_file[-14:-12]
        mm = self.phase_file[-17:-15]
        yy = self.phase_file[-22:-18]
        data_out = np.zeros((self.nphases,14))
        
        for ip in np.arange(self.nphases):
            data_out[ip] = [self.base + int(ip) + 1, self.expt, int(ip) + 1, code, yy ,mm, dd, 1140, rmsphsc[ip], rmsphn[ip], Slope, errSlope, PeakScint*1e-4, snm]
        
        np.savetxt(self.summ_file, data_out, fmt='%d %d %d %d %d %d %d %d %.3f %.3f %.3f %.3f %.3f %.3f', delimiter=' ', newline='\n', header=' NSc NSe Sc An YYYY MM DD  Ts  PSc  NSc  Slo  Err  Peak Noise')

station = 'Wz'
dataDir= '/Users/molera/Treball/SCtracking/data/ef026/' + station + '/'
fdetsFn = 'Fdets.vex2014.06.16.' + station + '.0001.r2i.txt'
phaseFn = 'Phases.vex2014.06.16.' + station + '.0001.txt'
files = [1, 4, 7, 10, 13]
if station == 'Wz':
	files = [4, 7, 10, 13]
ef026 = cr(dataDir,fdetsFn,phaseFn,files)
#ef026.plot_fdets()
ef026.plot_phases()
for ip in files:
    ef026.split_phases(ip)
    
ef026.process_scintillation()
