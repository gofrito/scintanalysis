%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATLAB tools to analyze Interplanetary Plasma  %
% scintillation observations with VEX            %
%                                                %
% show_scint_results.m - G. Molera               %
% Read the results of the observations file      %
% Input: filename, Numero Samples,               %
%        Flag=1 save to file Flag=0 in columns   %
% Output: Data is sorted in columns              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Reading the Observation Table 
fprintf('1 - Remember to update the filename and number of samples\n');
filename  = '~/Treball/Scintillation/results/ScintObsSummaryTab_r101.txt';
nrsamples = 501;
flag      = 0;

% The file is a generic text file .txt
out     = read_scint_table(filename,nrsamples,flag);

SOT     = out.SOT;
phsc    = out.phsc;
phn     = out.phn;
frq     = out.frq;
%station = out.station;
nstation= out.nstation;
ScSlope = out.ScintSlope;

LSOT    = log10(SOT);
Lphsc   = log10(phsc);
Lc      = polyfit(LSOT,Lphsc,1);
Lff     = 0:0.01:2;
Lfit    = Lc(2) + Lc(1).*(Lff);
ff      = 10.^(Lff);
fit     = 10.^(Lfit);
stdfit  = std(phsc);


dev(1:nrsamples)=0;
% Checking the individual deviation.
for ii=1:nrsamples
    dev(ii) = Lphsc(ii) - (Lc(2) + Lc(1).*log10(SOT(ii)));    
end

sigmadev = std(dev);
fprintf('sigmadev: %6f2\n',sigmadev);
ser = sigmadev/(std(LSOT)*sqrt(nrsamples-2));
fprintf('Slope determination error: %6f3\n',ser);

% Plotting the results 
subaxis(2,2,3);
semilogx(SOT,dev,'.');grid on;hold on;axis([1 100 -0.4 0.6]);
semilogx(1:0.1:100,var(Lphsc),'r-');semilogx(0:0.1:100,-var(Lphsc),'r-');
title('Deviations in log-log');
xlabel('Sun-Observer-Target (degrees)');
ylabel('Deviation from fit line');
fprintf('\n Scintillation Slope respect SOT is: %f \n',Lc(1));

subaxis(2,2,4);
%histfit(dev);axis([-0.75 0.75 0 40]);
title('Histogram of deviations');
xlabel('Deviation');
ylabel('Counts');

figure(1);
subplot(2,1,1);% plot(LSOT,Lphsc,'o');hold on;grid on; axis([0 2 -2 3]);
title('Phase Scintillation RMS vs Sun-Observer-Target');
xlabel('Sun-Observer-Target (degrees)');
ylabel('Phase Scintilaltion RMS (radians)');
plot(Lff,Lfit,'r');%set(gca,'XTickLabel',{'0','10','100'});set(gca,'YTickLabel',{'0.01','0.1','0','10','100'});
loglog(SOT,phsc,'.');hold on;grid minor;
axis([1 100 0.01 100]);
loglog(10.^Lff,fit,'r');grid minor;
title('Phase Scintillation RMS vs Sun-Observer-Target -log scale');
xlabel('Sun-Observer-Target (degrees)');
ylabel('Phase Scintilaltion RMS (radians)');

%% Doing some stadistics with the data
% index of freedom for a t-distribution
% t-score is the t value for t-distribution

%[tth,ttc,ttst]=ttest(Lphsc);
x = LSOT;
y = dev;

[tth,ttp,ttc,ttst]=ttest2(x,y);

%fprintf(' t-test gives a t-score value of: %d\n a probability of : %d\n',tth,ttc);

%% Isolate the data by Metsahovi and others stations.
mh_samples = 0;
ot_samples = 0;
ys_samples = 0;
ma_samples = 0;
wz_samples = 0;
nt_samples = 0;
mc_samples = 0;
pu_samples = 0;
hh_samples = 0;
on_samples = 0;

for ii=1:nrsamples
    if(nstation(ii)==1) % Mh
        mh_samples = mh_samples + 1;
        mh_SOT(mh_samples) = SOT(ii);
        mh_phsc(mh_samples) = phsc(ii);
    else 
        ot_samples = ot_samples + 1;
        ot_SOT(ot_samples) = SOT(ii);
        ot_phsc(ot_samples) = phsc(ii);        
    end
    if(nstation(ii)==5) % Wz strcmp(station(ii),'WZ'))        
        wz_samples = wz_samples + 1;
    end
    if(nstation(ii)==6) % Ys strcmp(station(ii),'YS'))
        ys_samples = ys_samples + 1;
    end
    if(nstation(ii)==3) % Ma strcmp(station(ii),'MA'))
        ma_samples = ma_samples + 1;
    end
    if(nstation(ii)==4) % Nt strcmp(station(ii),'NT'))
        nt_samples = nt_samples + 1;
    end
    if(nstation(ii)==2) % Mc strcmp(station(ii),'MC'))
        mc_samples = mc_samples + 1;
    end
    if(nstation(ii)==7) % Pu strcmp(station(ii),'PU'))
        pu_samples = pu_samples + 1;
    end
    if(nstation(ii)==8) % On strcmp(station(ii),'ON'))
        on_samples = on_samples + 1;
    end
    if(nstation(ii)==9) % Hh strcmp(station(ii),'HH'))
        hh_samples = hh_samples + 1;
    end
end


figure(2);
bar([mh_samples,wz_samples,mc_samples,ys_samples,nt_samples,ma_samples,on_samples,hh_samples]);
title('Histogram of Observations'); xlabel('stations'); ylabel('Number of samples');
set(gca,'XTickLabel',{'Mh','Wz','Mc','Ys','Nt','Ma','On','Hh'});

figure(3);
mh_LSOT    = log10(mh_SOT);
ot_LSOT    = log10(ot_SOT);
mh_Lphsc   = log10(mh_phsc);
ot_Lphsc   = log10(ot_phsc);
mh_Lc      = polyfit(mh_LSOT,mh_Lphsc,1);
ot_Lc      = polyfit(ot_LSOT,ot_Lphsc,1);
mh_Lfit    = mh_Lc(2) + mh_Lc(1).*(Lff);
ot_Lfit    = ot_Lc(2) + ot_Lc(1).*(Lff);
mh_fit     = 10.^(mh_Lfit);
ot_fit     = 10.^(ot_Lfit);

loglog(mh_SOT,mh_phsc,'r.');hold on;grid minor;
loglog(ot_SOT,ot_phsc,'b.');
axis([1 100 0.01 100]);
loglog(10.^Lff,fit,'c');grid minor;
loglog(10.^Lff,mh_fit,'r');
loglog(10.^Lff,ot_fit,'b');
title('Phase Scintillation RMS vs SOT-log scale','fontname','Arial','fontsize',11);
xlabel('Sun-Observer-Target (degrees)','fontname','Arial','fontsize',10);
ylabel('Phase Scintilaltion RMS (radians)','fontname','Arial','fontsize',10);

for ii=1:mh_samples
    mh_dev(ii) = mh_Lphsc(ii) - (mh_Lc(2) + mh_Lc(1).*log10(mh_SOT(ii)));    
end
for ii=1:ot_samples
    ot_dev(ii) = ot_Lphsc(ii) - (ot_Lc(2) + ot_Lc(1).*log10(ot_SOT(ii)));    
end

std(dev)
std(mh_dev)
std(ot_dev)



%% Calculating the broadening effect.
% Broadening is defined by 10.0*phsc^1.2
% 
% B= 10.*(frq./(2*pi)).^1.2;
% 
% figure(3), grid on;
% semilogy(SOT,B,'ro');title('Spectral broadening, B=10*rms_frq^{1.2}');
% xlabel('SOT in degrees');ylabel('B in Hz');
% set(gca,'YTickLabel',{'0.1','1','10','100','1000'});

% %% Probability distribution stadistics - Gaussian and t-student
% Nx   = 100001;
% jx   = 0:1:Nx-1;
% xint = 20;
% dx   = xint/(Nx-1);
% xx   = -0.5*xint + dx.*jx;
% 
% % Compute the probability density for Student's t-distribution with df
% % degrees of freedom. 
% 
% Gt = 1/(sqrt(2*pi))*exp((-xx.^2)./2);
% Dt = gamma((df+1)/2)/(sqrt(df*pi)*gamma(df/2))*((1+(xx.^2)/df)).^(-(df+1)/2);
% 
% btp = floor(tscore/dx + 0.5)+ (Nx-1)/2;
% btm =-floor(tscore/dx + 0.5)+ (Nx-1)/2;
% 
% Pg = sum(Gt(btm:btp).*dx);
% Pt = sum(Dt(btm:btp).*dx);
% 
% fprintf('By chance probability of a Gaussian  is: %6f3\n',1-Pg);
% fprintf('By chance probability of a t-student is: %6f3\n\n',1-Pt);
% 
% b   = Lc(1); 
% bo  = 0;
% df = nrsamples -2;
% ser = std(dev)/(sqrt(df)*std(LSOT));
% tscore = (b - bo)/ser;
% fprintf('tscore is equal to: %6f3\nstandard error of the estimations: %6f3\n', tscore, ser);
% 
% btp = floor(tscore/dx + 0.5)+ (Nx-1)/2;
% btm =-floor(tscore/dx + 0.5)+ (Nx-1)/2;
% 
% %semilogy(xx,Gt,'r'); hold on; semilogy(xx,Dt,'b');
% %plot(tscore,10^-25:10^0);plot(-tscore,10^-25:10^0);
% 
% Pg = sum(Gt(btm:btp).*dx);
% Pt = sum(Dt(btm:btp).*dx);
% 
% fprintf('By chance probability of a Gaussian  is: %6f3\n',1-Pg);
% fprintf('By chance probability of a t-student is: %6f3\n',1-Pt);
