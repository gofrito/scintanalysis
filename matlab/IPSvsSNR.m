%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% IPSvsSNR.m - G. Molera                         %
% Compares values for TEC and scintillation phase%
% 2012.02.12 Now data is read from the .mat file %
% data is in separate variables             	 %
% Including table 1 and 2                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
Rv = 0.723;

% Reading the Observation Table 
filename      = '../results/ScintObsSummaryTabVEX.Block1.103.mat';
load(filename);
nrs1     = length(nr);

doy(1:nrs1,1) = 0;

% We convert the samples timestamps to MJD
for k=1:nrs1
   doy(k)=mjd(yy(k),mm(k),dd(k),HH(k),MM(k),SS(k));   
end

PhScint = PhSc;
PhNoise = PhN;
SysN    = SysNoise;
sel     = SOT;
dist    = AUdist;
tec     = TEC;
Ion_TEC = VEX_Ion_TEC;
IPS_TEC = TEC;

% Reading the Observation Table 
filename      = '../results/ScintObsSummaryTabVEX.Block2.047.mat';
load(filename);
nrs2          = length(nr);

nrs = nrs1+nrs2;

% Load data from the Phase Cal tone
filename = '../results/DnSciPcal.txt';
fid = fopen(filename,'r');
celldata = textscan(fid,'%f %f');
data     = cell2mat(celldata);
scint    = data(:,1);
dopp     = data(:,2);
fclose(fid);

% We convert the samples timestamps to MJD
for k=1:nrs2
   doy(k+nrs1)=mjd(yy(k),mm(k),dd(k),HH(k),MM(k),SS(k));   
end

PhScint(nrs1+1:nrs) = PhSc;
PhNoise(nrs1+1:nrs) = PhN;
SysN(nrs1+1:nrs)    = SysNoise;
sel(nrs1+1:nrs)     = SOT;
dist(nrs1+1:nrs)    = AUdist;
tec(nrs1+1:nrs)     = TEC;
Ion_TEC(nrs1+1:nrs) = VEX_Ion_TEC;
IPS_TEC(nrs1+1:nrs) = TEC;

wt(1:nrs,1) = 1; 
StdDev(1:2500,1) = 0;

po = sot2gop(Rv,dist,sel);

% figure(1);
% h1 = subplot(1,2,1);
% plot(Dnoise,PhSc,'r.','LineWidth',3);
% h2 = subplot(2,2,2);
% semilogx(DopSNR,Dnoise,'r.','LineWidth',3);
% xlim([1e4,0.7e6]);
% h3 = subplot(2,2,4);
% semilogx(DopSNR,PhSc,'r.','LineWidth',3);
% xlim([1e4,0.7e6]);

% h = figure(1);
% h1 = subplot(2,1,1);
% loglog(10*DopSNR,Dnoise,'r.','LineWidth',3);
% xlabel('Carrier detection SNR','FontName','Arial','Fontsize',12);
% ylabel('Doppler noise','FontName','Helvetia','Arial',12);
% xlim([0.3e4,0.7e6]);ylim([0.5 10]);
% set(h1,'FontName','Helvetia','FontSize',12);
% h2 = subplot(2,1,2);
% loglog(10*DopSNR,PhSc,'r.','LineWidth',3);
% xlabel('Carrier detection SNR','FontName','Arial','Fontsize',12);
% ylabel('Scintillation index','FontName','Arial','Fontsize',12);
% xlim([0.3e4,0.7e6]);ylim([0.01 1]);
% set(h2,'FontName','Helvetia','FontSize',12);ScintvsDopnoiseSc

p = polyfit(PhSc,Dnoise,1);
r = (0:.01:0.8);
t = p(2) + p(1)*r;
g = figure(2);hold on;
plot(PhSc,Dnoise,'b.','LineWidth',3);hold on;
xlabel('Scintillation index (radians)','FontName','Arial','Fontsize',12);
ylabel('Doppler noise (mHz in 10s)','FontName','Arial','Fontsize',12);
plot(scint,dopp,'r.','LineWidth',3);
set(gca,'FontName','Arial','FontSize',12);
plot(r,t,'b-','LineWidth',2);
