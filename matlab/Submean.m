%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% Submean.m - G. Molera                          %
% Calculate the mean value of a vector           %
% Input: vector, xx, min(x) and max(x)           %
% Output: average output vector                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [md] = Submean(dat,xx,xmin,xmax)
 nd = length(dat);
 sd = 0;
 sx = 0;
 for jj=1:nd
     if((xmin <= xx(jj)) && (xx(jj) <= xmax))
         sd = dat(jj) + sd;
         sx = sx + 1;
     end
 end
 md = sd/sx;
end
