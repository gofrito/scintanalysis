%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% show_scint_bias_results.m - G. Molera          %
% Standalone program. Reads the scint results,   %
% biased by the observing station                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Reading the Observation Table 
clear;
%filename  = '../result_analysis/ScintObsSummaryTable_r103_matver.txt';
%nrsamples = 345;
filename  = '../result_analysis/ScintObsSummaryTable_r103.mat';
nrsamples = 500;
flag      = 0;

% The file is a generic text file .txt
%out     = read_scint_table(filename,nrsamples,flag);
load(filename);
SOT     = out.SOT;
phsc    = out.phsc;
station = out.station;

LSOT   = log10(SOT);
Lphsc  = log10(phsc);
Lc     = polyfit(LSOT,Lphsc,1);
Lff    = 0:0.01:2;
Lfit   = Lc(2) + Lc(1).*(Lff);
ff     = 10.^(Lff);
fit    = 10.^(Lfit);
stdfit = std(phsc);


dev(1:nrsamples)=0;
% Checking the individual deviation.
for ii=1:nrsamples
    dev(ii) = Lphsc(ii) - (Lc(2) + Lc(1).*log10(SOT(ii)));    
end

sigmadev = std(dev);
fprintf('sigmadev: %6f2\n',sigmadev);
ser = sigmadev/(std(LSOT)*sqrt(nrsamples-2));
fprintf('Slope determination error: %6f3\n',ser);

% Plotting the results 
subaxis(2,2,3);
plot(SOT,dev,'o');grid on;hold on;
line(0:0.1:48,sigmadev,'LineStyle','-','Color','r','LineWidth',1.5);
line(0:0.1:48,-sigmadev,'LineStyle','-','Color','r','LineWidth',1.5);
axis([1 48 -0.8 0.8 ]);
%semilogx(SOT,Lff.*var(Lphsc)^2,'r-');
title('Deviations in log scale','fontname','Times New Roman','fontsize',13);
xlabel('SOT (degrees)','fontname','Times New Roman','fontsize',13);
ylabel('Fit line deviation','fontname','Times New Roman','fontsize',13);
fprintf('\n Scintillation Slope respect SOT is: %f \n',Lc(1));

subaxis(2,2,4);
histfit(dev);axis([-0.75 0.75 0 40]);
title('Histogram of deviations','fontname','Times New Roman','fontsize',13);
xlabel('Deviation','fontname','Times New Roman','fontsize',13);
%ylabel('Samples','fontname','Times New Roman','fontsize',11);

figure(1);
subplot(2,1,1);% plot(LSOT,Lphsc,'o');hold on;grid on; axis([0 2 -2 3]);
% title('Phase Scintillation RMS vs Sun-Observer-Target');
% xlabel('Sun-Observer-Target (degrees)');
% ylabel('Phase Scintilaltion RMS (radians)');
% plot(Lff,Lfit,'r');
loglog(SOT,phsc,'bo');hold on;grid on;
axis([1 60 0.05 100]);
loglog(10.^Lff,fit,'r');
title('Phase Scintillation RMS vs SOT - in log scale','fontname','Times New Roman','fontsize',13);
xlabel('SOT (deg)','fontname','Times New Roman','fontsize',13);
ylabel('Phase Scintilaltion (rad)','fontname','Times New Roman','fontsize',13);

%% Doing some stadistics with the data
% index of freedom for a t-distribution
% t-score is the t value for t-distribution

%[tth,ttc,ttst]=ttest(Lphsc);
x = LSOT;
y = dev;

[tth,ttp,ttc,ttst]=ttest2(x,y);

fprintf(' t-test gives a t-score value of: %d\n',tth);

%% Isolate the data by Metsahovi and others stations.
mh_samples = 0;
ot_samples = 0;
ys_samples = 0;
ma_samples = 0;
wz_samples = 0;
nt_samples = 0;
mc_samples = 0;
on_samples = 0;
hh_samples = 0;

for ii=1:nrsamples
    if(strcmp(station(ii),'MH'))
        mh_samples = mh_samples + 1;
        mh_SOT(mh_samples) = SOT(ii);
        mh_phsc(mh_samples) = phsc(ii);
    else 
        ot_samples = ot_samples + 1;
        ot_SOT(ot_samples) = SOT(ii);
        ot_phsc(ot_samples) = phsc(ii);        
    end
    if(strcmp(station(ii),'WZ'))
        wz_samples = wz_samples + 1;
    end
    if(strcmp(station(ii),'YS'))
        ys_samples = ys_samples + 1;
    end
    if(strcmp(station(ii),'MA'))
        ma_samples = ma_samples + 1;
    end
    if(strcmp(station(ii),'NT'))
        nt_samples = nt_samples + 1;
    end
    if(strcmp(station(ii),'MC'))
        mc_samples = mc_samples + 1;
    end
    if(strcmp(station(ii),'ON'))
        on_samples = on_samples + 1;
    end
    if(strcmp(station(ii),'HH'))
        hh_samples = hh_samples + 1;
    end
end

fprintf('We have separated the data for Mh: %6f1\n numero of samples and the other stations: %6f1\n',mh_samples, ot_samples);

Lmh_SOT  = log10(mh_SOT);
Lmh_phsc = log10(mh_phsc);
Lot_SOT  = log10(ot_SOT);
Lot_phsc = log10(ot_phsc);

Lmh_c   = polyfit(Lmh_SOT,Lmh_phsc,1);
Lot_c   = polyfit(Lot_SOT,Lot_phsc,1);
Lff     = 0:0.01:2;
Lmh_fit = Lmh_c(2) + Lmh_c(1).*(Lff);
Lot_fit = Lot_c(2) + Lot_c(1).*(Lff);
ff      = 10.^(Lff);
mh_fit  = 10.^(Lmh_fit);
ot_fit  = 10.^(Lot_fit);

mh_dev(1:mh_samples)=0;
ot_dev(1:ot_samples)=0;

% Checking the individual deviation.
for ii=1:mh_samples
    mh_dev(ii) = Lmh_phsc(ii) - (Lmh_c(2) + Lmh_c(1).*log10(mh_SOT(ii)));    
end
for ii=1:ot_samples
    ot_dev(ii) = Lot_phsc(ii) - (Lot_c(2) + Lot_c(1).*log10(ot_SOT(ii)));    
end
sigmamh_dev = std(mh_dev);
mh_ser = sigmamh_dev/(std(Lmh_SOT)*sqrt(mh_samples-2));
sigmaot_dev = std(ot_dev);
ot_ser = sigmaot_dev/(std(Lot_SOT)*sqrt(ot_samples-2));
fprintf('sigmadev: %6f2\nSlope error: %6f3\n',sigmamh_dev,mh_ser);
fprintf('sigmadev: %6f2\nSlope error: %6f3\n',sigmamh_dev,mh_ser);

serp   = sqrt(mh_ser^2+ot_ser^2);
tscore = (Lot_c(1)-Lmh_c(1))/serp;
df = mh_samples + ot_samples - 2;
fprintf('tscore: %6f3\n df: %d',tscore,df);

figure(2);
subaxis(2,2,3);
plot(mh_SOT,mh_dev,'bo');grid on;hold on;
plot(ot_SOT,ot_dev,'ro');grid on;hold on;
title('Deviations per stations','fontname','Times New Roman','fontsize',13);
xlabel('SOT (degrees)','fontname','Times New Roman','fontsize',13);
ylabel('Deviation','fontname','Times New Roman','fontsize',13);
fprintf('\n Scintillation Slope respect SOT is: %f \n',Lmh_c(1));

subaxis(2,2,4);
histfit(mh_dev);axis([-0.75 0.75 0 20]);
title('Histogram of Mh deviations','fontname','Times New Roman','fontsize',13);
xlabel('Deviation','fontname','Times New Roman','fontsize',13);

figure(2);
subplot(2,1,1);
loglog(mh_SOT,mh_phsc,'ro');hold on;grid on;
loglog(ot_SOT,ot_phsc,'bo');
axis([1 60 0.05 100]);
loglog(10.^Lff,fit,'r');
title('Phase Scintillation RMS vs SOT per stations - in log scale','fontname','Times New Roman','fontsize',13);
xlabel('SOT (deg)','fontname','Times New Roman','fontsize',13);
ylabel('Phase Scintilaltion (rad)','fontname','Times New Roman','fontsize',13);

%%
x = Lmh_SOT;
y = mh_dev;

[ttmh_h,ttmh_p,ttmh_ci,ttmh_st]=ttest2(Lmh_SOT,mh_dev);

fprintf(' t-test gives a t-score value of: %d\n a probability of : %6f3\n',ttmh_h,ttmh_st.tstat);

% figure(3);
% subaxis(2,2,3);
% semilogx(ot_SOT,ot_dev,'o');grid on;hold on;
% title('Deviations in log-log');
% xlabel('Sun-Observer-Target (degrees)');
% ylabel('Deviation from fit line');
% fprintf('\n Scintillation Slope respect SOT is: %f \n',Lot_c(1));
% 
% subaxis(2,2,4);
% histfit(ot_dev);axis([-0.75 0.75 0 20]);
% title('Histogram of deviations');
% xlabel('Deviation');
% ylabel('Counts');
% 
% figure(3);
% subplot(2,1,1);
% loglog(ot_SOT,ot_phsc,'o');hold on;grid on;
% axis([1 100 0.01 100]);
% loglog(10.^Lff,fit,'r');
% title('Phase Scintillation RMS vs Sun-Observer-Target at other stations -log scale');
% xlabel('Sun-Observer-Target (degrees)');
% ylabel('Phase Scintilaltion RMS (radians)');

x = Lot_SOT;
y = ot_dev;

[ttot_h,ttot_p,ttot_ci,ttot_st]=ttest2(Lot_SOT,ot_dev);

fprintf(' t-test gives a t-score value of: %d\n a probability of : %6f3\n',ttot_h,ttot_st.tstat);

figure('Position',[150 600 375 300],'Units', 'centimeters');
bar([hh_samples,ma_samples,mc_samples,mh_samples,nt_samples,on_samples,wz_samples,ys_samples]);
title('Histogram of Observations','fontname','TimesNewRoman','fontsize',11);
xlabel('Stations','fontname','TimesNewRoman','fontsize',11);
ylabel('Number of samples','fontname','TimesNewRoman','fontsize',11);
set(gca,'XTickLabel',{'Hh','Ma','Mc','Mh','Nt','On','Wz','Ys'});

%% Probability distribution
Nx   = 100001;
jx   = 0:1:Nx-1;
xint = 20;
dx   = xint/(Nx-1);
xx   = -0.5*xint + dx.*jx;

% Compute the probability density for Student's t-distribution with df
% degrees of freedom. ``
%Dt = dt(xx,df);

Gt = 1/(sqrt(2*pi))*exp((-xx.^2)./2);
Dt = gamma((df+1)/2)/(sqrt(df*pi)*gamma(df/2))*((1+(xx.^2)/df)).^(-(df+1)/2);

btp = floor(tscore/dx + 0.5)+ (Nx-1)/2;
btm =-floor(tscore/dx + 0.5)+ (Nx-1)/2;

Pg = sum(Gt(btm:btp).*dx);
Pt = sum(Dt(btm:btp).*dx);

fprintf('By chance probability of a Gaussian  is: %6f3\n',1-Pg);
fprintf('By chance probability of a t-student is: %6f3\n\n',1-Pt);

%%
b   = Lc(1); 
%b   = mean(dev);
bo  = 0;
df = nrsamples -2;
ser = std(dev)/(sqrt(df)*std(LSOT));
tscore = (b - bo)/ser;
fprintf('tscore is equal to: %6f3\nstandard error of the estimations: %6f3\n', tscore, ser);


%% Probability distribution

%semilogy(xx,Gt,'r'); hold on; semilogy(xx,Dt,'b');

btp = floor(tscore/dx + 0.5)+ (Nx-1)/2;
btm =-floor(tscore/dx + 0.5)+ (Nx-1)/2;

%plot(tscore,10^-25:10^0);plot(-tscore,10^-25:10^0);

Pg = sum(Gt(btm:btp).*dx);
Pt = sum(Dt(btm:btp).*dx);

fprintf('By chance probability of a Gaussian  is: %6f3\n',1-Pg);
fprintf('By chance probability of a t-student is: %6f3\n',1-Pt);
