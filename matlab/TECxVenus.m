%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% TECxVenus.m - G. Molera                        %
% Compute Total Electron Content (TEC) for Venus %
% Input: Spectra,freq scale,freq window,         %
% 		 line to avoid                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

PLOT=0;

Ro  = 6.97e8;       % Radius of the Sun in metre
AU  = 149.598e9;    % 150e6 Km
Roe = AU;           % Distance from Earth to Sun
Rov = 0.723*AU;      % Distance from Venus to Sun
dl  = 5;            % Nominal electron density at 1 AU
bo  = 1.64e-21;
co  = 1.14e-24;
f   = 8.415;        % S/C carrier frequency in GHz 

% Orbital phases
Ncy = 360;
jcy = 0:Ncy;
xcy = cos(2*pi/Ncy.*jcy);
ycy = sin(2*pi/Ncy.*jcy);

% Draw the orbits in a plan
xs = 0;
ys = 0;
xe = Roe.*xcy./AU;
ye = Roe.*ycy./AU;
xv = Rov.*xcy./AU;
yv = Rov.*ycy./AU;

% Define an arbitrary orbital position
jv  = 121;
xvt = xv(jv);
yvt = yv(jv);

% positions of Earth, Venus at a given orbital phase
Ve       = [Roe,0]./AU;
Vv       = [xvt,yvt];
Npp      = 360;
jpp      = 1:Npp;
sota     = acos(nv(-Ve).*nv(Vv-Ve)).*180/pi;
dist     = vn(Vv-Ve);
sdist    = sin(sota(1).*pi/180);
Vvp(1,:) = xv;
Vvp(2,:) = yv;

% Calculating the vectors.
svt(1,:) = Ve;
svt(2,:) = Vv;
sv       = svt';
set(1,:) = Ve;
set(2,:) = 0 ;
se       = set';

Mdis = Rov;
Ndis = 100;
ddis = Mdis/Ndis;
jdis = 1:Ndis;
dis  = ddis.*jdis;

%Rho is the electron content on the path for the SoV
Rho  = dl.*((dis/AU).^-2);

% Integrate for Venus
tdatv=zeros(4,Npp);

for Jp=1:Npp
 tdatv(:,Jp) = TEC(Jp,Vvp,Ve);
end

SOTv  = tdatv(2,:);
TECv  = tdatv(3,:);
distv = tdatv(4,:);

Ts = [0,20;1,17;2,15;3,12;4,10;5,7;6,7;7,10;8,21;9,32;10,39;11,48;12,56;13,62;14,64;15,63;16,57;17,51;18,45;19,40;20,35;21,30;22,25;23,22];

a = sin(SOTv*pi/180)*Roe;
RTEC = 1.82e23.*(a./Ro).^(-5) + 4.29e21.*(a./Ro).^-1.3;
sigmaD = 2.*bo./f.*RTEC;    % Apply correction because is a two-way link

% Generating the geocentric orbit phase
pho = sot2gop(Rov/AU,distv*AU,SOTv);

%% Plotting some results
if (PLOT==1)
    figure(1);hold on;
    plot(xe,ye,'b-','LineWidth',2);plot(xv,yv,'g-','LineWidth',2);
    plot(xs,ys,'ko');plot(xe(1),ye(1),'bo');plot(xvt,yvt,'go');
    plot(sv(1,:),sv(2,:),'g');plot(se(1,:),se(2,:),'b');
    figure(2);
    semilogy(SOTv,TECv,'g','LineWidth',2);hold on;
    title('Plotting TEC respect SOT');
    xlabel('SOT in degrees');ylabel('TEC');
    figure(3);
    semilogy(SOTv,sigmaD,'c');xlim([0,30]);
    title('Plotting sigmaD');
    xlabel('SOT in degrees');ylabel('sigmaD');
end
