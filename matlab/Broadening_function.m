%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% broadening_function.m - G. Molera              %
% Function calculates the spectral broadening    %
% in the phase fluctuations                      %
% Input: handles (Outptu)                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [handles] = broadening_function(handles)

% Read timestamps and phases from file
Ph_tmp = textread(handles.Fn);
tp     = Ph_tmp(:,1);
Np     = length(tp);
dtp    = tp(2)-tp(1);
Ts     = dtp*Np;
df     = 1/Ts;
ff     = 1:df:Ts;
BW     = Np*df;
Fa     = 10;
Ph     = zeros(handles.Ns,Np);

for ii=1:handles.Ns
   Ph(ii,:)= Ph_tmp(:,ii+1);
end

% Helpers

colorlst = 'rgbcmyk';
clear legendlst;

for ii=1:handles.Ns,
    legendlst(ii,:) = ['Scan ' int2str(ii)];
end

%% Pseudo-CW recovery with windowing
% TD reconstruction
sigs = zeros(handles.Ns, Np);
%wf(1:Np) = cos(pi/Np.*(jp - 0.5*Np)).^2;
wf   = window(@hann, Np);

for ii=1:handles.Ns
 sigs(ii,:) = exp(i*(2*pi*handles.ffilter*tp + Ph(ii,:)'));
 sigs(ii,:) = sigs(ii,:) .* wf';
end

% Back to FD
sigsf = fft(sigs,[],2);

% Checking the power levels
Pf = sum(sigs,2)/Np;
fprintf('Pf: %3s\n',mat2str(round(Pf*1000)/1000));

%% Curve fitting to the spectra
xbin=11000;
sigsest = mle(abs(sigsf(1,1:xbin)),'distribution','generalized pareto');
fitted  = gppdf(1:xbin,sigsest(1),sigsest(2),0);

%% Curve fitting, est(x) = alpha / x^beta (or below, generalized pareto...)
% xplotmax   = 2500;
% ppf        = fft(Ph, [], 2);
% ppfa       = sum(ppf,1)./handles.Ns;
% 
%figure(1), clf, hold off;

% for ii=1:handles.Ns,
%     loglog(tp(1:xplotmax),abs(ppf(ii,1:xplotmax)),colorlst(mod(ii,size(colorlst,2))));
%     hold on;
% end
 
%gpEsts = mle(abs(ppfa),'distribution','generalized pareto');
%fitted = gppdf(1:Np,gpEsts(1),gpEsts(2),0);
% Lf = polyfit(log10(tp),log10(ppfa)',1);
% Lfit = Lf(2) + Lf(1).*log10(tp);
% 
% figure(2), clf, hold on;
% semilogy(abs(ppfa(1:xplotmax)),'r'), semilogy(2*Nps*fitted(1:xplotmax),'g');
% xlim([0,xplotmax/25]);
% well, as apparent from the plots, I'm not doing it rite...!
%% FD smoothing of residuals
%sigsfit Real fit to sigsf

sigsressmooth = zeros(handles.Ns, Np);
sigsfit       = ones(handles.Ns, Np); % TODO: real fit
smoothlen     = 20;
swf           = gausswin(smoothlen); 
swf           = swf/sum(swf);

% Moving average
for ii=1:handles.Ns,
    % sigsressmooth(ii,:) = smooth(abs(sigsf(ii,:))./sigsfit(ii,:),smoothlen);
    % Gaussian
    tmp = conv(abs(sigsf(ii,:))./sigsfit(ii,:), swf);
    sigsressmooth(ii,:) = tmp(smoothlen/2:end - smoothlen/2);
end

%% Spectral broadening -- narrow down the bin range
% Find the peak
[xv,xc] = max(sigsf(1,:));
xxvals = 2*handles.ffilter*((1:Np)-1)/Np;
xspanF = 0.10;
xspanmin = max( 1, xc - xspanF*Np);
xspanmax = min(Np, xc + xspanF*Np);

%% Spectral broadening -- determine half power bandwidths
handles.hpbws    = zeros(handles.Ns,1);
hpbwidxs = zeros(handles.Ns,1);

for ii=1:handles.Ns,
    cumu = cumsum(abs(sigsf(ii, xspanmin:xspanmax)));
    llst = (cumu<(max(cumu)/4));
    hlst = (cumu>(max(cumu)*3/4));
    [vlow, ilow] = min(llst);
    [vhi, ihi] = max(hlst);
    hpbwidxs(ii) = (ihi-ilow);
    handles.hpbws(ii) = (ihi-ilow)*2*handles.ffilter/Np;
end

fprintf('Hpbws: %s\n',mat2str(round(handles.hpbws*1000)/1000));

%% Spectral broadening -- assistive visual plot

figure(3), hold off, clf;
for ii=1:handles.Ns,
     colorval = colorlst(mod(ii,size(colorlst,2)));
     bwhalf = floor(hpbwidxs(ii)/2);
     [vpeak,ipeak] = max(abs(sigsf(ii, xspanmin:xspanmax)));
     figure(3), 
     semilogy(xxvals(xspanmin:xspanmax), abs(sigsf(ii, xspanmin:xspanmax)), colorval),
     line([xxvals(xspanmin+ipeak-bwhalf-1) xxvals(xspanmin+ipeak+bwhalf-1)], [vpeak/4 vpeak/4], 'Color', colorval, 'Marker', 'x'),
     hold on;
end

grid on, axis tight, legend(legendlst), xlabel('Freq [Hz]'), ylabel('Power');

end
