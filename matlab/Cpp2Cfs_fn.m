%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% Cpp2Cfs_fn.m - G. Molera                       %
% Converts the Cpp polys to Cfs                  %
% Input: Cpp_file and Sample Rate                %
% Output: Cfs_file                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [out] = Cpp2Cfs_fn(filename,SR)
  Cpp        = textread(filename);
  dts        = SR^-1;
  Npf        = length(Cpp)-1;
  Cfs(1:Npf) = 0;
  
 for jj=1:Npf
      Cfs(jj) = jj*Cpp(jj+1)/(2*pi*(dts)^(jj))*2^jj;
  end
 
  out = Cfs';
  save(strcat(filename(1:39),'.X5cfs.txt'),'Cfs','-double','-ASCII');
end
