%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% read_scint_table.m - G. Molera                 %
% Read the results of the observations           %
% Input: filename, Numero Samples,               %
%        Flag=1 save to file Flag=0 in columns   %
% Output: Data is sorted in columns              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [out]= read_scint_table (filename,nrsamples,flag)
 fprintf('Summary of the scintillations observations:\n');
 fprintf(2,'%s\n',filename);
 
 fid = fopen(filename,'r');
 if(fid<1)
    fprintf('The file has not been opened correctly');
 end

 % remove the headers from the beginning of the file
 for k=1:4
    fgetl(fid);
 end

 C = textscan(fid,'%s %d %d %d %d %d %d %d %d %d %d %d %d %f %d %d %d %f %f %f %f %f %f %f %s %f %f %f %f %f %f %f %f %f %f %f %f %s',nrsamples);
 
 fclose(fid);
     out.nr         = C{1,2};
     out.ns         = C{1,3};
     out.nsc        = C{1,4};
     out.nstation    = C{1,5};
     out.yy         = C{1,6};
     out.mm         = C{1,7};
     out.dd         = C{1,8};
     out.HH         = C{1,9};
     out.MM         = C{1,10};
     out.SS         = C{1,11};
     out.dur        = C{1,12};
     out.rah        = C{1,13};
     out.ram        = C{1,14};
     out.ras        = C{1,15};
     out.deh        = C{1,16};
     out.dem        = C{1,17};
     out.des        = C{1,18};
     out.az         = C{1,19};
     out.el         = C{1,20};
     out.SOT        = C{1,21};
     out.AUdist     = C{1,22};
     out.LonE       = C{1,23};
     out.LatE       = C{1,24};
     out.phn        = C{1,26};
     out.phsc       = C{1,27};
     out.frq        = C{1,28};
     out.ScintSlope = C{1,29};
     out.errorSlope = C{1,30};
     out.Peak3mHz   = C{1,31};
     out.SysN       = C{1,32};
     out.SolarActA  = C{1,33};
     out.SolarActB  = C{1,34};
     out.Ionos_VEX  = C{1,35};
     out.Ionos_Ceb  = C{1,36};
     out.IPS_TEC    = C{1,37};
     
 if (flag==1)
    outfile = strcat(filename(1:66),'.mat'); 
    
    fprintf('Saving the Observations Table Summary:\n');
    fprintf(2,'%s\n',outfile);
    save (outfile,'out');
 end
end
