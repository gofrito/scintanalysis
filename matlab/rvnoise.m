fid      = fopen('VelocityNoise.txt','r');
celldata = textscan(fid,'%f %f');
data     = cell2mat(celldata);
rvn      = data(:,1);
nmeas    = data(:,2);
fclose(fid);

fid      = fopen('VelNoisePcal.txt','r');
celldata = textscan(fid,'%f %f');
dataPcal = cell2mat(celldata);
rvp      = dataPcal(:,1);
pmeas    = dataPcal(:,2);
fclose (fid);

bar(rvn,nmeas);hold on;
xlabel('Radial velocity noise (um/s in 60s)','FontName','Arial','Fontsize',13);
ylabel('Number of measurements','FontName','Arial','Fontsize',13);
bar(rvp,pmeas,'r');
xlim([0 80]);
