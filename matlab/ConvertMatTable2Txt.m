%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% ConvertMatTable2Txt.m - G. Molera              %
% Convert a Mat format table to a regular text   %
% file                                           %
%                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print a struct of values and letters into a regular ascii file
filename='ScintObsSummaryTab_r102.mat';
load(filename);

fid = fopen(strcat(filename(1:24),'txt'),'w+');
len = length(nsc);

for ii=1:len
    fprintf(fid,'|\t%s\t%s\t%s\t%s\t',int2str(nr(ii)),int2str(ns(ii)),int2str(nsc(ii)),int2str(code(ii)));
    fprintf(fid,'\t%s %s %s\t%s ',int2str(yy(ii)),int2str(mm(ii)),int2str(dd(ii)),int2str(HH(ii)));
    fprintf(fid,'%s %s\t%s\t%s ',int2str(MM(ii)),int2str(SS(ii)),int2str(dur(ii)),int2str(rah(ii)));
    fprintf(fid,'%s %s\t%s %s ',int2str(ram(ii)),int2str(ras(ii)),int2str(deh(ii)),int2str(dem(ii)));
    fprintf(fid,'%s\t%s %s\t%s ',int2str(des(ii)),int2str(az(ii)),int2str(el(ii)),int2str(SOT(ii)));
    fprintf(fid,'%s\t%s %s\t|',int2str(AUdist(ii)),int2str(LonEcl(ii)),int2str(LatEcl(ii)));
    fprintf(fid,'\t%s\t%s\t%s\t%s',int2str(PhN(ii)),int2str(PhSc(ii)),int2str(Broadening(ii)),int2str(ScintSlope(ii)));
    fprintf(fid,'\t%s\t%s\t%s\t%s',int2str(ErrorSlope(ii)),int2str(Peak3mHz(ii)),int2str(SysNoise(ii)),int2str(SolarActA(ii)));
    fprintf(fid,'\t%s\t%s\t%s\t%s',int2str(SolarActB(ii)),int2str(VEX_Ion_TEC(ii)),int2str(TEC(ii)));
    fprintf(fid,'\n');
end

fclose(fid);

%matrix(:,1) = nr;matrix(:,2) = ns;matrix(:,3) = nsc;matrix(:,4) = nstation;
%matrix(:,5) = yy;matrix(:,6) = mm;matrix(:,7) = dd;
%matrix(:,8) = HH;matrix(:,9) = MM;matrix(:,10) = SS;matrix(:,11) = dur;matrix(:,12) = rah;
%matrix(:,13) = ram;matrix(:,14) = ras;matrix(:,15) = deh;matrix(:,16) = dem;matrix(:,17) = des;matrix(:,18) = az;
%matrix(:,19) = el;matrix(:,20) = SOT;matrix(:,21) = AUdist;matrix(:,22) = LonE;matrix(:,23) = LatE;
%matrix(:,24) = phn;matrix(:,25) = phsc;matrix(:,26) = frq;matrix(:,27) = ScintSlope;
%matrix(:,28) = errorSlope;matrix(:,29) = Peak3mHz;matrix(:,30) = SysN;matrix(:,31) = SolarActA;
%matrix(:,32) = SolarActB;matrix(:,33) = Ion_TEC;matrix(:,34) = TEC;
