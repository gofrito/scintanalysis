%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% TECxIonos.m - G. Molera                        %
% Read the Ionospheric TEC values for a two years%
% data and plot in 0 to 180 degree scale         %
% Ionopsheric TEC contain time, mean TEC, std    %
% deviance, minimum value and maximum value      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%fprintf('\nReading the input file with Ionospheric TEC estimates\n\n');
%fprintf('Time Mean StdDev Min Max\n');

filename='../results/Ionospheric_TEC.txt';

fid = fopen(filename);
C   = textscan(fid,'%f%f%f%f%f');
tec = cell2mat(C);


ttec = tec(:,1);
mtec = tec(:,2);
rtec = tec(:,3);
ntec = tec(:,4);
xtec = tec(:,5);

ttec = ttec-12;
stec = abs(ttec)*180/12;

figure(2);
errorbar(stec,mtec,xtec,ntec/2,'k'); hold on;
plot(stec,mtec,'k','LineWidth',2);
xlim([0 180]);
%xlabel('Angle [degrees]');%ylabel('TEC [tecu]');
%title('Simulated TEC for the Ionosphere');