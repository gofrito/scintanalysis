%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% TEC.m - G. Molera                              %
% Calculates TEC for a given vector to target    %
% Input: Jp, vector target and vector earth      %
% Output: SOT, TEC and distribution              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [out] = TEC(Jp,Vp,Ve)
 AU   = 149.598e9;      % Astronomical Unit distance
 tecu = 10e16;          % Total Electron Content Unit is 10e16 m^2
 dl   = 05e-04;         % 5 cm^-2
 Npi  = 1000;
 Sot  = acos(nv(-Ve).*nv(Vp(:,Jp)'-Ve))*(180/pi);
 dist = vn(Vp(:,Jp)' - Ve)/AU;
 Tec  = 0;
 dvp  = (Vp(:,Jp)' - Ve)/Npi;
 for jj=1:Npi
     Vpc = Ve + dvp*(jj-1);
     sdp = vn(Vpc);
     dcp = dl.*((sdp/AU).^-2);
     Tec = Tec + dcp*vn(dvp);
 end
 Tec = Tec/tecu;
 out = [Jp,Sot(1),Tec,dist]';
end
