%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% plot_TECxMarsVenus.m - G. Molera				 %
% Plot the TEC for Mars and Venus				 %
% Calling TECxMars and TECxVenus scripts		 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TECxMars;
TECxVenus;

% Tunning up
figure(2);
legend('Mars Express','Venus Express');
xlabel('Solar elongation (degrees)','FontSize',12,'FontName','Arial');
ylabel('TEC (cm^{-3})','FontSize',12,'FontName','Arial');
title('Simulated TEC for the MEX and VEX signal path','FontSize',13,'FontName','Arial');
