%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% EstimateTECvalues.m - G. Molera                %
% Estimate the TEC values given a specific angle %
% and the distance to the Earth                  %
% Input:  Solar Elongation and Distance to Earth %
% Output: Angle, TEC, Distribution               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [out] = EstimateTECvalues(SOT,Dist)

% Calculate the x and y components of Venus
Xv = 1-cos(SOT*pi/180)*Dist;
Yv = sin(SOT*pi/180)*Dist;

out = TECs([Xv,Yv],[1,0]);

fprintf('1- Venus is at solar elongation equal %f and at a distance %f\n',SOT,Dist);
fprintf('2- The estimated TEC value is %f\n',out(2));
end