%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% testFilterRMS.m - G. Molera                    %
% test the Filter of RMS                         %
% Input:                                         %
% Output:                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Declaring the variables
Nt = 3000;
jt = 1:Nt;
dt = 0.1;
tt = jt.*dt;
Tt = Nt*dt;
df = Tt^-1;
ft = df.*jt;

% We run a test example
F1 = 0.5;
F2 = 0.05;
s1 = 2*cos(2*pi*F1.*tt);
s2 = cos(2*pi*F2.*tt);

fprintf('Standard deviation of the signals 1.414: %6.3f and 0.707 %6.3f',std(s1),std(s2));
fprintf('\n');

st = s1 + s2;

% Move to frequency domain
sp = fft(st);
Filt1(1:Nt) = 0;
Filt2(1:Nt) = 0;
count = 0;

for i=1:Nt
    if((0.3 <= ft(i)) && (ft(i) <= 0.6))
        Filt1(i) = 2;
    end
    if((0.03 <=ft(i)) && (ft(i) <= 0.06))
        Filt2(i) = 2;
    end
end

% Filter and recover the signal

sp1 = sp.*Filt1;
sp2 = sp.*Filt2;

%sp1 = sp1./count;
s1f = real(ifft(sp1));
s2f = real(ifft(sp2));

fprintf('Standard deviation at the end of the exercise should be the same: %6.3f and %6.3f\n',std(s1f),std(s2f));
