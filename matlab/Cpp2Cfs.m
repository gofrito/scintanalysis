%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% Cpp2Cfs.m - G. Molera                          %
% Converts the Cpp polys to Cfs                  %
% Input: Cpp values and Sample Rate              %
% Output: Cfs values                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [out] = Cpp2Cfs(in,SR)
  dts        = SR^-1;
  Npf        = length(in)-1;
  Cfs(1:Npf) = 0;

  for jj=1:Npf
      Cfs(jj) = jj*in(jj+1)/(2*pi*(dts)^(jj))*2^jj;
  end

  out = Cfs';
end
