%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% TECxMars.m - G. Molera                         %
% Compute Total Electron Content (TEC) for Mars  %
% Input: Spectra,freq scale,freq window,         %
% 		 line to avoid                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

PLOT=1;

Ro  = 6.97e8;       % Radius of the Sun in metre
AU  = 149.598e9;    % 150e6 Km
Roe = AU;           % Distance from Earth to Sun
Rom = 1.6*AU;		% Distance from Mars to Sun
dl  = 5;			% Nominal electron denisity at 1 AU.
bo  = 1.64e-21;
co  = 1.14e-24;
f   = 8.415;        % S/C carrier frequency in GHz 

% Orbital phases
Ncy = 360;
jcy = 0:Ncy;
xcy = cos(2*pi/Ncy.*jcy);
ycy = sin(2*pi/Ncy.*jcy);

% Draw the orbits in a plan
xs = 0;
ys = 0;
xe = Roe.*xcy./AU;
ye = Roe.*ycy./AU;
xm = Rom.*xcy./AU;
ym = Rom.*ycy./AU;

% Define an arbitrary orbital position
jm  = 121;
xmt = xm(jm);
ymt = ym(jm);

% positions of Earth, Mars at a given orbital phase
Ve       = [Roe,0]./AU;
Vm       = [xmt,ymt];
Npp      = 360;
jpp      = 1:Npp;
sota     = acos(nv(-Ve).*nv(Vm-Ve)).*180/pi;
dist     = vn(Vm-Ve);
sdist    = sin(sota(1).*pi/180);
Vmp(1,:) = xm;
Vmp(2,:) = ym;

% Calculating the vectors.
smt(1,:) = Ve;
smt(2,:) = Vm;
sm		 = smt';
set(1,:) = Ve;
set(2,:) = 0 ;
se       = set';

Mdis = Rom;
Ndis = 100;
ddis = Mdis/Ndis;
jdis = 1:Ndis;
dis  = ddis.*jdis;

% Rho is the electron content along the SoM trajectory.
Rho  = dl.*((dis/AU).^-2);

% Integrate the TEC for the Mars path
tdatv=zeros(4,Npp);

for Jp=1:Npp
	tdatm(:,Jp) = TEC(Jp,Vmp,Ve);
end

SOTm  = tdatm(2,:);
TECm  = tdatm(3,:);
distm = tdatm(4,:);

a		= sin(SOTm*pi/180)*Roe;
RTEC	= 1.82e23.*(a./Ro).^(-5) + 4.29e21.*(a./Ro).^(-1.3);
sigmaD	= 2.*bo./f.*RTEC;	% Apply a correction cause is a 2-way link.

%% Plotting some results
if (PLOT==1)
	figure(1);hold on;
	plot(xe,ye,'b-','LineWidth',2);plot(xm,ym,'r-','LineWidth',2);
	plot(xs,ys,'ko');plot(xe(1),ye(1),'bo');plot(xmt,ymt,'ro');
	plot(sm(1,:),sm(2,:),'r');plot(se(1,:),se(2,:),'b');
	figure(2);
	semilogy(SOTm,TECm,'r','LineWidth',2);hold on;
	title('Plotting TEC respect SOT');
	xlabel('SOT in degrees');ylabel('TEC');
	figure(3);
	semilogy(SOTm,sigmaD,'c');xlim([0,30]);
	title('Plotting sigmaD');
	xlabel('SOT in degrees');ylabel('sigmaD');
end
