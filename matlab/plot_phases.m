%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% plot_phases.m G. Molera                        %
% the aim of this program is to                  %
% in the same figure two example of extracted    %
% phases. One with high and the other with low   %
% fluctactions.                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fin1 = '../Phase_data/Phases.vex2010.01.16.Mh.txt';
fin2 = '../Phase_data/Phases.vex2010.08.30.On.txt';
nsc  = 4;
nPh  = 2;

for j=1:nPh
    fprintf(' Reading data from the Phase files\n\n');
    if (j == 1)
        fprintf('We open the 1st file : %s \n',fin1); Ph_tmp = textread(fin1); end;
    if (j == 2)
        fprintf('We open the 2nd file : %s \n',fin2); Ph_tmp = textread(fin2); end;

    tp  = Ph_tmp(1:Np,1);
    Np  = 42000;
%    Np  = length(tp)
    dtp = tp(2)-tp(1);
    Ts  = dtp*Np;
    df  = 1/Ts;
    BW  = Np*df;
    Fa  = 10;
    bfa = floor(Fa/df);
    if (j == 1)
        Ph1 =zeros(nsc,Np);
        for k=1:nsc
            Ph1(k,:)=Ph_tmp(1:Np,k+1); 
        end
    end
    if (j == 2)
        Ph2 =zeros(nsc,Np);
        for k=1:nsc
            Ph2(k,:) = Ph_tmp(1:Np,k+1);
        end
    end
end
size(Ph1)
size(Ph2)

figure(1);hold on; grid on;
%title({'Extracted phase from VEX signal';' observed on 2011.03.25 at On-Mh'},'fontsize',13,'fontname','Arial');
%xlabel('Time in seconds - 5 scans','fontsize',13,'fontname','Arial');
%ylabel('Phase variation [rad]','fontsize',13,'fontname','Arial');
title({'Residual phase from the VEX signal';' observed at low and wide solar elongation'},'fontsize',13,'fontname','Arial');
xlabel('Time in seconds - 5 scans','fontsize',13,'fontname','Arial');
ylabel('Phase variation [rad]','fontsize',13,'fontname','Arial');
plot(tp+1140*0,Ph1(1,:),'r');plot(tp+1140*0,Ph2(1,:),'b');
plot(tp+1140*1,Ph1(2,:),'r');plot(tp+1140*1,Ph2(2,:),'b');
plot(tp+1140*2,Ph1(3,:),'r');plot(tp+1140*2,Ph2(3,:),'b');
plot(tp+1140*3,Ph1(4,:),'r');plot(tp+1140*3,Ph2(4,:),'b');
legend('SOT 1.5 deg','SOT 45 deg');
axis auto;

% Combined rms
%std_high = 1/4*(std(Ph1(1,:)) + std(Ph1(2,:)) + std(Ph1(3,:)) + std(Ph1(4,:)));
%std_low = 1/4*(std(Ph2(1,:)) + std(Ph2(2,:)) + std(Ph2(3,:)) + std(Ph2(4,:)));

%fprintf('if SOT is 08? the RMS of phase variation is %s \n if SOT is 41? the RMS of phase variation is %s \n',std_high,std_low); 

%rPh(1,:)=Ph1(1,:)-Ph2(1,:);
%rPh(2,:)=0;
%rPh(3,:)=Ph1(3,:)-Ph2(2,:);
%rPh(4,:)=Ph1(4,:)-Ph2(3,:);
%rPh(5,:)=Ph1(5,:)-Ph2(4,:);

%figure(2);hold on; grid on;
%plot(tp+1140*0,rPh(1,:),'c');
%plot(tp+1140*2,rPh(3,:),'c');
%plot(tp+1140*3,rPh(4,:),'c');
%plot(tp+1140*4,rPh(5,:),'c');

%std(rPh(1,:))
%std(rPh(3,:))
%std(rPh(4,:))
%std(rPh(5,:))
