% Reading Horizons data and plotting the ecliptic Latitude observations vs.
% the solar elongation and epoch.

filename = 'Horizons_2years.txt';
nrsamples= 72;

fid = fopen(filename,'r');
if (fid < 1)
    fprintf(2,'Error opening the file\n');
end

 % remove the headers from the beginning of the file
for k=1:2
    fgetl(fid);
end
                   % 2008-Jul-11    00:00   08 00  05 +21  44   43  12  -7  -3  0.7 1.7  -3  8. /T  12. 7.   1.
 C = textscan(fid,'%s %s %s %d %d %d %d %f %d %d %f %f %f %f %f %f %d %f %s %f %f %f',nrsamples);

 fclose(fid);
 size(C)
 
 ec   = C{1,22};
 yy   = C{1,1};
 mm   = C{1,2};
 dd   = C{1,3};
 so   = C{1,18};