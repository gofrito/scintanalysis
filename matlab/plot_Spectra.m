%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% plotSpectra.m - G. Molera                      %
% Standalone                                     %
% Plot several examples of phase power spectra   %
% and their polynomial fit                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('1 - Data observed on 25.01.2010 at Wettzell\n');
a.slope  = -2.322;
a.rmsphs = 3.7;
s.rmsphn = 0.06;
a.sysn   = 0.46;
a.fmax   = 1;
a.fmin   = 0.02;
[a_pspaw,a_fit,Freq] = scint_analysis('../Phase_data/Phases.vex2010.01.25.Wz.txt',5);
fprintf('2 - Data observed on 21.03.2010 at Wettzell\n');
b.slope  = -2.600;
b.rmsphs = 0.3817;
b.rmsphn = 0.0119;
b.sysn   = 0.19;
b.fmax   = 0.7;
b.fmin   = 0.010;
[b_pspaw,b_fit,Freq] = scint_analysis('../Phase_data/Phases.vex2010.03.31.Wz.txt',5);
fprintf('3 - Data observed on 21.03.2011 at Wettzell\n');
c.slope  = -2.286;
c.rmsphs = 0.300;
c.rmsphn = 0.0120;
c.sysn   = 0.278;
c.fmax   = 0.6;
b.fmin   = 0.008;
[c_pspaw,c_fit,Freq] = scint_analysis('../Phase_data/Phases.vex2011.03.21.Wz.txt',5);

figure(2);
loglog(Freq,a_pspaw,'b');hold on;
loglog(Freq,a_fit,'r');
loglog(Freq,b_pspaw,'b');hold on;
loglog(Freq,b_fit,'r');
loglog(Freq,c_pspaw,'b');hold on;
loglog(Freq,c_fit,'r');
title('Kolmogorov spectrum for 3 epochs at Wz','fontsize',16,'fontname','Arial');
xlabel('Frequency','fontname','Arial','fontsize',14);
ylabel('Phase power','fontname','Arial','fontsize',14);
xlim([0.001 10]); ylim([1e-5 1e5]);grid minor;
