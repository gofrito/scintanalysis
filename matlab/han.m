%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% han.m - G. Molera                              %
% 1 Hanning window from specific vector          %
% Input: data vector                             %
% Output: data vector smoothed                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [dout] = han(dat)
 nd = length(dat);
 dout(1:nd)=0;
 dout(1)=0.5*(dat(1)+dat(2));
 dout(nd)=0.5*(dat(nd-1)+dat(nd));
 for i=2:nd-1
     dout(i)=0.5*(0.5*dat(i-1)+dat(i)+0.5*dat(i+1));
 end
end
