%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% vn.m - G. Molera                               %
% Calculates the square root of two inputs       %
% Input: two values                              %
% Output: sqrt(a^2+b^2)                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [out] = vn(a)
 out = sqrt(a(1)^2 + a(2)^2);
end
