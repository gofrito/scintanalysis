%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% ChePolyfitW.m - G. Molera                      %
% Calculate Chebyshev Polynomial function        %
% Input: vectors x and y, weight and Npol        %
% Output: vector yf                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [dout] = ChePolyfitW(x,y,w,np)
nx = length(x);
xx = max(x);
xy = min(x);
xc = 0.5*(xx+xy);
xa = 0.5*(xx-xy);
xn = (x-xc)/xa;

Vp(1:np+1)=0;
Mp=zeros(np+1);
yf(1:nx)=0;

 for ii=1:np+1
    tmpTcheb = 0;
    for jj=1:nx
        tmpTcheb = tmpTcheb + Tcheb(ii,xn(jj)).*y(jj).*w(jj);
    end
    Vp(ii) = tmpTcheb;
    for kk=1:np+1
        tmpTcheb = 0;
        for jj=1:nx
            tmpTcheb = tmpTcheb + Tcheb(ii,xn(jj)).*Tcheb(kk,xn(jj)).*w(jj);
        end
        Mp(ii,kk) = tmpTcheb;
    end
 end

 Mr = Mp^(-1);
 Cp = Mr*Vp';

 for jj=1:nx
    tmpTcheb = 0;
    for ii=1:np+1
        tmpTcheb = tmpTcheb + Cp(ii)*Tcheb(ii,xn(jj));
    end
    yf(jj)= tmpTcheb;
 end
 dout = yf;
end
