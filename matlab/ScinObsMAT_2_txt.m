% Print the Table of results stored in a matlab file to text format
filename = '~/Treball/Scintillation/results/ScintObsSummaryTab_r96.mat';
nscans   = 478;
load(filename);

fid = fopen(strcat(filename(1:55),'txt'),'w+');
fprintf(fid,'//----------------------------- obs setup ----------------------------------------------------------------------|-------------------detections-------------------------------------------------------------|\n');
fprintf(fid,'//Obs  Run Scan St         Date/Time       Dwell   Target Coordinates   Local Az/El   SOT    Dist   Ecl Observ. |  SysN   Scint Broad   Scint.spec  PeakSPD  SysN     Solar       Ionosphere TEC    IPS    |\n');
fprintf(fid,'// #    #   #  Name  YYYY MM DD  hh mm ss    s       RA         Dec      deg deg      deg     AU    Lat    Long |   rad    rad    Hz     slope/err   at3mHz  lev     Activity      VEX    Cebre     TEC    |\n');
fprintf(fid,'|---------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|\n');
if 1
 for ii=1:nscans
    fprintf(fid,'| %4d %3d %3d %3d ',nr(ii),ns(ii),nsc(ii),code(ii));
    fprintf(fid,'%6d %2d %2d %4d ',yy(ii),mm(ii),dd(ii));
	
	if(HH(ii)<10) aa = strcat('0',int2str(HH(ii))); else aa = int2str(HH(ii)); end
    if(MM(ii)<10) bb = strcat('0',int2str(MM(ii))); else bb = int2str(MM(ii)); end
    if(SS(ii)<10) cc = strcat('0',int2str(SS(ii))); else cc = int2str(SS(ii)); end
    fprintf(fid,' %s %s %s %5d ',aa,bb,cc,dur(ii));

    fprintf(fid,'%3d %2d %4.1f %3d %2d %4.1f ',rah(ii),ram(ii),ras(ii),deh(ii),dem(ii),des(ii));
    fprintf(fid,'%6.1f %4.1f %6.2f %4.4f %6.1f %6.2f | ',az(ii),el(ii),SOT(ii),AUdist(ii),LatEcl(ii),LonEcl(ii));
    
    fprintf(fid,'%6.3f %6.3f %6.3f %7.3f %4.2f ',PhN(ii),PhSc(ii),Broadening(ii),ScintSlope(ii),ErrorSlope(ii));
    fprintf(fid,'%8.2f %5.2f %7.1f %6.1f ',Peak3mHz(ii),SysNoise(ii),SolarActA(ii),SolarActB(ii));
    fprintf(fid,'%7.3f %7.3f %8.1f |\n',VEX_Ion_TEC(ii),Ceb_Ion_TEC(ii), TEC(ii));
 end
fprintf(fid,'|_______________________________________________________________________________________________________________|__________________________________________________________________________________________|\n');
fprintf(fid,'//----------------------------- obs setup ----------------------------------------------------------------------|-------------------detections-------------------------------------------------------------|\n');
fprintf(fid,'//Obs  Run Scan St         Date/Time       Dwell   Target Coordinates   Local Az/El   SOT    Dist   Ecl Observ. |  SysN   Scint Broad   Scint.spec  PeakSPD  SysN     Solar       Ionosphere TEC    IPS    |\n');
fprintf(fid,'// #    #   #  Name  YYYY MM DD  hh mm ss    s       RA         Dec      deg deg      deg     AU    Long    Lat |   rad    rad    Hz     slope/err   at3mHz  lev     Activity      VEX    Cebre     TEC    |\n');
fprintf(fid,'|---------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|\n');
fprintf(fid,'//\n');
fprintf(fid,'//Columns meaning:\n');
fprintf(fid,'//Column   1    Obs #                   - Sequential number of the scan acquired and processed;\n');
fprintf(fid,'//Column   2    Run #                   - Sequential number of the observational run for single- or multi-station observations;\n');
fprintf(fid,'//Column   3    Scan #                  - Sequential number of the scan acquired and processed within the observational run;\n');
fprintf(fid,'//Column   4    St.Name and index       - Name of the observing station, stations are indexed in the order of appearance in the project;\n');
fprintf(fid,'//                                         Site          Country     Elev (m)  Longitude (E)  Latitude (N) Diameter (m)  SEFD (Jy)\n');
fprintf(fid,'//                              1  MH - Metsahovi,       Finland       75.63    024 23 35      60 13 04.1      14          3200  \n'); 
fprintf(fid,'//                              2  MC - Medicina,        Italy         67.18    011 38 49.0    44 31 13.8      32           320  \n');
fprintf(fid,'//                              3  MA - Matera,          Italy        543.37    016 42 14.5    40 38 58.3      20          3000  \n');
fprintf(fid,'//                              4  NT - Noto,            Italy        143.23    014 59 20.6    36 52 33.8      32           770  \n');
fprintf(fid,'//                              5  WZ - Wettzell,        Germany      669.12    012 52 38.8    49 08 42.0      20           750  \n');
fprintf(fid,'//                              6  YS - Yebes,           Spain        998.93    356 54 38.1    40 31 27.0      40           200  \n');
fprintf(fid,'//                              7  PU - Pustchino,       Russia                 54  49 20.1    37 37 53.0      22            ?   \n');
fprintf(fid,'//                              9  HH - Hartebeesthoek,  S. Africa    1415      27  41 05.2   -25 53 14.4      26          3000  \n');
fprintf(fid,'//                             10  SH - Sheshai,         China                  00  00 00.0    25 00 00.0      00            ?   \n');
fprintf(fid,'//                             11  KM - Kungming,        China        102       00  00 00.0    25 00 00.0      40            ?   \n');
fprintf(fid,'//                             12  UR - Urumchi,         China        2080      87  00 00.0    43 00 00.0      25            ?   \n');
fprintf(fid,'//Columns  5-10 Date/Time               - UTC of the nominal start of the scan\n');
fprintf(fid,'//Column  11    Dwell                   - nominal time on source (seconds), actual time on source for extracted phases is some 1 minute shorter\n');
fprintf(fid,'//                                        due to the phase extractor PLL set-up time;\n');
fprintf(fid,'//Columns 12-17 Target Coordinates      - J2000 coordinates (Ra/Dec) of the Venus at the mid-time of the scan,\n');
fprintf(fid,'//                                        Note that the spacecraft can be offset from Venus by 10-20 arc seconds;\n');
fprintf(fid,'//Columns 18-19 Local Az/El             - Local azimuth and elevation of the target (degrees) at the mid-time of the scan;\n');
fprintf(fid,'//Column  20    SOT                     - Sun-Observer-Target angle;\n');
fprintf(fid,'//Column  21    Dist                    - Distance from the observing station to the center of Venus (AU);\n');
fprintf(fid,'//Column  22    Ecl Obs. Lon/Lat        - Observable elciplitic Latitude and Longitude of the target;\n');
fprintf(fid,'//Column  22    SysN.RMS                - System phase noise RMS (in radians) in a post-PLL band within a band-pass filter 4.0 - 7.0 Hz;\n');
fprintf(fid,'//Column  23    Scint.RMS               - Phase fluctuation RMS (radians) in the Scintillation band-pass filter 0.003 - 3.003 Hz;\n');
fprintf(fid,'//Column  24-25 Scint.spec slope/error  - Slope of the power spectrum of the phase scintillations in a band of 0.003 - 0.25 Hz;\n');
fprintf(fid,'//                                        and a formal error of the slope estimation;\n');
fprintf(fid,'//Column  26    PeakSPD                 - Peak spectral power density of phase fluctuations (radians square per Hz), as measured at a frequency of 3 mHz;\n');
fprintf(fid,'//Column  27    SysN level              - System noise level (radian square per Hz * 10^4)\n');
fprintf(fid,'//Column  28-29 Solar activity          - Min/Max Solar magnetic field strength (nanoTeslas?).\n');


 fclose(fid);
 clear fid aa bb cc ans dd ii nscans

 else
    matrix(:,1) = nr;matrix(:,2) = ns;matrix(:,3) = nsc;matrix(:,4) = code; matrix(:,5) = yy;matrix(:,6) = mm;matrix(:,7) = dd;
    matrix(:,8) = HH;matrix(:,9) = MM;matrix(:,10) = SS;matrix(:,11) = dur;matrix(:,12) = rah;matrix(:,13) = ram;matrix(:,14) = ras;
    matrix(:,15) = deh;matrix(:,16) = dem;matrix(:,17) = des;matrix(:,18) = az;matrix(:,19) = el;matrix(:,20) = SOT;matrix(:,21) = AUdist;matrix(:,22) = LonEcl;matrix(:,23) = LatEcl;
    matrix(:,24) = PhN;matrix(:,25) = PhSc;matrix(:,26) = Broadening;matrix(:,27) = ScintSlope;
    matrix(:,28) = ErrorSlope;matrix(:,29) = Peak3mHz;matrix(:,30) = SysNoise;matrix(:,31) = SolarActA;
    matrix(:,32) = SolarActB;matrix(:,33) = Ionos_TEC;matrix(:,34) = TEC;

    save strcat(filename(1:34),'txt') '-ascii' '-tabs' 'matrix';
 end
    