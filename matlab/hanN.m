%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% hanN.m - G. Molera                             %
% Hanning window from specific vector            %
% Input: data vector and Number of Hanning win   %
% Output: data vectors smoothed                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [dout] = hanN(dat,Nh)
 dout = han(dat);
 for i=1:Nh
    dout = han(dout);
 end
end
