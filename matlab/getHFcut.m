%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% getHFcut.m - G. Molera                         %
% Get the high frequency limit in the vector     %
% Input: frequency, fit and level                %
% Output: frequency cut                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [fcut] = GetHFCut(fs,fit,Level)
 nd = length(fs);
 bcut = 0;
 for jj=1:nd-1
     if((fit(jj)>Level)&&(fit(jj+1)<=Level))
         bcut = jj;
     end
 end
 fcut = fs(bcut);
end
