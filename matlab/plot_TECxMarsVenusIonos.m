%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% plot_TECxMarsVenusIonos.m - G. Molera          %
% Plot the TEC for Mars and Venus and Ionos      %
% No input just calling TECxMars/Venus/Ionos sc  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\nMeasuring TEC values for Mars, Venus and Ionosphere\n');
fprintf('TECxMars  ');
TECxMars;
fprintf(' OK\nTECxVenus ');
TECxVenus;
fprintf(' OK\nTECxIonos ');
TECxIonos;
fprintf(' OK\n');

% Tunning up
figure(2);
legend('Mars','Venus','Ionosphere');
xlabel('Solar elongation (degrees)','FontSize',12,'FontName','Arial');
ylabel('TEC (cm^{-3})','FontSize',12,'FontName','Arial');
title('Simulated TEC for the MEX, VEX signal path and ionosphere','FontSize',13,'FontName','Arial');
