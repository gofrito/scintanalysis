%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATLAB tools to analyze Interplanetary Plasma  %
% scintillation observations with VEX            %
%                                                %
% scint_analysis_function.m - G. Molera          %
% version number 8 - 07/05/2010                  %
% handles function                               %
% Input: filename and Number of Scans            %
% Output: pspaw, fitline and ff                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [pspaw,fitline2,ff]= scint_analysis (filename,Ns)
format long
begin = now;

%% Setting global variables to change each iteration
%filename = '../Phase_data/Phases.vex2009.11.26.WZ.txt';
%Ns   = 5;       % number of scans.
flc  = 0.003;   % low  boundary of the scintillation band in Hz.
fhc  = 3.003;   % high boundary of the scintillation band in Hz.
fns  = 4;       % low  boundary of the noise in Hz
fne  = 7;       % high boundary of the noise in Hz
%alfa = -2.25;   % Alfa is a guess, we should try with several numbers.
flo = 0.003;
fhi = 0.2;

%% The processing part starts here.
fprintf('\n');
fprintf('We open the file : %s \n',filename);
fprintf('Number of scans  : %3.1f \n',Ns);

% Read the values from the daily phase file 
Ph_tmp = textread(filename);

tp  = Ph_tmp(:,1);
Np  = length(tp);
dtp = tp(2)-tp(1);
Ts  = dtp*Np;
df  = 1/Ts;
BW  = Np*df;
BWo = 0.5*BW;
Fa = 10;
bfa = floor(Fa/df);
Ph  = zeros(Ns,Np);

for i=1:Ns
   Ph(i,:)= Ph_tmp(:,i+1);
end

js = 1:Ns; 
jp = 0:1:(Np-1);
ff = jp*df;

% Create a window in time domain - Unity window, cosine window and cosine
% square window.
winU(1:Np)=1;
winC1 = cos(pi/Np.*(jp-0.5*Np)).^1;
winC2 = cos(pi/Np.*(jp-0.5*Np)).^2;


% figure;plot(tp,Ph,'r');ylabel('Ph');xlabel('tp');
% title('Plot all the Phases in same plot');

% Get max values of the singal
Phx(1:Ns) = 0;
Phn(1:Ns) = 0;
Phr(1:Ns) = 0;
Phm(1:Ns) = 0;
for k=1:Ns
    Phx(k) = max(Ph(k,:));
    Phn(k) = min(Ph(k,:));
    Phr(k) = std(Ph(k,:));
    Phm(k) = mean(Ph(k,:));
end

fprintf('Phx: %s \nPhn: %s \nPhr: %s \nPhm: %s\n',mat2str(round(Phx*100)/100),mat2str(round(100*Phn)/100),mat2str(round(100*Phr)/100),mat2str(round(Phm*100)/100));

Et(1:Ns)=0;
% Compute the "Total Energy" of the fluctuactions
for k=1:Ns
    for j=1:Np
        Et(k) = Et (k) + dtp*Ph(k,j)^2;
    end
end
Pt = Et./Ts;
fprintf('Compute the "Total Energy" \n');
fprintf('sqrt(Pt): %s \nand RMS : %s\n',mat2str(sqrt(Pt)),mat2str(Phr));

% Make the spectra
sp=zeros(Ns,Np);
spw=zeros(Ns,Np);
psp=zeros(Ns,Np);
pspw=zeros(Ns,Np);

WinOpt = 1;
win = winU;
for i=1:Np
    if (WinOpt == 1)
        win=winC1;
    end
    if (WinOpt == 2)
        win = winC2;
    end
end
for i=1:Ns
    sp(i,:) = fft(Ph(i,:));
    spw(i,:) = fft(Ph(i,:).*win);
end;

for i=1:Ns    
    psp(i,:) = (abs(sp(i,:)).^2) / Np;
    pspw(i,:) = (abs(spw(i,:)).^2) / Np;
end

% Check again power levels and RMS
Pf = sum(psp,2)/Np;

fprintf('Compare power levels in time and frequency domain \n');
fprintf('Pf: %s\nPt: %s\n',mat2str(round(Pf*100)/100),mat2str(round(Pt*100)/100));
fprintf('\n');

% figure;semilogy(ff,psp,'r');ylabel('psp');xlabel('ff');
% title('2-sided power spectra plot');grid on;

%% Normalise the power spectra in absolute mode.
psp  = 2*psp/BW;
pspw = 2*pspw/BW;
Pft  = sum(psp(:,1:bfa),2).*df;

fprintf('Compare power levels in time and frequency domain \n');
fprintf('Pft: %s\nPt: %s\n',mat2str(round(Pft*100)/100),mat2str(round(Pt*100)/100));
fprintf('\n');

% figure;loglog(ff,psp,'r');ylabel('psp');xlabel('ff');grid on;
% title('logarithmic power spectra in units square radians per Hz');

% Now we isolate the scintillation and system band noise:
% making the average spectrum
% and smooth with a Hann window
pspa  = sum(psp,1)/Ns;
pspaw = sum(pspw,1)/Ns;
pspah = han(pspa);

% We compute the system noise level. 
snm = Submean(pspaw,ff,fns,fne);
fprintf('System noise average: %6.5f\n',snm);

% figure;loglog(ff,pspan,'r');xlabel('ff');ylabel('pspa');
% title('average power spectra scan and smoothed by Hann window');grid on;

%% Method 1 to calculate the scintillation BW
% Extract the part of the spectrum within the boundaries
bbeg = floor(flo/df+0.5)+1;
bend = floor(fhi/df+0.5)+1;

ffs = ff(bbeg:bend);
pss = pspaw(bbeg:bend);

% Now will convert into Log-Log domain
Lffs = log10(ffs);
Lpss = log10(pss);
Lf   = polyfit(Lffs,Lpss,1);

Lfit = Lf(2) + Lf(1).*Lffs;

%figure;plot(Lffs,Lpss,'r');hold on;plot(Lffs,Lfit,'b'); grid on;
%ylabel('Lpss\nLfit');xlabel('Lffs');title('Comparison between spectra average and fitting curve');

%Check the goodness of fit
dLfit     = Lpss - Lfit;
rdLfit    = std(dLfit);
Slope     = Lf(1);
errSlope  = rdLfit/(max(Lffs)-min(Lffs));
var       = -errSlope/Slope;
fprintf('Slope: %6.3f \nerrSlope: %6.3f \nerrSlope/-Slope: %6.3f (0.02818 in previous results)\n',Lf(1),errSlope,var);

%% Plot again the full spectra and the Fit Line
Lfitline = Lf(2) + Lf(1).*log10(ff+0.001.*df);
fitline = 10.^(Lfitline);

%loglog(ff,pspa,'r');hold on;loglog(ff,fitline,'b');grid on;
%xlim([10^-3,10]);title('Comparison between spectra aver. and fitting curve in freq domain');
%xlabel('ff');ylabel('pspa-fitline');

% Find a frequency at which the fit line will cross certain level
% Lfo + Lf1*log(fcross)=log(crosslevel)
CrossLev1   = snm;
Lfcross1    = (log10(CrossLev1) - Lf(2))/Lf(1);
fcross1     = 10^(Lfcross1);
CrossLev2   = 10^-5;
Lfcross2    = (log10(CrossLev2) - Lf(2))/Lf(1);
fcross2     = 10^(Lfcross2);
HighFscint  = fcross2;
fprintf( 'fcross1 : %6.3f HighFscint or fcross2: %6.3f \n', fcross1,fcross2);

%% Method 2 to calculate the scintillation BW
% Integrate the total power of the scintillation with a high BW. fhi=8;
bfhi = floor(8/df+0.5);

Npi = bfhi - bbeg;
jpi = 1:Npi-1;
fp1 = ff(bbeg) + jpi.*df;

ScintTotalPower(1:Npi-1)=0;
ScintTotalPower(1) = fitline(bbeg)*df;
for jj=1:Npi-2
    ScintTotalPower(jj+1) = ScintTotalPower(jj) + fitline(bbeg+jj+1)*df;
end
NoiseTotalPower = (jpi).*df;
Xstp = max(ScintTotalPower);

ScintTotalPower = ScintTotalPower/Xstp;
NoiseTotalPower = NoiseTotalPower/Xstp;

%% Filtering and outputtig the data.
FiltScint(1:Np)=0;
FiltNoise(1:Np)=0;
for i=1:Np
    if ((flc <= ff(i)) && (ff(i) <= fhc))
        FiltScint(i) = 2;
    end
    if ((fns <= ff(i)) && (ff(i) <= fne))
        FiltNoise(i) = 2;
    end
end

% Analysis of the Scintillation band.
sps=zeros(Ns,Np);
phs=zeros(Ns,Np);
rmsphs(1:Ns)=0;

for k=1:Ns
     sps(k,:) = sp(k,:).*FiltScint;
     phs(k,:) = real(ifft(sps(k,:)));
     rmsphs(k) = std(phs(k,:));
     phs(k,1)
end

%Analysis of the System Noise band
spn=zeros(Ns,Np);
phn=zeros(Ns,Np);
rmsphn(1:Ns)=0;

for k=1:Ns
    spn(k,:) = sp(k,:).*FiltNoise; 
    phn(k,:) = real(ifft(spn(k,:)));
    rmsphn(k) = std(phn(k,:));
end


fprintf('Phase fluctuation RMS in the System noise band and Scintillation band');
fprintf('\n');
fprintf('HighFscint : %6.3f \n',fhc);
fprintf('System phase noise RMS \n');
fprintf('%s \n',mat2str(round(10000*rmsphn)/10000));
fprintf('Scintillation phase noise RMS \n');
fprintf('%s \n',mat2str(round(10000*rmsphs)/10000));

% % Check for excessive high frequency scintillations
 SysNoiseLevel = snm;
 PeakScint = pspa(bbeg);
 pspan = pspa;
 noiseline = SysNoiseLevel;
 fitline1 = fitline;
 fitline2 = fitline + noiseline;

fin = now;
fprintf(' %6.2f time ellapsed\n',(fin-begin)*3600*24);
end