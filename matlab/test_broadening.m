%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% test_broadening.m - G. Molera                  %
% test the broadening effect on the phase        %
% Input:                                         %
% Output:                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data = handles.espash;
ld = length(data);
lm = mean(data(4000:7000));
filt(1:ld) = 0.8;
xmin = ld;
xmax = 0;

for k=6:ld-5
    if(mean(data(k-5):data(k+5)) > 1.65*lm)
        if ( k < xmin)
            xmin = k;
        end
        if ( k > xmax)
            xmax = k;
        end
    end
end

filt(xmin:xmax) = 2;    

figure(1), hold off;
loglog(handles.ffs,handles.espash,'b');hold on;
loglog(handles.ffs,filt,'r');xlim([0.01,10]);


ffmin=handles.ffs(xmin);
ffmax=handles.ffs(xmax);
[x,y]= max(handles.espash);
ffpeak=handles.ffs(y);
