%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% Kolmogorov_simulation - G. Molera              %
% Simulation the phase scintillation spectra     %
% of the signal propagation in the IPS, like a   %
% Kolmogorov media                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
time1= now;

%Total Electron Content Unit
TECU = 10e6;   		% electrons per square metre.
kph  = 2*pi*1.34e9; % constant converts TEC values to Phase.
Fo   = 8.4e9;    	% Observing Frequency
Vx   = 500;      	% Solar wind velocity in (km/s)

% Setting the simulation parameters
Xmax = 0.6e6;   	% Widht transversal of the line of sight in km.
dx   = 25;      	% Step in X-coordinate direction.
Nx   = Xmax/dx;   	% xbinnning
jx   = 0:1:Nx-1;  
x    = jx.*dx;
dtx  = dx/Vx;    	% time = space/velocity of the wind
tt   = jx.*dtx;
Tt   = Nx*dtx;
dff  = 1/Tt;
Nf   = Nx/2+1;    	% and in frequency domain
jf   = 0:1:Nf-1;
ff   = jf.*dff;
Ymax = 0.6e6;   	% Width parallel to the line of sight.

%% Generating the "multi-bubble" model to fill the pipe, we assume a
%% multi-modal Gaussian distrubution of bubble statistics to approximate
%% Kolmogorov statistics.

fprintf('Nb is a number of bubbles in the pipe\nrb is a radius of the bubble(km)\n');
fprintf('xy,yb - are the coordinates in km\nAb-amplitude of an electron content(1/cm^3)\n');

CN = 2 ;    % Component number factor
fprintf('Distribution component 0');
Nb0  = 40*CN;
jb0  = 0:1:(Nb0-1);
s    = rand(1,1)
rbm0 = s*300*1000;
Abm0 = 1;
xb0  = -4*rbm0 + (Xmax+8*rbm0).*rand(1,Nb0);
yb0  = 0 + Ymax.*rand(1,Nb0);
rb0  = 0 + rbm0.*randn(1,Nb0);
rb0  = abs(rb0);
Ab0  = Abm0.*(1-0.7.*randn(1,Nb0));

fprintf('.1');
Nb1  = 100*CN;
jb1  = 0:1:(Nb1-1);
s    = rand(1,1)
rbm1 = s*100*1000;
Abm1 = 1;
xb1  = -4*rbm1 + (Xmax+8*rbm1).*rand(1,Nb1);
yb1  = 0 + Ymax.*rand(1,Nb1);
rb1  = 0 + rbm1.*randn(1,Nb1);
rb1  = abs(rb1);
Ab1  = Abm1.*(1-0.7.*randn(1,Nb1));

fprintf('.2');
Nb2  = 230*CN;
jb2  = 0:1:(Nb2-1);
s    = rand(1,1)
rbm2 = s*30*1000;
Abm2 = 1;
xb2  = -4*rbm2 + (Xmax+8*rbm2).*rand(1,Nb2);
yb2  = 0 + Ymax.*rand(1,Nb2);
rb2  = 0 + rbm2.*randn(1,Nb2);
rb2  = abs(rb2);
Ab2  = Abm2.*(1-0.7.*randn(1,Nb2));

fprintf('.3');
Nb3  = 800*CN;
jb3  = 0:1:(Nb3-1);
s    = rand(1,1)
rbm3 = s*10*1000;
Abm3 = 1;
xb3  = -4*rbm3 + (Xmax+8*rbm3).*rand(1,Nb3);
yb3  = 0 + Ymax.*rand(1,Nb3);
rb3  = 0 + rbm3.*randn(1,Nb3);
rb3  = abs(rb3);
Ab3  = Abm3.*(1-0.7.*randn(1,Nb3));

fprintf('.4');
Nb4  = 1220*CN;
jb4  = 0:1:(Nb4-1);
s    = rand(1,1)
rbm4 = s*3*1000;
Abm4 = 1;
xb4  = -4*rbm4 + (Xmax+8*rbm4).*rand(1,Nb4);
yb4  = 0 + Ymax.*rand(1,Nb4);
rb4  = 0 + rbm4.*randn(1,Nb4);
rb4  = abs(rb4);
Ab4  = Abm4.*(1-0.7.*randn(1,Nb4));

fprintf('.5');
Nb5  = 2800*CN;
jb5  = 0:1:(Nb5-1);
s    = rand(1,1)
rbm5 = s*1*1000;
Abm5 = 1;
xb5  = -4*rbm5 + (Xmax+8*rbm5).*rand(1,Nb5);
yb5  = 0 + Ymax.*rand(1,Nb5);
rb5  = 0 + rbm5.*randn(1,Nb5);
rb5  = abs(rb5);
Ab5  = Abm5.*(1-0.7.*randn(1,Nb5));

fprintf('.6');
Nb6  = 6500*CN;
jb6  = 0:1:(Nb6-1);
s    = rand(1,1)
rbm6 = s*0.3*1000;
Abm6 = 1;
xb6  = -4*rbm6 + (Xmax+8*rbm6).*rand(1,Nb6);
yb6  = 0 + Ymax.*rand(1,Nb6);
rb6  = 0 + rbm6.*randn(1,Nb6);
rb6  = abs(rb6);
Ab6  = Abm6.*(1-0.7.*randn(1,Nb6));

fprintf('.7\n');
Nb7  = 14800*CN;
jb7  = 0:1:(Nb7-1);
s    = rand(1,1)
rbm7 = s*0.1*1000;
Abm7 = 1;
xb7  = -4*rbm7 + (Xmax+8*rbm7).*rand(1,Nb7);
yb7  = 0 + Ymax.*rand(1,Nb7);
rb7  = 0 + rbm7.*randn(1,Nb7);
rb7  = abs(rb7);
Ab7  = Abm7.*(1-0.7.*randn(1,Nb7));

% Combine the distribution data.
Nb = Nb0+Nb1+Nb2+Nb3+Nb4+Nb5+Nb6+Nb7;
rb = [rb0 rb1 rb2 rb3 rb4 rb5 rb6 rb7];
xb = [xb0 xb1 xb2 xb3 xb4 xb5 xb6 xb7];
yb = [yb0 yb1 yb2 yb3 yb4 yb5 yb6 yb7];
Ab = [Ab0 Ab1 Ab2 Ab3 Ab4 Ab5 Ab6 Ab7];
jb = 0:1:(Nb-1);
%Toal electron content in the buble jb
Abr = sqrt(2*pi).*Ab.*rb;

% Checking the statistics and compare with Kolmogorov distribution
% fprintf('Min: %f\nMax: %f\n',min(rb),max(rb));

Xrb=500e3;
Nbb=2001;
dbb = Xrb/(Nbb-1);
Nbc = Nbb-1;
bb=(0:1:Nbb-1).*dbb;
bc = bb + 0.5.*dbb;
% make a histogram
Hstr = hist(rb,bb);

minrb = 0.1e3; 
maxrb = 0.2e5;
bcb   = floor(minrb/dbb+0.5+1);
bce   = floor(maxrb/dbb+0.5+1);
bcs   = bc(bcb:bce);
szs   = Hstr(bcb:bce)+1;
Lbcs  = log10(bcs);
Lszs  = log10(szs);
Ls    = polyfit(Lbcs,Lszs,1);

fprintf(2,'Kolmogorv dist: %6.2f\n',Ls(1));
Lszfit = Ls(2)+Ls(1).*Lbcs;
szfit  = 10.^Lszfit;


% figure('Position',[150 600 425 300],'Units', 'centimeters');
% set(gca,'Position',[0.11 0.18 0.84 0.75],'Units','normalized','fontsize',10,'fontname','Times New Roman');
% loglog(bc,Hstr,'r');hold on;loglog(bcs,szfit,'b');grid on;
% title('Distribution of sizes of bubbles','fontsize',11,'fontname','Times New Roman');
% xlabel('bins','fontsize',11,'fontname','Times New Roman');
% ylabel('Hist','fontsize',11,'fontname','Times New Roman');

% figure('Position',[150 600 425 300],'Units','centimeters');
% set(gca,'Position',[0.11 0.18 0.84 0.75],'Units','normalized','fontsize',10,'fontname','Times New Roman');
% semilogy(rb,Ab,'r');grid on;
% title('Peak electron concentration (in units 1/cm^3) vs. bubble size');

%% Assuming the Gaussian profile of the electron density distribution
%% within the bubbles
% Integrate TEC width time sampling or 10 Hz phase tracking band. dtx=0.05s
dtx = 0.05;
tec0(1:Nx)=0;tec1(1:Nx)=0;tec2(1:Nx)=0;tec3(1:Nx)=0;
tec4(1:Nx)=0;tec5(1:Nx)=0;tec6(1:Nx)=0;tec7(1:Nx)=0;
% tecx represents the integral according the time sampling

for jj=1:Nx
    tec0(jj) = sqrt(2*pi)*sum(Ab0.*rb0.*exp(-((xb0 - x(jj)).^2)./(2.*rb0.^2)));%Nbo
    tec1(jj) = sqrt(2*pi)*sum(Ab1.*rb1.*exp(-((xb1 - x(jj)).^2)./(2.*rb1.^2)));
    tec2(jj) = sqrt(2*pi)*sum(Ab2.*rb2.*exp(-((xb2 - x(jj)).^2)./(2.*rb2.^2)));
    tec3(jj) = sqrt(2*pi)*sum(Ab3.*rb3.*exp(-((xb3 - x(jj)).^2)./(2.*rb3.^2)));
    tec4(jj) = sqrt(2*pi)*sum(Ab4.*rb4.*exp(-((xb4 - x(jj)).^2)./(2.*rb4.^2)));
    tec5(jj) = sqrt(2*pi)*sum(Ab5.*rb5.*exp(-((xb5 - x(jj)).^2)./(2.*rb5.^2)));
    tec6(jj) = sqrt(2*pi)*sum(Ab6.*rb6.*exp(-((xb6 - x(jj)).^2)./(2.*rb6.^2)));
    tec7(jj) = sqrt(2*pi)*sum(Ab7.*rb7.*exp(-((xb7 - x(jj)).^2)./(2.*rb7.^2)));
end
tec0 = tec0./TECU; tec1 = tec1./TECU; tec2 = tec2./TECU; tec3 = tec3./TECU;
tec4 = tec4./TECU; tec5 = tec5./TECU; tec6 = tec6./TECU; tec7 = tec7./TECU;

ph0 = kph*tec0/Fo; ph1 = kph*tec1/Fo; ph2 = kph*tec2/Fo; ph3 = kph*tec3/Fo;
ph4 = kph*tec4/Fo; ph5 = kph*tec5/Fo; ph6 = kph*tec6/Fo; ph7 = kph*tec7/Fo;

fprintf('std(ph0): %6.3f mean(tec0): %6.3f std(tec0): %6.3f\n',std(ph0),mean(tec0),std(tec0));
fprintf('std(ph1): %6.3f mean(tec1): %6.3f std(tec1): %6.3f\n',std(ph1),mean(tec1),std(tec1));
fprintf('std(ph2): %6.3f mean(tec2): %6.3f std(tec2): %6.3f\n',std(ph2),mean(tec2),std(tec2));
fprintf('std(ph3): %6.3f mean(tec3): %6.3f std(tec3): %6.3f\n',std(ph3),mean(tec3),std(tec3));
fprintf('std(ph4): %6.3f mean(tec4): %6.3f std(tec4): %6.3f\n',std(ph4),mean(tec4),std(tec4));
fprintf('std(ph5): %6.3f mean(tec5): %6.3f std(tec5): %6.3f\n',std(ph5),mean(tec5),std(tec5));
fprintf('std(ph6): %6.3f mean(tec6): %6.3f std(tec6): %6.3f\n',std(ph6),mean(tec6),std(tec6));
fprintf('std(ph7): %6.3f mean(tec7): %6.3f std(tec7): %6.3f\n',std(ph7),mean(tec6),std(tec7));

% Aggregate the contributions of the components
tec=tec0+tec1+tec2+tec3+tec4+tec5+tec6+tec7;

mtec = mean(tec);
rtec = tec - mtec;
fprintf('std(rtc): %6.3f error: %6.3f\n',std(rtec),std(rtec)/mtec);

% Convert TEC fluctuactions to phase fluctuactions at our observing
% frequency.
ph = rtec.*kph./Fo;

% Remove the 6th order Chebyshev fit from phase fluctuactions as did in
% observations and apply the window.
wgt(1:Nx) = 1;
Npfit     = 6;
phfit     = ChePolyfitW(tt,ph,wgt,Npfit);
rph       = ph - phfit;

fprintf('std(ph): %6.3f; std(rph): %6.3f\n',std(ph),std(rph));
% Apply a window
win       = cos((pi/Nx).*(jx - 0.5.*Nx)).^1;
rphw      = rph.*win;

% Make spectrum
sp   = fft(rphw);
psp  = (abs(sp(1:0.5*Nx+1)).^2)./(0.5*Nx+1);
fb   = 0.003;
fe   = 1;
bbeg = floor(fb/dff + 0.5+1);
bend = floor(fe/dff + 0.5+1);

% Make the estimate of the spectrum slope
ffs      = ff(bbeg:bend);
psps     = psp(bbeg:bend);
Lsps     = log10(psps);
Lffs     = log10(ffs);
Lfs      = polyfit(Lffs,Lsps,1);
Lfitline = Lfs(2) + Lfs(1).*Lffs;
fitline  = 10.^Lfitline;
Slope    = Lfs(1);
fprintf(2,'Slope: %6f3\n',Slope);

% Add a ssytem Noise to the system
sysNoise = 10^7;
psp      = psp + sysNoise;

time2    = now;
fprintf('Total time ellapsed: %6.5f\n',(time2-time1)*3600);
