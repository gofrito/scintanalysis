filename = 'Horizons_2years.txt';
nrsamples= 72;

fid = fopen(filename,'r');
if (fid < 1)
    fprintf(2,'Error opening the file\n');
end

 % remove the headers from the beginning of the file
for k=1:2
    fgetl(fid);
end
                   % 2008-Jul-11    00:00   08 00  05 +21  44   43  12  -7  -3  0.7 1.7  -3  8. /T  12. 7.   1.
 C = textscan(fid,'%s %s %s %d %d %d %d %f %d %d %f %f %f %f %f %f %d %f %s %f %f %f',nrsamples);

 fclose(fid);
 size(C)
 
 ec   = C{1,22};
 YY   = C{1,1};
 MM = C{1,2};
 DD  = C{1,3};
 SO  = C{1,18};
 
 filename='';
 fid = fopen(filename2,'r');
 if(fid<1)
    fprintf('The file has not been opened correctly');
 end

 % remove the headers from the beginning of the file
 for k=1:4
    fgetl(fid);
 end

 C = textscan(fid,'%s %d %d %d %s %d %d %d %d %d %d %d %d %d %f %d %d %d %f %f %f %f %f %s %f %f %f %f %f %f %f %f %f %s',nrsamples);

 % Please find out inside the text file which is each column for
 nr=C{1,2};
 ns=C{1,3};
 nsc=C{1,4};
 station=C{1,5};
 nstation=C{1,6};
 yy=C{1,7};
 mm=C{1,8};
 dd=C{1,9};
 HH=C{1,10};
 MM=C{1,11};
 SS=C{1,12};
 dur=C{1,13};
 rah=C{1,14};
 ram=C{1,15};
 ras=C{1,16};
 deh=C{1,17};
 dem=C{1,18};
 des=C{1,19};
 az=C{1,20};
 el=C{1,21};
 SOT=C{1,22};
 AUdist=C{1,23};
 phn=C{1,25};
 phsc=C{1,26};
 frq=C{1,27};
 ScintSlope=C{1,28};
errorSlope=C{1,29};
Peak3mHz=C{1,30};
SysN=C{1,31};
SolarActA=C{1,32};
SolarActB=C{1,33};

for i=1:nrsamples
    write(fid,'%s %d %d %d %s %d %d %d %d %d %d %d %d %d %f %d %d %d %f %f %f %f %f %s %f %f %f %f %f %f %f %f %f %s',nr(i),);
end
