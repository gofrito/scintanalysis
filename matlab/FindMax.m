%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% FindMax.m - G. Molera                          %
% Find the peak spectra of a window data         %
% Input: spectrum, frequency scale, search       % 
%        windows defined by max and min          % 
% Output: bin of max value, parabolic estimate   %
%        of max position, max value              % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function [out] = FindMax(Spec,Fscale,Fmin,Fmax)
 np = length(Spec);
 mx = 0;
 jmax = 1;

 for jj =1:np
  if (Spec(jj) > mx && Fmin < Fscale(jj) && Fscale(jj) < Fmax)
    mx = Spec(jj);  % max value from the spectra
    jmax = jj;      % return position on the vector of the max value.
  end;
 end;
 
 if (jmax == 1)
    jmax = 2;
 end;
 if (jmax == np)
  jmax = np-1;
 end;
 
 a2 = 0.5*(Spec(jmax-1) + Spec(jmax+1) - 2*Spec(jmax));
 a1 = 0.5*(Spec(jmax+1) - Spec(jmax-1));
 djx = -a1/(2*a2);
 xmax = jmax + djx;
 out = cat(1,jmax,xmax,mx);
end
