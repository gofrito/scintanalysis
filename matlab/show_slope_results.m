%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% show_slope_results.m - G. Molera               %
% Standalone file                                %
% Plot the scintillation slope for the sessions  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Reading the text file with the observations Table
function show_slope_results(plt)
 fprintf('1 - Remember to update the principal table and number of samples\n');
 filename  = '../results/ScintObsSummaryTab_r103.txt';
 nrsamples = 520;
 flag      = 0;
 plt       = 2;

% The file is a generic text file .txt
out     = read_scint_table(filename,nrsamples,flag);
SOT     = out.SOT;
yy      = out.yy;
mm      = out.mm;
dd      = out.dd;
HH      = out.HH;
MM      = out.MM;
SS      = out.SS;
phsc    = out.phsc;
phn     = out.phn;
frq     = out.frq;
ns      = out.ns;
nstation= out.nstation;
ScSlope = out.ScintSlope;

% Initialise some parameters to 0.
cs      = 0;
nscans  = 0;
nses    = 0;
slope(1:max(ns)) = 0;
stsot(1:max(ns)) = 0;
nsta(1:max(ns))  = 0;
dt(1:max(ns))    = 0;
mhn     = 0;
on      = 0;

for ii=1:nrsamples
    dt(ii) = mjd(yy(ii),mm(ii),dd(ii),HH(ii),MM(ii),SS(ii));
end
size(dt)

for jj=1:length(ns)
    if (ns(jj) == cs)
        nscans = nscans + 1;
    else
        nscans      = nscans + 1;
        cs          = ns(jj);
        nses        = nses + 1;
        slope(nses) = ScSlope(jj);
        stsot(nses) = SOT(jj);
        nsta(nses)  = nstation(jj);
    end
end
slope(1:10)

 switch plt
    case 1
        figure('Position',[150 600 425 220],'Units', 'centimeters'); 
        %    set(gca,'Position',[0.11 0.18 0.84 0.72],'Units','normalized','fontsize',10,'fontname','Times New Roman');
        plot(slope,'ro');
        ylim([-3 -1.9]);xlim([0 max(nses)]);grid on;
        mSlope   = mean(slope);
        stdSlope = std(slope);
        line(0:0.01:length(slope),mSlope+stdSlope,'LineStyle','-','color','b');
        line(0:0.01:length(slope),mSlope-stdSlope,'LineStyle','-','color','b');
        title('Fluctuations spectral index vs time','fontname','Times New Roman','fontsize',12);
        xlabel('Sessions','fontname','Times New Roman','fontsize',12);
        ylabel('Scint. slope','fontname','Times New Roman','fontsize',12);
     case 2
        figure('Position',[150 600 425 220],'Units', 'centimeters'); 
        set(gca,'Position',[0.11 0.18 0.84 0.72],'Units','normalized','fontsize',12,'fontname','Times New Roman');
        plot(stsot,slope,'rx');
        ylim([-3 -1.8]);xlim([0 47]); grid on;
        mSlope   = mean(slope);
        stdSlope = std(slope);
        %line(0:0.01:length(slope),mSlope+stdSlope,'LineStyle','-','color','c');
        %line(0:0.01:length(slope),mSlope-stdSlope,'LineStyle','-','color','c');
        %line(0:0.01:length(slope),mSlope,'LineStyle','-','color','b');
        title('Fluctuations spectral index vs solar elongation','fontname','Times New Roman','fontsize',13);
        xlabel('Solar Elongation (deg)','fontname','Times New Roman','fontsize',12);
        ylabel('Scint. slope','fontname','Times New Roman','fontsize',12);
    case 3
        figure('Position',[150 600 425 220],'Units', 'centimeters'); 
        set(gca,'Position',[0.11 0.18 0.84 0.72],'Units','normalized','fontsize',12,'fontname','Times New Roman');
        plot(stsot,slope,'bx','MarkerSize',7);xlim([0 45]); grid on;hold on;
        title('Slope of the scintillation vs SOT','fontname','Times New Roman','fontsize',13);
        xlabel('SOT (degrees)','fontname','Times New Roman','fontsize',12);
        ylabel('Scint slope','fontname','Times New Roman','fontsize',12);
        Lc  = polyfit(stsot,slope,1);
        ss  = 0:0.01:47;
        fit = Lc(2) + Lc(1).*ss;
        plot(ss,fit,'c','LineWidth',2);        
    case 4
        figure('Position',[150 600 425 250],'Units', 'centimeters');
        set(gca,'Position',[0.12 0.17 0.85 0.74],'Units','normalized','fontsize',10,'fontname','Times New Roman');
        hold on; grid on;
        ot = 0;
        on = 0;
        mh = 0;
        ma = 0; mc = 0; no = 0; wz = 0; os = 0; ys = 0; hh = 0;
        for jj=1:nses
            if (nsta(jj) == 1)
                if ( -2.1 > slope(jj) && slope(jj) > -2.8)
                    plot(stsot(jj),slope(jj),'rx');
                    mh = mh + 1;
                    mh_stsot(mh) = stsot(jj);
                    mh_phn(mh) = phn(jj);
                    mh_slope(mh) = slope(jj);
                end
            end
            if (nsta(jj) == 2) % MC
                plot(stsot(jj),slope(jj),'bx');
                mc  = mc + 1;
                mc_slope(mc) = slope(jj);
                on  = on + 1;
                on_stsot(on) = stsot(jj);
                on_slope(on) = slope(jj);
            end
            if (nsta(jj) == 3) % NO
                plot(stsot(jj),slope(jj),'cx');
                no  = no + 1;
                no_slope(no) = slope(jj);
                ot  = ot + 1;
                ot_stsot(ot) = stsot(jj);
                ot_slope(ot) = slope(jj);
            end
            if (nsta(jj) == 4) % MA
                plot(stsot(jj),slope(jj),'cx');
                ma  = ma + 1;
                ma_slope(ma) = slope(jj);
                ot  = ot + 1;
                ot_stsot(ot) = stsot(jj);
                ot_slope(ot) = slope(jj);
            end
            if (nsta(jj) == 5) % WZ
                plot(stsot(jj),slope(jj),'bx');
                wz  = wz + 1;
                wz_slope(wz) = slope(jj);
                on  = on + 1;
                on_stsot(on) = stsot(jj);
                on_slope(on) = slope(jj);
            end
            if (nsta(jj) == 6) % YS
                plot(stsot(jj),slope(jj),'bx');
                ys  = ys + 1;
                ys_slope(ys) = slope(jj);
                on  = on + 1;
                on_stsot(on) = stsot(jj);
                on_slope(on) = slope(jj);
            end
            if (nsta(jj) == 7) % PU
                % plot(stsot(jj),slope(jj),'ko');
            end
            if (nsta(jj) == 8) % ON
                plot(stsot(jj),slope(jj),'bx');
                os  = os + 1;
                os_slope(os) = slope(jj);
                os_phn(os) = phn(jj);
                on  = on + 1;
                on_stsot(on) = stsot(jj);
                on_slope(on) = slope(jj);
            end
            if (nsta(jj) == 9) % HH
                plot(stsot(jj),slope(jj),'kx');
                hh  = hh + 1;
                hh_slope(hh) = slope(jj);
                hh_phn(hh) = phn(jj);
                ot  = ot + 1;
                ot_stsot(ot) = stsot(jj);
                ot_slope(ot) = slope(jj);
            end
        end
        nn=0;
        for jj=1:nrsamples
            if ( -2.15 > ScSlope(jj))
                nn          = nn+1;
                nn_phsc(nn) = phsc(jj);
                nn_sot(nn)  = SOT(jj);
            end
        end
        
        on_Lc  = polyfit(on_stsot,on_slope,1);
        mh_Lc  = polyfit(mh_stsot,mh_slope,1);
        ot_Lc  = polyfit(ot_stsot,ot_slope,1);
        %Lc     = polyfit(stsot(1:55),slope(1:55),1);
        %Lc     = polyfit([mh_stsot on_stsot ot_stsot],[mh_slope on_slope ot_slope],1);
        %Lc     = polyfit(stsot,slope,1);
        ss     = 0:0.01:45;
        ot_fit = ot_Lc(2) + ot_Lc(1).*ss;
        on_fit = on_Lc(2) + on_Lc(1).*ss;
        mh_fit = mh_Lc(2) + mh_Lc(1).*ss;
        %fit    =    Lc(2) +    Lc(1).*ss;
        
        plot(ss,on_fit,'b');
        plot(ss,mh_fit,'r');
        %plot(ss,ot_fit,'c');
        plot(ss,ot_fit,'c');
        xlim([0 45]);ylim([-2.8 -2.1]);
        title('Scintillation slope vs SOT based on stations','fontname','TimesNewRoman','fontsize',11);
        xlabel('SOT','fontname','Times New Roman','fontsize',11);
        ylabel('Scint slope','fontname','Times New Roman','fontsize',11);

        fprintf('Mh slope average: %2.3d (samples %d) std: %1.3d\n',mean(mh_slope),mh,std(mh_slope));
        fprintf('Mc slope average: %2.3d (samples %d) std: %1.3d\n',mean(mc_slope),mc,std(mc_slope));
        fprintf('No slope average: %2.3d (samples %d) std: %1.3d\n',mean(no_slope),no,std(no_slope));
        fprintf('Ma slope average: %2.3d (samples %d) std: %1.3d\n',mean(ma_slope),ma,std(ma_slope));
        fprintf('Wz slope average: %2.3d (samples %d) std: %1.3d\n',mean(wz_slope),wz,std(wz_slope));
        fprintf('Ys slope average: %2.3d (samples %d) std: %1.3d\n',mean(ys_slope),ys,std(ys_slope));
        fprintf('On slope average: %2.3d (samples %d) std: %1.3d\n',mean(os_slope),on,std(os_slope));
        fprintf('Hh slope average: %2.3d (samples %d) std: %1.3d\n',mean(hh_slope),hh,std(hh_slope));
        
        %legend(['Mh','Others','Total']);
    case 5
        % Plot the first TECvsIPS with removing certain points.
        TECxVenus;
        nrs_discarded = 0;
        hold off;
        
        figure('Position',[150 600 425 300],'Units', 'centimeters');
        %set(gca,'Position',[0.12 0.14 0.85 0.78],'Units','normalized','fontsize',10,'fontname','Times New Roman');
        semilogy(SOTv,TECv*(1/1.5),'--');hold on;
        semilogy(SOTv,TECv*1.5,'--');
        semilogy(SOTv,TECv,'c');
        semilogy(nn_sot,1935*nn_phsc,'r.');
        %semilogy(SOT(ii),1935*phsc(ii),'r.');
        title({'Projecting the phase scintillation index onto';'Venus-to-Earth IP TEC over the line of sight'},'fontsize',20,'fontname','Arial');
        xlabel('Solar elongation (degrees)','fontsize',17,'fontname','Arial');
        ylabel('TEC of IP (cm^-2), blue line','fontsize',17,'fontname','Arial');
        
        dev(1:nrsamples)=0;
        Lphsc       = log10(nn_phsc);
        Lc          = polyfit(log10(nn_sot),Lphsc,1);
        
        % Checking the individual deviation.
        for ii=1:nn
            dev(ii) = Lphsc(ii) - (Lc(2) + Lc(1).*log10(nn_sot(ii)));
        end
        
        std(dev)
    case 6
        figure('Position',[150 600 425 300],'Units', 'centimeters');
        set(gca,'Position',[0.12 0.14 0.85 0.78],'Units','normalized','fontsize',10,'fontname','Times New Roman');
        semilogy(dt,phsc,'r.');
        xlim([2.45505e6 2.45575e6]);
        ylim([0.05 100]);grid on;
        title('Phase scintillation index evolution from 11/2009 to 05/2011','fontsize',11,'fontname','Times New Roman')
        xlabel('MJD time','fontsize',11,'fontname','Times New Roman');
        ylabel('Phase scintillation index','fontsize',11,'fontname','Times New Roman');
    case 7
        semilogy(SOT,phsc,'rx');%ylim([-50 50])
        figure('Position',[150 600 425 300],'Units', 'centimeters');
        set(gca,'Position',[0.12 0.14 0.85 0.78],'Units','normalized','fontsize',10,'fontname','Times New Roman','ytick',[0.1 1 10 100]);
        nrs_discarded = 0;
        
        for ii=1:nrsamples
            if (phsc(ii) > 0.45) && (SOT(ii) > 30)
                nrs_discarded = nrs_discarded + 1;
            else
               semilogy(SOT(ii),phsc(ii),'rx');hold on;
            end
        end
        
        ylim([0.05 100]); grid on;
        fprintf('Samples discarded: %d\n',nrs_discarded);
        title('Phase scintillation index compared to the Solar elongation','fontsize',11,'fontname','Times New Roman')
        xlabel('Solar elongation (deg)','fontsize',11,'fontname','Times New Roman');
        ylabel('Phase scintillation index','fontsize',11,'fontname','Times New Roman');
    otherwise
        fprintf('Select an option to print a plot\n');
 end
end