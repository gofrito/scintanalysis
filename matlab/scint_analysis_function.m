%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MATLAB tools to analyze Interplanetary Plasma  %
% scintillation observations with VEX            %
%                                                %
% scint_analysis_function.m - G. Molera          %
% version number 8 - 07/05/2010                  %
% handles function                               %
% Input: handles                                 %
% Output: handles                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [handles] = scint_analysis_function(handles)
%% Introduction to the process
fprintf('Read the phase file. Store the scans in a matrix format.   Calculate \n');
fprintf('basic statistic parameters: max-min,RMS or avg energy in time domain \n');
fprintf('We use windowed spectra for slope estimation and un-windowed for the \n');
fprintf('data filtering.\n');
fprintf('To characterise the scintillation band and the system noise, compute \n');
fprintf('the averaged power spectra of the windowed phase data. Boundaries of \n');
fprintf('each band are selected of best approach.  The scintillation phase is \n');
fprintf('obtained by calculating the RMS of each scan after filter in spectra \n');
fprintf('domain the scint band. \n');
format long;
begin = now;
filename = handles.Fn;
base = 257;
exp  = 36;

%% The processing part starts here.
fprintf('\n');
fprintf('We open the file : %s \n',filename);
clear Ph_tmp;
% Read the values from the daily phase file
Ph_tmp = textread(filename);
tp  = Ph_tmp(:,1);
t   = size(Ph_tmp);
Np  = t(1);
Ns  = t(2)-1;
dtp = tp(2)-tp(1);
Ts  = dtp*Np;
df  = 1/Ts;
BW  = Np*df;
Fa  = 10;
bfa = floor(Fa/df);
Ph  = zeros(Ns,Np);
fprintf('Number of scans  : %3.1f \n',Ns);
fprintf('\n');

for k=1:Ns
   Ph(k,:)= Ph_tmp(:,k+1);
end

jp = 0:1:(Np-1);
ff = jp*df;

% Defining windows
winU(1:Np)  = 1;
winC1(1:Np) = cos(pi/Np.*(jp - 0.5*Np));
winC2(1:Np) = cos(pi/Np.*(jp - 0.5*Np)).^2;


% Get max values of the signal
Phx(1:Ns) = 0;
Phn(1:Ns) = 0;
Phr(1:Ns) = 0;
Phm(1:Ns) = 0;
for k=1:Ns
    Phx(k) = max(Ph(k,:));
    Phn(k) = min(Ph(k,:));
    Phr(k) = std(Ph(k,:));
    Phm(k) = mean(Ph(k,:));
end

fprintf('Phx: %s \nPhn: %s \nPhr: %s \nPhm: %s\n',mat2str(round(Phx*100)/100),mat2str(round(100*Phn)/100),mat2str(round(100*Phr)/100),mat2str(round(Phm*100)/100));
fprintf('\n');

Et(1:Ns)=0;
% Compute the "Total Energy" of the fluctuactions
for k=1:Ns
    for j=1:Np
        Et(k) = Et (k) + dtp*Ph(k,j)^2;
    end
end
Pt = Et./Ts;
fprintf('Compute the "Total Energy" \n');
fprintf('sqrt(Pt): %s \nand RMS : %s\n',mat2str(round(sqrt(Pt)*100/100)),mat2str(round(Phr)*100/100));
fprintf('\n');

% Make the spectra
sp   = zeros(Ns,Np);
spw  = zeros(Ns,Np);
psp  = zeros(Ns,Np);
pspw = zeros(Ns,Np);

% Here I could decide or choose which kind of windows I want
win  = winC1;

%sp  = fft(Ph, [], Ns);
%spw = fft(Ph.*win, [], Ns);

for k=1:Ns
    sp(k,:)  = fft(Ph(k,:));
    spw(k,:) = fft(Ph(k,:).*win);
end;

for k=1:Ns
    psp(k,:)  = (abs(sp(k,:)).^2)/Np;
    pspw(k,:) = (abs(spw(k,:)).^2)/Np;
end

% Check again power levels and RMS
Pf = sum(psp,2)/Np;

fprintf('Compare the power levels: time vs. frequency domain \n');
fprintf('Pf: %s\nPt: %s\n',mat2str(round(Pf*100)/100),mat2str(round(Pt*100)/100));
fprintf('\n');

%% Normalise the power spectra in absolute mode.
psp  = 2*psp/BW;
pspw = 2*pspw/BW;
Pft = sum(psp(:,1:bfa),2).*df;

fprintf('Compare the power levels absolute mode: time vs. frequency domain \n');
fprintf('Pft: %s\nPt: %s\n',mat2str(round(Pft*100)/100),mat2str(round(Pt*100)/100));
fprintf('\n');

% Now we isolate the scintillation and system band noise:
% making the average spectrum and smooth with a Hann window
pspa  = sum(psp,1)/Ns;
pspaw = sum(pspw,1)/Ns;
pspah = han(pspa);

% We compute the system noise level. 
snm   = Submean(pspaw,ff,handles.fln,handles.fhn);
fprintf('SystemNoise: %6.5f\n',snm);

%% Method 1 to calculate the scintillation BW
% Extract the part of the spectrum within the boundaries
bbeg = floor(handles.flo/df+0.5)+1;
bend = floor(handles.fhi/df+0.5)+1;

ffs  = ff(bbeg:bend);
pss  = pspaw(bbeg:bend);

% Now will convert into Log-Log domain
Lffs = log10(ffs);
Lpss = log10(pss);
Lf   = polyfit(Lffs,Lpss,1);

% Polyfit returns the first grade polynomial fit. First element is slope
% and second element is yo.
Lfit = Lf(2) + Lf(1).*Lffs;

%Check the goodness of fit
dLfit             = Lpss - Lfit;
rdLfit            = std(dLfit);
slope     = Lf(1);
errslope  = rdLfit/(max(Lffs)-min(Lffs));
var       = -errslope/slope;
fprintf('Slope      : %6.3f \nerrSlope   : %6.3f\nerr/Slope  : %6.3f\n',Lf(1),errslope,var);

%% Plot again the full spectra and the Fit Line
Lfitline = Lf(2) + Lf(1).*log10(ff + 0.001*df);
fitline  = 10.^(Lfitline);


% Find a frequency at which the fit line will cross certain level
CrossLev1  = snm;
Lfcross1   = (log10(CrossLev1) - Lf(2))/Lf(1);
fcross1    = 10^(Lfcross1);
CrossLev2  = 1e-5;
Lfcross2   = (log10(CrossLev2) - Lf(2))/Lf(1);
fcross2    = 10^(Lfcross2);
HighFscint = fcross2;
fprintf( 'Fcross1    : %6.3f\nHighFscint : %6.3f \n',fcross1,fcross2);

%% Method 2 to calculate the scintillation BW
% Integrate the total power of the scintillation with a high BW. fhi=8;
bfhi = floor(8/df+0.5+1);
Npi  = bfhi - bbeg;
jpi  = 1:Npi-1;
fp1  = ff(bbeg) + jpi.*df;

ScintTotalPower(1:Npi)   = 0;
NoiseTotalPower(1:Npi)   = 0;
NoiseTotalPower(1)       = snm*df ;
ScintTotalPower(1)       = fitline(bbeg)*df;

for jj=1:Npi-1
    ScintTotalPower(jj+1) = ScintTotalPower(jj) + fitline(bbeg+jj)*df;
    NoiseTotalPower(jj+1) = NoiseTotalPower(jj) + snm*df;
end

Xstp = max(ScintTotalPower);

ScintTotalPower = ScintTotalPower/Xstp;
NoiseTotalPower = NoiseTotalPower/Xstp;

%% Filtering and outputtig the data. In this case we use the un-windowed
%% data.
FiltScint(1:Np) = 0;
FiltNoise(1:Np) = 0;
for k=1:Np
    if ((handles.flc <= ff(k)) && (ff(k) <= handles.fhc))
        FiltScint(k-1) = 2;
    end
    if ((handles.fln <= ff(k)) && (ff(k) <= handles.fhn))
        FiltNoise(k-1) = 2;
    end
end

% Analysis of the Scintillation band.
sps          = zeros(Ns,Np);
phs          = zeros(Ns,Np);
rmsphs(1:Ns) = 0;

for k=1:Ns
    sps(k,:)  = sp(k,:).*FiltScint;
    sps(k,5)
    phs(k,:)  = real(ifft(sps(k,:)));
    rmsphs(k) = std(phs(k,:));
    phs(k,1)
end

%Analysis of the System Noise band
spn          = zeros(Ns,Np);
phn          = zeros(Ns,Np);
rmsphn(1:Ns) = 0;

for k=1:Ns
	spn(k,:)  = sp(k,:).*FiltNoise; 
	phn(k,:)  = real(ifft(spn(k,:)));
	rmsphn(k) = std(phn(k,:));
end

%% Check for excessive high frequency scintillations
SysNoiseLevel   = snm;
PeakScint       = pspaw(bbeg);
noiseline(1:Np) = SysNoiseLevel;
fitline1        = fitline;
fitline2        = fitline + noiseline;
 
bbeg  = floor(0.01/df + 0.5 + 1);
bend  = floor(9.5/df  + 0.5 + 1);

spas  = pspaw(bbeg:bend); % pspan(bbeg:bend);
fit2s = fitline2(bbeg:bend);
ffs   = ff(bbeg:bend);
Nps   = length(spas);

% Residual with respect to the Slope + flat noise model
espas  = spas./fit2s;
mespas = mean(espas);

% Clean up the 1Hz spurious
fs1    = 1;
fs2    = 1.1236;

for k=1:Nps
    if (abs(ffs(k)-fs1) < df*2)
        espas(k) = mespas;
    end
    if (abs(ffs(k)-fs2) < df*3)
        espas(k) = mespas;
    end
end

% Hanning smoothed 100 times
espash   = hanN(espas,100);
RMSfit   = std(espas);
CrossLev = CrossLev2;
pspas    = espash.*fit2s;

fprintf('RMSfit     : %6.3f \n',RMSfit);
fprintf('PeakScint  : %6.3f radian^2 per Hz, at 3 mHz \n',PeakScint);
fprintf('CrossLev   : %6.3f and HighFscint : %6.3f @ 1Hz \n',CrossLev,HighFscint); 
fprintf('Correcting the phase scintillations due to the system noise contribution\n');
fprintf('Removing the contribution of the noise to the rmsphs\n');

rmsphc = sqrt(rmsphs.^2 - rmsphn.^2);

fprintf('Scintillation phase noise RMS \n');
fprintf('%s \n',mat2str(round(10000*rmsphc)/10000));
fprintf('Phase noise RMS \n');
fprintf('%s \n',mat2str(round(10000*rmsphn)/10000));

fprintf('mean(rmsphc): %6.3f and stdev(rmsphc): %6.3f\n', mean(rmsphc), std(rmsphc)/sqrt(Ns-1));
fprintf('mean(rmsphs): %6.3f and stdev(rmsphs): %6.3f\n', mean(rmsphs), std(rmsphs)/sqrt(Ns-1));

%% Frequency is a time derivative of the phase (1/2pi)dPhase/dTime

Npfrq = Np - 1;
frq   = zeros(Ns,Npfrq);

for k=1:Ns
    frq(k,:) = phs(k,2:Np) - phs(k,1:(Np-1));
end
frq = frq./(2*pi*dtp);

% Residual freq: watch out the glitches at the ends caused by FFT filtering
frqs             = zeros(Ns,Npfrq-200+1);
tps(1:Npfrq-199) = tp(101:Npfrq-99)';    
rmsfrq(1:Ns)     = 0;

for k=1:Ns
    frqs(k,:) = frq(k,101:Npfrq-99);
    rmsfrq(k) = std(frqs(k,:));
end

fprintf('Scintillation phase in frequency Hz \n');
fprintf('%s \n',mat2str(round(10000*rmsfrq)/10000));

%% Charecterizing 1 Hz bump in the the final smooth phase noise signal
ld = length(espash);
lm = mean(espash(4000:7000));
filt1Hz(1:ld) = 0.8;
xmin = ld;
xmax = 1;

for k=400:ld-5
    if(mean(espash(k-5):espash(k+5)) > 1.5*lm)
        if ( k < xmin)
            xmin = k;
        end
        if ( k > xmax)
            xmax = k;
        end
    end
end

handles.filt1Hz(xmin:xmax) = 2;
handles.ffmin  = ffs(xmin);
handles.ffmax  = ffs(xmax);
[xx,yy]        = max(espash);
handles.ffpeak = ffs(yy);
% Calculating the power of the 1 Hz bump.
pbump          = mean(espash(xmin:xmax));
rmsbump        = std(espash(xmin:xmax));
bpbump         = mean(espash(1:200));
apbump         = mean(espash(4000:7000));

fprintf('Mean power of the 1 Hz bump: %s \n',mat2str(round(100*pbump)/100));
fprintf('Mean power before the bump: %s and mean power after the bump: %s \n',mat2str(round(100*bpbump)/100),mat2str(round(100*apbump)/100));
fprintf('Standard deviation the 1 Hz bump: %s \n',mat2str(round(100*rmsbump)/100));

fprintf('Storing the results in a summary file\n');

%fid = fopen(strcat('Fdets.vex',handles.PhasesFile(11:24),'r2i.txt'),'r');
%if (fid < 0)
%    fprintf(1,'Error reading the Fdets file\n');
%end
%data = textscan(fid,'%f %f %f %f %f','HeaderLines',4);
%fclose(fid);
 
%datamat = cell2mat(data);
%SNR     = datamat(:,2);
%rfdets  = datamat(:,5);
%[ro,co] = size(SNR);
%mSNR    = mean(SNR);
%mrfdets = mean(rfdets);

fid = fopen(strcat('Phases.sum.',handles.PhasesFile(8:10),handles.PhasesFile(11:24),'txt'),'w');

switch(handles.PhasesFile(22:23))
    case{'Mh'}
        code = 1;
    case{'Mc'}
        code = 2;
    case{'Ma'}
        code = 3;
    case{'Nt'}
        code = 4;
    case{'Wz'}
        code = 5;
    case{'Ys'}
        code = 6;
    case{'Pu'}
        code = 7;
    case{'On'}
        code = 8;
    case{'Hh'}
        code = 9;
    case{'Sh'}
        code = 10;
    case{'Km'}
        code = 11;
    case{'Ur'}
        code = 12;
    case{'Ht'}
        code = 13;
    case{'Sv'}
        code = 14;
    case{'Zc'}
        code = 15;
    case{'Bd'}
        code = 16;
    case{'Tm'}
        code = 17;
    case{'Ww'}
        code = 18;
end

for i=1:Ns
    fprintf(fid,'|  %d %d %d %d %s %s %s  1140 |  %s  %s  %s %s  %s  %s |\n',base+i,exp+1,i,code,handles.PhasesFile(11:14),handles.PhasesFile(16:17),handles.PhasesFile(19:20),mat2str(round(1000*rmsphs(i))/1000),mat2str(round(1000*rmsphn(i))/1000),mat2str(round(1000*slope)/1000),mat2str(round(1000*errslope)/1000),mat2str(round(100*PeakScint)/100),mat2str(round(1e6*snm)/100));
end
% Add a summary of the observations
fprintf(fid,'|-------------------------------------------------------\n');
fprintf(fid,'|  %d %d %d %s %s %s  |  %s  %s  %s %s  %s  %s |\n', exp,Ns,code,handles.PhasesFile(11:14),handles.PhasesFile(16:17),handles.PhasesFile(19:20),mat2str(round(1000*sum(rmsphs)/Ns)/1000),mat2str(round(1000*sum(rmsphn)/Ns)/1000),mat2str(round(1000*slope)/1000),mat2str(round(1000*errslope)/1000),mat2str(round(100*PeakScint)/100),mat2str(round(1e6*snm)/100));
fclose(fid);

% Storing the variables for later use in the GUI menu.
handles.ff          = ff;
handles.ffs         = ffs;
handles.psp         = psp;
handles.tp          = tp;
handles.Ph          = Ph;
handles.slope       = slope;
handles.errslope    = errslope;
%handles.pspan       = pspan;
handles.pspaw       = pspaw;
handles.rmsphn      = rmsphn;
handles.rmsphs      = rmsphs;
handles.pspa        = pspa;
handles.pspas       = pspas;
handles.espash      = espash;   
handles.fitline2    = fitline2;
handles.fitline     = fitline;
handles.fit2s       = fit2s;
handles.Lffs        = Lffs;
handles.Lfit        = Lfit;
handles.Lpss        = Lpss;
handles.snm         = snm;
handles.crosslevel  = CrossLev2;
handles.highfscint  = HighFscint;
handles.PeakScint   = PeakScint;
handles.pbump       = pbump;
handles.rmsbump     = rmsbump;
handles.rmsfrq      = rmsfrq;
handles.filt1Hz     = filt1Hz;

fin = now;
fprintf('Time ellapsed: %6.2f s.\n',(fin-begin)*3600*24);
end