ls %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% IPSvsTEC.m - G. Molera                         %
% Compares values for TEC and scintillation phase%
% 2012.02.12 Now data is read from the .mat file %
% data is in separate variables             	 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
TECxVenus;

% Reading the Observation Table 
filename      = '../results/ScintObsSummaryTabVEX.Block1.103.mat';
nrsamples     = length(nr);

load(filename);

doy(1:nrsamples) = 0;
wt(1:nrsamples)  = 1; 

% We convert the samples timestamps to MJD
for k=1:nrsamples
   doy(k)=mjd(yy(k),mm(k),dd(k),HH(k),MM(k),SS(k));   
end

PhSc        = PhSc(1:nrsamples);
PhN         = PhN(1:nrsamples);
SysNoise    = SysNoise(1:nrsamples);
SOT         = SOT(1:nrsamples);
TEC         = TEC(1:nrsamples);
VEX_Ion_TEC = VEX_Ion_TEC(1:nrsamples);
StdDev(1:2500) = 0;

%Determining the best fit
for jj=1:2500
    count = 0;
    for kk=1:nrsamples
        if(SOT(kk) < 40)
            StdDev(jj) = (log10(jj*PhSc(kk)-VEX_Ion_TEC(kk))-log10(TEC(kk)))^2 + StdDev(jj);
            count = count + 1;
        end
    end
    StdDev(jj) = sqrt(StdDev(jj))/count;
end

% Best fitting was achieved with 1935;
Diff     = 2015*PhSc - TEC;
Difflog  = log10(2015*PhSc) - log10(TEC);
Diffelog = log10(2015*PhSc) - log10(TEC + VEX_Ion_TEC);

% Fit function to our data
fit = PolyfitW(doy,Difflog,wt,1);

% Correction of the Phase noise in our data;
PhCr1 = sqrt(PhSc.^2 - PhN.^2);

DiffCr1    = 2015*PhCr1 - TEC;
DiffCrlog1 = log10(2015*PhCr1) - log10(TEC);

DiffCr2    = (2150*PhCr1 - VEX_Ion_TEC) - TEC ;
DiffCrlog2 = log10(2150*PhCr1-VEX_Ion_TEC) - log10(TEC);

%% Ploting first the comparison bw IP scintillation and TEC
%figure(1);
%semilogy(SOTv,TECv*(1/1.5),'--');hold on;
%semilogy(SOTv,TECv*1.5,'--');
%semilogy(SOTv,TECv,'c');
%semilogy(SOT,2015*PhSc,'r.','LineWidth',3);
%grid on;
%title({'Projecting the phase scintillation index onto';'Venus-to-Earth IP TEC on the line of sight'},'fontsize',14,'fontname','Times New Roman');
%xlabel('Solar elongation (degrees)','fontsize',12,'fontname','Times New Roman');
%ylabel('TEC of IP (cm^{-2})','fontsize',12,'fontname','Times New Roman');

figure(2);
semilogy(SOTv,TECv*2,'--');hold on;
semilogy(SOTv,TECv,'c');
semilogy(SOTv,TECv*0.6,'--');
semilogy(SOT,2015*PhCr1,'r.','LineWidth',3);
grid on;
%title({'Projecting the phase scintillation index onto';'Venus-to-Earth IP TEC along the line of sight'},'fontsize',14,'fontname','Helvetica');
title('Comparison of phase scintillation indices and estimated TEC','fontsize',14,'fontname','Helvetica');
xlabel('Solar elongation (degrees)','fontsize',13,'fontname','Helvetica');
ylabel('TEC (TECU)','fontsize',13,'fontname','Helvetica');
legend('fsw TEC','ave TEC','ssw TEC','\sigma_{Sc}');

%figure(3);
%semilogy(SOTv,TECv*(3/5),'--');hold on;
%semilogy(SOTv,TECv*(10/5),'--');
%semilogy(SOTv,TECv,'c');
%semilogy(SOT,2150*PhCr1-VEX_Ion_TEC,'r.','LineWidth',3);
%grid on;

% figure(2);
% plot(doy,Difflog,'rx');grid on; hold on; plot(doy,fit,'b');
% %set(gca,'XTickLabel',{'042009','012010','022010','032010','042010','012011','022011'});
% title('\sigma_{Sc} and TEC difference along 2 years of data','fontsize',13,'fontname','Arial');
% xlabel('Observation day MJD','fontsize',12,'fontname','Arial');
% ylabel('Log Diff \sigma_{Sc} and TEC','fontsize',12,'fontname','Arial');
% 
% figure(3);
% plot(LatEcl,Difflog,'kx');grid on;
% title('Diff IPS and TEC vs. Ecliptic latitude','fontsize',13,'fontname','Arial');
% ylabel('Log diff','fontsize',12,'fontname','Arial');
% xlabel('Ecliptic Latitude','fontsize',12,'fontname','Arial');
% 
% figure(4);
% plot(SOT,Difflog,'bo');hold on;
% plot(SOT,Diffelog,'r+');

fprintf('a) Std deviation sigma_{Sc}: %f\n',std(Difflog));
fprintf('b) Std deviation sigma_{Sc} with Ionosphere correction: %f\n',std(Diffelog));
fprintf('c) Improvement         : %f%%\n',2*100*(std(Difflog)-std(Diffelog))/(std(Difflog)+std(Diffelog)));
fprintf('*****************************\n');
fprintf('d) Std deviation (sigma_{Sc}^2 - sigma_{noise}^2)^{1/2}: %f\n',std(DiffCrlog1));
fprintf('e) Improvement         : %f%%\n',2*100*(std(Difflog)-std(DiffCrlog1))/(std(DiffCrlog1)+std(Difflog)));
fprintf('*****************************\n');
fprintf('f) Std deviation (sigma_{Sc}^2 - sigma_{noise}^2)^{1/2}: %f\n',std(DiffCrlog2));
fprintf('g) Improvement         : %f%%\n',2*100*(std(Difflog)-std(DiffCrlog2))/(std(DiffCrlog2)+std(Difflog)));