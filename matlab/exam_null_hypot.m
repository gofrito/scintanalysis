%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% exam_null_hypot.m - G. Molera                  %
% Test the null hypothesis and student -test     %
% Input:                                         %
% Output:                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Basic example of a Student t-test
% Create 100 randoms values from (0,1) with a normal distribution.
nrsamples = 100;
x = rand(nrsamples,1);
%y = random('norm',0,1,[nrsamples,1]);
y=x.^2;

% And now we test it with ttest or chi2gof.
[th,tp,ci,stat] = ttest2(x,y,0.5,[],'equal')

index = nrsamples - 2;

% my=mean(y);
% dev = sum((y - my).^2);
% SSE = 0;
% for ii=1:nrsamples
%     SSE = (Lf(2) + Lf(1).*x(ii) - y(ii)).^2 + SSE;
% end
% 
% Blse = mean(x);
% Bo   = var(x);
% tscore = (B - Bo)*sqrt(nrsamples-2)/sqrt(SSE/dev);
% 
