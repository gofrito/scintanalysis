%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% sot2gop.m - G. Molera                          %
% Converting Sun-Observer-Target angles into  	 %
% Geocentric orbital phase. The function is used %
% display the TEC and IPS as a function of pho   %
% Geocentric orbit respect the Earth position    %
% Input  : Rv, distance of Venus, SOT to Venus   %
% Output : pho                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [phi] = sot2gop(Rv,dist,sot)

len = length(dist);

phi = zeros(len,1);
gam = zeros(len,1);
dturn = 1.228;			% Not so sure


for kk=1:len
    phi(kk) = asind(dist(kk)*sind(sot(kk))/Rv);
    gam(kk) = 180 - asind(dist(kk)*sind(sot(kk))/Rv);
end

for kk=1:len
	if (dist(kk) <= dturn)
		phi(kk) = gam(kk);
	end
end
end