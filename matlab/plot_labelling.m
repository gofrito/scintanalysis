%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% plot_labelling.m - G. Molera 			 %
% print phase data from several phases           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Printing a simple graph
fprintf('Loading .mat files with the scintillation analysis for multiple observations\n');
load Mc2611.mat;
Mc = handles;
load Mh1801.mat;
Mh = handles;
load Ys2602.mat;
Ys = handles;
load Wz3103.mat;
Wz = handles;

figure(1);
handles = Mc;
subplot(2,2,1);pf=loglog(handles.ff,handles.pspa,'r');hold on;
pm=loglog(handles.ff,handles.fitline2,'k');
grid on; set(gca,'Xcolor','b');
set(gca,'Ycolor','b'); 
xlim([10^-3,10]);ylim([10^-5,10^5]);
set(gca,'XTickLabel',{'0.001','0.01','0.1','1','10'});set(gca,'GridLineStyle','-');
xlabel('frequency band');legend('phase fluctuation','model');
ylabel('Phase spectra');
title('Spectral phase observed at Medicina 26 Nov 2009');
text(0.05,0.005,['slope = ',num2str(Mc.slope)],'VerticalAlignment','bottom','HorizontalAlignment','right','FontSize',10,'BackgroundColor','white');

handles = Mh;
subplot(2,2,2);pf=loglog(handles.ff,handles.pspa,'r');hold on;
pm=loglog(handles.ff,handles.fitline2,'k');
grid on; set(gca,'Xcolor','b');set(gca,'Ycolor','b'); 
xlim([10^-3,10]);ylim([10^-5,10^5]);
set(gca,'XTickLabel',{'0.001','0.01','0.1','1','10'});set(gca,'GridLineStyle','-')
xlabel('frequency band');legend('phase fluctuation','model');
ylabel('Phase spectra');
title('Spectral phase observed at Mets�hovi 18 Jan 2010');
text(0.05,0.1,['slope = ',num2str(Mh.slope)],'VerticalAlignment','bottom','HorizontalAlignment','right','FontSize',10,'BackgroundColor','white');

handles = Ys;
subplot(2,2,3);pf=loglog(handles.ff,handles.pspa,'r');hold on;
pm=loglog(handles.ff,handles.fitline2,'k');
grid on; set(gca,'Xcolor','b');set(gca,'Ycolor','b'); 
xlim([10^-3,10]);ylim([10^-5,10^5]);set(gca,'XTickLabel',{'0.001','0.01','0.1','1','10'});set(gca,'GridLineStyle','-')
xlabel('frequency band');legend('phase fluctuation','model');
ylabel('Phase spectra');
title('Spectral phase observed at Yebes 26 Feb 2010');
text(0.05,0.005,['slope = ',num2str(Ys.slope)],'VerticalAlignment','bottom','HorizontalAlignment','right','FontSize',10,'BackgroundColor','white');

handles = Wz;
subplot(2,2,4);pf=loglog(handles.ff,handles.pspa,'r');
hold on;pm=loglog(handles.ff,handles.fitline2,'k');
grid on; set(gca,'Xcolor','b');set(gca,'Ycolor','b'); 
xlim([10^-3,10]);ylim([10^-5,10^5]);set(gca,'XTickLabel',{'0.001','0.01','0.1','1','10'});set(gca,'GridLineStyle','-')
xlabel('frequency band');legend('phase fluctuation','model');
ylabel('Phase spectra');
title('Spectral phase observed at Wettzell 31 Mar 2010');
text(0.05,0.005,['slope = ',num2str(Wz.slope)],'VerticalAlignment','bottom','HorizontalAlignment','right','FontSize',10,'BackgroundColor','white');


% Printing summary output
% title('Phase Scintillation RMS vs Sun-Observer-Target');
% xlabel('Sun-Observer-Target (degrees)');
% ylabel('Phase Scintilaltion RMS (radians)');
% loglog(SOT,phsc,'ko');grid minor;
% grid on;hold on;set(gca,'Xcolor','b');set(gca,'Ycolor','b');set(gca,'XTickLabel',{'0','10','100'});set(gca,'YTickLabel',{'0.01','0.1','1','10','100'});
% axis([1 100 0.01 100]);set(gca,'GridLineStyle','-')
% loglog(10.^Lff,fit,'r');
% loglog(10.^Lff,fit*(1+var(Lphsc)),'y');loglog(10.^Lff,fit.*(1-var(Lphsc)),'y');
% title('Phase Scintillation RMS vs Sun-Observer-Target - log scale');
% xlabel('Sun-Observer-Target (degrees)');
% ylabel('Phase Scintilaltion RMS (radians)');

% Plotting the results 
% semilogx(SOT,dev,'o');grid minor;hold on;set(gca,'GridLineStyle','-')
% axis([1 100 -0.4 0.6]);
% semilogx(1:0.01:100,sigmadev,'r-');semilogx(0:0.01:100,-sigmadev,'r-');
% title('Deviations in log-log');
% xlabel('Sun-Observer-Target (degrees)');
% ylabel('Deviation from fit line');
% 
% figure;
% histfit(dev);axis([-0.75 0.75 0 40]);
% title('Histogram of deviations');
% xlabel('Deviation');
% ylabel('Counts');
