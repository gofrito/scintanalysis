%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% IPSvsTEC.m - G. Molera                         %
% Compares values for TEC and scintillation phase%
% 2012.02.12 Now data is read from the .mat file %
% data is in separate variables             	 %
% Including table 1 and 2                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
TECxVenus;
Rv = 0.723;

% Reading the Observation Table 
filename      = '../results/ScintObsSummaryTabVEX.Block1.103.mat';
load(filename);
nrs1     = length(nr);

doy(1:nrs1,1) = 0;

% We convert the samples timestamps to MJD
for k=1:nrs1
   doy(k)=mjd(yy(k),mm(k),dd(k),HH(k),MM(k),SS(k));   
end

PhScint = PhSc;
PhNoise = PhN;
SysN    = SysNoise;
sel     = SOT;
dist    = AUdist;
tec     = TEC;
Ion_TEC = VEX_Ion_TEC + Ceb_Ion_TEC;
IPS_TEC = TEC;

% Reading the Observation Table 
filename      = '../results/ScintObsSummaryTabVEX.Block2.047.mat';
load(filename);
nrs2          = length(nr);

nrs = nrs1+nrs2;

% We convert the samples timestamps to MJD
for k=1:nrs2
   doy(k+nrs1)=mjd(yy(k),mm(k),dd(k),HH(k),MM(k),SS(k));   
end

PhScint(nrs1+1:nrs) = PhSc;
PhNoise(nrs1+1:nrs) = PhN;
SysN(nrs1+1:nrs)    = SysNoise;
sel(nrs1+1:nrs)     = SOT;
dist(nrs1+1:nrs)    = AUdist;
tec(nrs1+1:nrs)     = TEC;
Ion_TEC(nrs1+1:nrs) = VEX_Ion_TEC + Ceb_Ion_TEC;
IPS_TEC(nrs1+1:nrs) = TEC;

wt(1:nrs,1) = 1; 
StdDev(1:2500,1) = 0;

po = sot2gop(Rv,dist,sel);

%Determining the best fit
for jj=1:2500
    count = 0;
    for kk=1:nrs
        if(sel(kk) < 40)
            StdDev(jj) = (log10(jj*PhScint(kk)-Ion_TEC(kk))-log10(tec(kk)))^2 + StdDev(jj);
            count = count + 1;
        end
    end
    StdDev(jj) = sqrt(StdDev(jj))/count;
end

% Best fitting was achieved with 1935;
Diff     = 2015*PhScint - tec;
Difflog  = log10(2015*PhScint) - log10(tec);
Diffelog = log10(2015*PhScint) - log10(tec + Ion_TEC);

% Fit function to our data
fit = PolyfitW(doy,Difflog,wt,1);

% Correction of the Phase noise in our data;
PhCr1 = sqrt(PhScint.^2 - PhNoise.^2);

DiffCr1    = 2015*PhCr1 - tec;
DiffCrlog1 = log10(2015*PhCr1) - log10(tec);

DiffCr2    = (2150*PhCr1 - Ion_TEC) - tec;
DiffCrlog2 = log10(2150*PhCr1-Ion_TEC) - log10(tec);

%% Ploting first the comparison bw IP scintillation and TEC
%figure(1);
%semilogy(SOTv,TECv*(1/1.5),'--');hold on;
%semilogy(SOTv,TECv*1.5,'--');
%semilogy(SOTv,TECv,'c');
%semilogy(SOT,2015*PhSc,'r.','LineWidth',3);
%grid on;
%title({'Projecting the phase scintillation index onto';'Venus-to-Earth IP TEC on the line of sight'},'fontsize',14,'fontname','Times New Roman');
%xlabel('Solar elongation (degrees)','fontsize',12,'fontname','Times New Roman');
%ylabel('TEC of IP (cm^{-2})','fontsize',12,'fontname','Times New Roman');

%figure(2);
%semilogy(SOTv,TECv*2,'--');hold on;
%semilogy(SOTv,TECv,'c');
%semilogy(SOTv,TECv*0.6,'--');
%semilogy(sel,2015*PhCr1,'r.','LineWidth',3);
%title({'Projecting the phase scintillation index onto';'Venus-to-Earth IP TEC along the line of sight'},'fontsize',14,'fontname','Helvetica');
%title('Comparison of phase scintillation indices and estimated TEC','fontsize',14,'fontname','Helvetica');
%xlabel('Solar elongation (degrees)','fontsize',13,'fontname','Helvetica');
%ylabel('TEC (TECU)','fontsize',13,'fontname','Helvetica');
%legend('fsw TEC','ave TEC','ssw TEC','\sigma_{Sc}');

h= figure(1);
subplot(2,1,1);grid minor;
semilogy(sel,PhCr1,'b.','LineWidth',3);
ylabel('Scint. index','fontsize',13,'fontname','Helvetica');
xlabel('Solar elongation (degrees)','fontsize',13,'fontname','Helvetica');
subplot(2,1,2);grid minor;
semilogy(po,PhCr1,'b.','LineWidth',3);
ylabel('Scint. index','fontsize',13,'fontname','Helvetica');
xlabel('Orbital phase (degrees)','fontsize',13,'fontname','Helvetica');
%set(h,'fontsize',12,'fontname','Helvetica');


figure(3);
semilogy(pho,TECv*2,'--');hold on;
semilogy(pho,TECv,'c');
semilogy(pho,TECv*0.6,'--');
semilogy(po,3500*PhCr1,'r.','LineWidth',3);
semilogy(po,Ion_TEC,'gx');
%semilogy(po,IPS_TEC,'co');
semilogy(po,6*Ion_TEC+IPS_TEC,'bx');
xlabel('Orbital phase (degrees)','fontsize',13,'fontname','Helvetica');
ylabel('TEC (TECU)','fontsize',13,'fontname','Helvetica');
legend('fsw TEC','ave TEC','ssw TEC','\sigma_{Sc}','Ion TEC','Ion+IPS TEC');

%figure(3);
%semilogy(SOTv,TECv*(3/5),'--');hold on;
%semilogy(SOTv,TECv*(10/5),'--');
%semilogy(SOTv,TECv,'c');
%semilogy(SOT,2150*PhCr1-VEX_Ion_TEC,'r.','LineWidth',3);
%grid on;

% figure(2);
% plot(doy,Difflog,'rx');grid on; hold on; plot(doy,fit,'b');
% %set(gca,'XTickLabel',{'042009','012010','022010','032010','042010','012011','022011'});
% title('\sigma_{Sc} and TEC difference along 2 years of data','fontsize',13,'fontname','Arial');
% xlabel('Observation day MJD','fontsize',12,'fontname','Arial');
% ylabel('Log Diff \sigma_{Sc} and TEC','fontsize',12,'fontname','Arial');
% 
% figure(3);
% plot(LatEcl,Difflog,'kx');grid on;
% title('Diff IPS and TEC vs. Ecliptic latitude','fontsize',13,'fontname','Arial');
% ylabel('Log diff','fontsize',12,'fontname','Arial');
% xlabel('Ecliptic Latitude','fontsize',12,'fontname','Arial');
% 
% figure(4);
% plot(SOT,Difflog,'bo');hold on;
% plot(SOT,Diffelog,'r+');

fprintf('a) Std deviation sigma_{Sc}: %f\n',std(Difflog));
fprintf('b) Std deviation sigma_{Sc} with Ionosphere correction: %f\n',std(Diffelog));
fprintf('c) Improvement         : %f%%\n',2*100*(std(Difflog)-std(Diffelog))/(std(Difflog)+std(Diffelog)));
fprintf('*****************************\n');
fprintf('d) Std deviation (sigma_{Sc}^2 - sigma_{noise}^2)^{1/2}: %f\n',std(DiffCrlog1));
fprintf('e) Improvement         : %f%%\n',2*100*(std(Difflog)-std(DiffCrlog1))/(std(DiffCrlog1)+std(Difflog)));
fprintf('*****************************\n');
fprintf('f) Std deviation (sigma_{Sc}^2 - sigma_{noise}^2)^{1/2}: %f\n',std(DiffCrlog2));
fprintf('g) Improvement         : %f%%\n',2*100*(std(Difflog)-std(DiffCrlog2))/(std(DiffCrlog2)+std(Difflog)));