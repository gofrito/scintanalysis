%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% mjd.m - G. Molera                              %
% Calculates MJD for a given date                %
% Input:  Year Month Day Hour Minute Second      %
% Output: Modified Julian Date                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [out] = mjd(yy,mm,dd,HH,MM,SS)
    JDN = 367*yy - (7*(yy+5001+(mm-9)/7))/4+(275*mm)/9+dd+1729777;
    JD  = JDN + (HH-12)/24 + MM/1440 + SS/86400;
    out = JD;
end
 
