function varargout = scint_menu(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @scint_menu_OpeningFcn, ...
                   'gui_OutputFcn',  @scint_menu_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

% --- Executes just before menu is made visible.
function scint_menu_OpeningFcn(hObject, ~, handles, varargin)
 handles.output = hObject;
 guidata(hObject, handles);
 initialize_gui(hObject, handles, false);
end

% --- Outputs from this function are returned to the command line.
function varargout = scint_menu_OutputFcn(hObject, ~, handles)
 varargout{1} = handles.output;
 initialize_gui(gcbf, handles, true);
end

% --------------------------------------------------------------------
function initialize_gui(fig_handle, handles, isreset)
 if isfield(handles, 'metricdata') && ~isreset
    return;
 end

 % Update handles structure
 guidata(handles.figure1, handles);
end

% --- Executes on selection change in PhasesFile.
function PhasesFile_Callback(hObject, ~, handles)
 handles.Fn = strcat(handles.PhasesPath,handles.PhasesFile);
 guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function PhasesFile_CreateFcn(hObject, eventdata, handles)
 if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
 end
end

function scans_Callback(hObject,eventdata, handles)
 handles.Ns = str2double(get(hObject,'String'));
 guidata(hObject, handles);
end

% --- Executes during object creation, after setting all properties.
function scans_CreateFcn(hObject, eventdata , handles)
 if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
 end
end

function slope_value_Callback(hObject, eventdata, handles)

end

% --- Executes during object creation, after setting all properties.
function slope_value_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function Process_Callback(hObject, eventdata, handles)
scans_Callback(hObject,eventdata,handles);
PhasesFile_Callback(hObject, eventdata, handles);

fprintf( '%s; %d',handles.Fn,handles.Ns);
handles = scint_analysis_function(handles); 

%save temp.mat handles;
%set = 0;
%if (set == 1)
%    fid = fopen('tst.txt','w');
%    for ii=1:10
%        fprintf(fid,'%1.5s  %1.5s  %1.5s  %1.6s %1.4s %6.6s    %2.3s %s     0.0    0.0   0.000   0.000      0.0 |\n',num2str(handles.rmsphn(ii)),num2str(handles.rmsphs(ii)),num2str(handles.rmsfrq(ii)),num2str(handles.slope),num2str(handles.errslope),num2str(handles.highfscint),num2str(handles.snm*10000));
%    end
 %   fclose(fid);
%end

set(handles.rmsphn_values,    'String', handles.rmsphn);
set(handles.rmsphs_values,    'String', handles.rmsphs);
set(handles.slope_values,     'String', handles.slope);
set(handles.snm_values,       'String', handles.snm);
set(handles.errorslope_values,'String', handles.errslope);
set(handles.highfscint_values,'String', handles.highfscint);
set(handles.crosslevel_values,'String', handles.crosslevel);
set(handles.fmin_1Hz,         'String', handles.ffmin);
set(handles.fmax_1Hz,         'String', handles.ffmax);
set(handles.rmsfrq_values,    'String', handles.rmsfrq);

guidata(hObject,handles);
%HERE
end


% --- Executes on selection change in popuplot.
function popuplot_Callback(hObject, eventdata, handles)
  if (get(handles.ext_plot,'Value') == 1)
    %figure('Position',[150 200 440 275], 'Units', 'centimeters'); % bigbox
    figure('Position',[150 600 425 225],'Units', 'centimeters');% smallbox
    set(gca,'Position',[0.11 0.18 0.84 0.72],'Units','normalized','fontsize',10,'fontname','Times New Roman');
  end
  value = get(handles.popuplot, 'Value');
if isequal(value, 1)
 hold off;
 plot(handles.tp,handles.Ph(1,:),'r');grid on;
 xlim([min(handles.tp) max(handles.tp)]);
 %ylim([-31 31]);
 ylabel('Phase [rad]','fontsize',11,'fontname','Times New Roman');
 xlabel('time [s]','fontsize',11,'fontname','Times New Roman');
elseif isequal(value, 2)
 hold off;
 semilogy(handles.ff,handles.psp,'r');ylabel('psp');xlabel('ff');
 title('2-sided power spectra plot');grid on;  
elseif isequal(value, 3)
 hold off;
 loglog(handles.ff,handles.pspaw,'r');xlabel('ff');ylabel('pspa');xlim([10^-3 10]);ylim([10^-6 100]);
 title('average power spectra scan and smoothed by Hann window');grid on;
elseif isequal(value, 4)
 hold off;
 plot(handles.Lffs,handles.Lpss,'r');hold on;plot(handles.Lffs,handles.Lfit,'b');
 xlim([-2.5,-0.5]);ylim([-5,3]);
 title('Log spectra');xlabel('Lffs');ylabel('Lpss & Lfit');
elseif isequal(value, 5)
 hold off;
 loglog(handles.ff,handles.pspaw,'r');hold on;loglog(handles.ff,handles.fitline,'b');grid on;
 xlim([10^-3,10]);ylim([10^-6 100]);title('Comparison between spectra aver. and fitting curve in freq domain','fontsize',11,'fontweight','b','fontname','Arial');
 xlabel('ff');ylabel('pspa & fitline');
elseif isequal(value, 6) 
 hold off;
 loglog(handles.ff,handles.pspaw,'b');hold on;loglog(handles.ff,handles.fitline2,'r');grid on;
 grid minor;
 set(gca,'xlim',[1e-3 10],'YLim',[1e-5 1e3],'XTickLabel',{'0.001','0.01','0.1','1','10'});%,'YTickLabel',{'10^-4','10^-2','1','100'});
 xlabel('freq [Hz]','fontsize',10,'fontname','Arial');ylabel('power [rad^2 Hz]','fontsize',10,'fontname','Arial');
elseif isequal(value, 7)
 hold off;
 loglog(handles.ffs,handles.pspas,'r');hold on;loglog(handles.ffs,handles.fit2s,'b');grid on;
 xlim([10^-2,10]);ylim([10^-6 100]);title('Comparison between spectra aver. and Fitline 2');
 xlabel('ff');ylabel('pspa fitline');
elseif isequal(value, 8)
 hold off;
 loglog(handles.ffs,handles.espash,'b');hold on;loglog(handles.ffs,handles.filt1Hz,'r');grid on;
 xlim([10^-2,10]);title('System noise vs. 1 Hz filtering');
 xlabel('ffs');ylabel('Power');
 %set(gca,'XTickLabel',{'0.01','0.1','1','10'});
 %set(gca,'YTickLabel',{'10^-4','10^-2','1','100'});
elseif isequal(value,9)    
end
end

% --- Executes during object creation, after setting all properties.
function popuplot_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function lowNoise_Callback(hObject, eventdata, handles)
handles.fln = str2double(get(hObject,'String'));
guidata(hObject, handles);
end

function lowNoise_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function highNoise_Callback(hObject, eventdata, handles)
 handles.fhn = str2double(get(hObject,'String'));
 guidata(hObject, handles);
end

function highNoise_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function lowScint_Callback(hObject, eventdata, handles)
handles.flc = str2double(get(hObject,'String'));
guidata(hObject, handles);
end

function lowScint_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function highScint_Callback(hObject, eventdata, handles)
 handles.fhc = str2double(get(hObject,'String'));
 guidata(hObject, handles);
end

function highScint_CreateFcn(hObject, eventdata, handles)
 if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
 end
end

function flo_Callback(hObject, eventdata, handles)
 handles.flo = str2double(get(hObject,'String'));
 guidata(hObject, handles);
end

function flo_CreateFcn(hObject, eventdata, handles)
 if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
 end
end


function fhi_Callback(hObject, eventdata, handles)
 handles.fhi = str2double(get(hObject,'String'));
 guidata(hObject, handles);
end

function fhi_CreateFcn(hObject, eventdata, handles)
 if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
 end
end

function rmsphn_values_Callback(hObject, eventdata, handles)

end

% --- Executes during object creation, after setting all properties.
function rmsphn_values_CreateFcn(hObject, eventdata, handles)
 if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
 end
end

function ProcessBW_Callback(hObject, eventdata, handles)
scans_Callback(hObject,eventdata,handles);
PhasesFile_Callback(hObject, eventdata, handles);

fprintf( '%s; %d',handles.Fn,handles.Ns);
handles = broadening_function(handles); 

set(handles.rmsphn_values,    'String', handles.rmsphn);
set(handles.rmsphs_values,    'String', handles.rmsphs);
set(handles.slope_values,     'String', handles.slope);
set(handles.snm_values,       'String', handles.snm);
set(handles.errorslope_values,'String', handles.errslope);
set(handles.highfscint_values,'String', handles.highfscint);
set(handles.crosslevel_values,'String', handles.crosslevel);
set(handles.rmsfrq_values,    'String', handles.rmsfrq);

guidata(hObject,handles);
end

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
 value = get(handles.popuplot2, 'Value');
 if isequal(value, 1)
  for ii=1:handles.Ns,
    colorval = colorlst(mod(ii,size(colorlst,2)));
    bwhalf = floor(hpbwidxs(ii)/2);
    [vpeak,ipeak] = max(abs(sigsf(ii, xspanmin:xspanmax)));
    semilogy(xxvals(xspanmin:xspanmax), abs(sigsf(ii, xspanmin:xspanmax)), colorval),
    line([xxvals(xspanmin+ipeak-bwhalf-1) xxvals(xspanmin+ipeak+bwhalf-1)], [vpeak/4 vpeak/4], 'Color', colorval, 'Marker', 'x'),
    hold on;
  end
 elseif isequal(value, 2)
 end
end

function popupmenu2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% --- Executes on button press in selectPhfile.
function selectPhfile_Callback(hObject, eventdata, handles)
 [handles.PhasesFile, handles.PhasesPath] = uigetfile('*.txt','Select the phase file');
 set(handles.PhaseFileText,'String',handles.PhasesFile);
 handles.Fn = strcat(handles.PhasesPath,handles.PhasesFile); 
 guidata(hObject, handles);
end


% --- Executes on button press in ext_plot.
function ext_plot_Callback(hObject, eventdata, handles)
    
end
